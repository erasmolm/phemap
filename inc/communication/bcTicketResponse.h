//===- bcTicketResponse.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file bcTicketResponse.h
/// \author Andrea Aletto
/// \brief This file defines the header of the BCTicketResponse class
//===----------------------------------------------------------------------===//

#ifndef _BC_ICKET_RES_
#define _BC_ICKET_RES_

#include "messageSkeleton.h"
#include "messageAuth.h"
#include "string.h"

namespace phemap
{
class BCTicketResponse;
};

using namespace std;

class phemap::BCTicketResponse : public phemap::MessageAuth
{

  protected:
	int carnetLength;				 /**< Number of request tickets.*/
	puf_t *carnet = NULL;			 /**< pointer to the carnet attribute*/
	string targetHost = "localhost"; /**< address of the target device*/
	int targetPort = -1;			 /**< port of the target device*/

  public:
	//* A constructor.
	BCTicketResponse();
	//* A constructor.
	BCTicketResponse(int, puf_t *);
	
	BCTicketResponse *setCarnetLength(int len) { this->carnetLength = len; return this; }
	BCTicketResponse *setCarnet(puf_t *, int);
	BCTicketResponse *setTargetHost(string);
	BCTicketResponse *setTargetPort(int p){ this->targetPort = p; return this; }

	int getCarnetLength() const { return this->carnetLength; }
	puf_t *getCarnet() const { return this->carnet; }
	puf_t *getBody() const { return NULL; }
	string getTargetHost() { return this->targetHost; }
	int getTargetPort() { return this->targetPort; }
	
	void serialize(uint8_t *) const;
	void deserialize(uint8_t *);

	friend std::ostream &operator<<(std::ostream &, const BCTicketResponse &);

	//* A Destructor.
	virtual ~BCTicketResponse();
};

#endif