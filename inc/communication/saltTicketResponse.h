//===- saltTicketResponse.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file saltTicketResponse.h
/// \author Erasmo La Montagna
/// \brief This file defines the header of the SaltTicketResponse class
//===----------------------------------------------------------------------===//

#ifndef _SALT_TICKET_RES_
#define _SALT_TICKET_RES_

#include <iostream>
#include "messageSkeleton.h"
#include "string.h"
#include "puf.h"

namespace phemap
{
class SaltTicketResponse;
};

using namespace std;

class phemap::SaltTicketResponse : public phemap::MessageSkeleton
{

  protected:
	int carnetLength;				 /**< carnet length*/
	int pufLength;					 /**< Puf length*/
	puf_t *carnet = NULL;			 /**< pointer to the carnet attribute*/
	string targetHost = "localhost"; /**< address of the target device*/
	int targetPort = -1;			 /**< port of the target device*/

  public:
	//* A constructor.
	SaltTicketResponse();

	SaltTicketResponse *setCarnetLength(int len){ this->carnetLength = len; return this; }
	SaltTicketResponse *setCarnet(puf_t *, int);
	SaltTicketResponse *setTargetHost(string);
	SaltTicketResponse *setTargetPort(int p){ this->targetPort = p; return this; }

	int getCarnetLength() { return this->carnetLength; }
	puf_t *getCarnet() const { return this->carnet; }
	string getTargetHost() { return this->targetHost; }
	int getTargetPort() { return this->targetPort; }
	int getPufLength(void) const { return pufLength; }

	void serialize(uint8_t *) const;
	void deserialize(uint8_t *);

	friend std::ostream &operator<<(std::ostream &, const SaltTicketResponse &);

	//* A Destructor.
	~SaltTicketResponse();
};

#endif /* _SALT_TICKET_RES_ */