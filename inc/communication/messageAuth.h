//===- messageAuth.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file messageAuth.h
/// \author Andrea Aletto
/// \brief This file defines the header of the MessageAuth class
//===----------------------------------------------------------------------===//
#ifndef message_auth_h
#define message_auth_h
#include <iostream>
#include <string.h>
#include "phemap.h"
#include "puf.h"
#include "messageSkeleton.h"

#define BODY_BYTE_SIZE 32

namespace phemap
{
class MessageAuth;
}

using namespace std;

class phemap::MessageAuth : public phemap::MessageSkeleton
{
  protected:
	puf_t *li; /**< pointer to the Li attribute*/
	int pufLength; /**< Puf length*/
	string body; /**< body of the message*/
	int bodyLength = 0; /**< body length of the message*/

  public:
	//* A constructor.
	MessageAuth();
	//* A constructor.
	MessageAuth(int);
	//* A constructor.
	MessageAuth(const puf_t *, int);
	//* A constructor.
	MessageAuth(const puf_t *, int, string);

	puf_t *getLi(void) const;
	void getLi(puf_t *) const;
	void setLi(const puf_t *) const;

	int getPufLength(void) const;

	string getBody();
	void setBody(string);
	bool bodyIsEmpty();
	int getBodyLength() const;

	void serialize(uint8_t *) const;
	void deserialize(uint8_t *);

	friend std::ostream &operator<<(std::ostream &, const MessageAuth &);

 	//* A Destructor.
	virtual ~MessageAuth();
};

#endif /* messageAuth_h */
