//===- saltInstall.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file saltInstall.h
/// \author Erasmo La Montagna
/// \brief This file defines the header of the saltInstall class
//===----------------------------------------------------------------------===//

#ifndef _SALT_INSTALL_
#define _SALT_INSTALL_

#include "messageSkeleton.h"
#include "messageChangeAddress.h"
#include "string.h"
using namespace std;

namespace phemap{
	class SaltInstall;
};

class phemap::SaltInstall : public phemap::MessageChangeAddress{

	protected:
		puf_t *salt; /**< pointer to the salt attribute*/
		int ticketCounter = 0; /**< ticket counter*/

	public:
		//* A constructor.
		SaltInstall();

		SaltInstall* setSalt(puf_t *s) { memcpy(this->salt, s, this->pufLength); return this; }
		SaltInstall* setTicketCounter(int c) { this->ticketCounter = c; return this; }
		void setAddress(string);

		puf_t* getSalt() const{ return this->salt; }
		int getTicketCounter() { return this->ticketCounter;}
		puf_t* getBody() { return NULL; }

		void serialize(uint8_t *) const;
		void deserialize(uint8_t *);

		friend std::ostream &operator<<(std::ostream &, const SaltInstall &);

		//* A Destructor.
		~SaltInstall(); 

};


#endif