//===- message1.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file message1.h
/// \author Mario Barbareschi
/// \brief This file defines the header of the Message1 class
//===----------------------------------------------------------------------===//

#ifndef message1_h
#define message1_h

#include <iostream>

#include "phemap.h"
#include "puf.h"
#include "utils.h"
#include "messageSkeleton.h"

namespace phemap{
    class Message1;
}

class phemap::Message1 : public phemap::MessageSkeleton{
protected:
    puf_t *li; /**< pointer to the Li attribute*/
    puf_t *v1; /**< pointer to the V1 attribute*/
    puf_t *v2; /**< pointer to the V1 attribute*/
    int pufLength;  /**< Puf length*/
public:
    //* A constructor.
    Message1 ();
    //* A constructor.
    Message1 (int);
    //* A constructor.
    Message1 (puf_t*, int);
    Message1* setLi(puf_t*);
    Message1* setV1(puf_t*);
    Message1* setV2(puf_t*);
    puf_t* getLi(void) const;
    puf_t* getV1(void) const;
    puf_t* getV2(void) const;
    void getLi(puf_t*) const;
    void getV1(puf_t*) const;
    void getV2(puf_t*) const;
    int getPufLength(void) const;
    int serialize(puf_t*) const;
    bool deserialize(puf_t*, int);

    friend std::ostream& operator<<(std::ostream&, const Message1&);

    //* A Destructor.
    virtual ~Message1 ();
};

#endif /* message1_h */
