//===- bcTicketRequest.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file bcTicketRequest.h
/// \author Andrea Aletto
/// \brief This file defines the header of the BCTicketRequest class
//===----------------------------------------------------------------------===//

#ifndef _BC_ICKET_REQ_
#define _BC_ICKET_REQ_

#include "messageSkeleton.h"
#include "messageAuth.h"
#include "string.h"
using namespace std;

namespace phemap{
	class BCTicketRequest;
};

class phemap::BCTicketRequest : public phemap::MessageAuth{

	protected:
		int requestedTickets; /**< tickets to request*/
		puf_t *targetID; /**< pointer to the targetID attribute*/

	public:

		//* A constructor.
		/** 
		 * Constructor used by the device.
		 */
		BCTicketRequest(int, puf_t*, puf_t*);	

		//* A constructor.
		/**
		 * Constructor used by the verifier.
		 */
		BCTicketRequest();				
		BCTicketRequest* setRequestedTickets(int len){ this->requestedTickets = len; return this; }
		BCTicketRequest* setTargetID(puf_t *id) { memcpy(this->targetID, id, this->pufLength); return this; }
		int getRequestedTickets(){ return this->requestedTickets; }
		puf_t* getTargetID() const{ return this->targetID; }
		puf_t* getBody() { return NULL; }
		void serialize(uint8_t *) const;
		void deserialize(uint8_t *);

		friend std::ostream &operator<<(std::ostream &, const BCTicketRequest &);

		//* A Destructor.
		virtual ~BCTicketRequest(); 

};


#endif