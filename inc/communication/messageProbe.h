//===- messageProbe.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file messageProbe.h
/// \author Andrea Aletto
/// \brief This file defines the header of the MessageProbe class
//===----------------------------------------------------------------------===//

#ifndef messageProbe_h
#define messageProbe_h

#include <iostream>
#include "string.h"
#include "messageSkeleton.h"
#include "puf.h"
using namespace std;

namespace phemap{
    class MessageProbe;
}

class phemap::MessageProbe : public phemap::MessageSkeleton{
protected:
    string deviceId; /**< Device Id*/
public:
    //* A constructor.
	MessageProbe() { this->deviceId = ""; this->flag = phemap::PROBE; this->length = sizeof(flag_t); }
    //* A constructor.
    MessageProbe(string deviceId) { this->deviceId=deviceId; this->flag=phemap::PROBE; this->length = sizeof(flag_t) + deviceId.size(); };

    string getDeviceId(void) const { return deviceId; };
    void serialize(uint8_t*) const;
    void deserialize(uint8_t*, int);

    friend std::ostream& operator<<(std::ostream&, const MessageProbe&);

    //* A Destructor.
    virtual ~MessageProbe () {};
};

#endif /* messageProbe_h */
