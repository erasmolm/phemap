//===- messageChangeAddress.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file messageChangeAddress.h
/// \author Erasmo La Montagna
/// \brief This file defines the header of the MessageChangeAddress class
//===----------------------------------------------------------------------===//

#ifndef _MESSAGE_CHANGE_ADDRESS_
#define _MESSAGE_CHANGE_ADDRESS_

#include "messageSkeleton.h"
#include "messageAuth.h"
#include "string.h"
using namespace std;

namespace phemap{
	class MessageChangeAddress;
};

class phemap::MessageChangeAddress : public phemap::MessageAuth{

	protected: 
		int port = -1; /**< port of the proxy to set*/
		string address = "localhost"; /**< address of the proxy to set*/

	public:
		//* A constructor.
		MessageChangeAddress(); 			

		int getPort(void) {return this->port; }
		string getAddress(void) {return this->address; }

		MessageChangeAddress* setPort(int p) { port = p; return this; }
		void setAddress(string);

		void serialize(uint8_t *) const;
		void deserialize(uint8_t *);

		friend std::ostream &operator<<(std::ostream &, const MessageChangeAddress &);

	    //* A Destructor.
		virtual ~MessageChangeAddress(){}; 

};


#endif /* _MESSAGE_CHANGE_ADDRESS_ */