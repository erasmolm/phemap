//===- saltTicketRequest.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file saltTicketRequest.h
/// \author Erasmo La Montagna
/// \brief This file defines the header of the SaltTicketRequest class
//===----------------------------------------------------------------------===//

#ifndef _SALT_TICKET_REQ_
#define _SALT_TICKET_REQ_

#include <iostream>
#include "messageSkeleton.h"
#include "puf.h"
#include <string>

using namespace std;

namespace phemap{
	class SaltTicketRequest;
};


class phemap::SaltTicketRequest : public phemap::MessageSkeleton{

	protected:
		int requestedTickets;
		string originID; /**< origin Id*/
		string targetID; /**< target Id*/
		string originHost = "localhost"; /**< originn host*/
		int originPort = -1; /**< origin port*/


	public:
		//* A constructor.
		SaltTicketRequest();

		SaltTicketRequest* setRequestedTickets(int len){ this->requestedTickets = len; return this; }
		SaltTicketRequest* setOriginID(string);
		SaltTicketRequest* setTargetID(string);
		SaltTicketRequest* setOriginHost(string);
		SaltTicketRequest* setOriginPort(int);

		int getRequestedTickets(){ return this->requestedTickets; }
		string getOriginID() const{ return this->originID; }
		string getTargetID() const{ return this->targetID; }
		string getOriginHost() const{ return this->originHost; }
		int getOriginPort() const{ return this->originPort; }
		
		void serialize(uint8_t *) const;
		void deserialize(uint8_t *);

		friend std::ostream &operator<<(std::ostream &, const SaltTicketRequest &);

		//* A Destructor.
		~SaltTicketRequest() {};

};


#endif