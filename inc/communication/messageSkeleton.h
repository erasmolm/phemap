//===- messageSkeleton.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file messageSkeleton.h
/// \author Erasmo La Montagna
/// \brief This file defines the header of the MessageSkeleton class
//===----------------------------------------------------------------------===//

#ifndef MESSAGESKELETON_H
#define MESSAGESKELETON_H

#include <inttypes.h>

typedef uint8_t flag_t;

namespace phemap
{
class MessageSkeleton;

enum MSG_FLG
{
	INIT_M1, /**< Enum value INIT_M1 */
	INIT_M2, /**< Enum value INIT_M2 */
	INIT_M3, /**< Enum value INIT_M3 */
	VER_STR, /**< Enum value VER_STR */
	PROBE, /**< Enum value PROBE */
	BC_IM_TCK_REQ, /**< Enum value BC_IM_TCK_REQ */
	BC_EX_TCK_REQ, /**< Enum value BC_EX_TCK_REQ */
	BC_TCK_RES, /**< Enum value BC_TCK_RES */
	CHANGE_ADDR, /**< Enum value CHANGE_ADDR */
	UNSET_PROXY, /**< Enum value UNSET_PROXY */
	SALT_INSTALL, /**< Enum value SALT_INSTALL */
	SALT_TCK_REQ, /**< Enum value SALT_TCK_REQ */
	SALT_TCK_RES /**< Enum value SALT_TCK_RES */
};
} // namespace phemap

class phemap::MessageSkeleton
{

  protected:
	flag_t flag; /**< flag of the message*/
	int length = 0; /**< length of the message*/

  public:
  //* A constructor.
  	MessageSkeleton(){};
	flag_t getFlag() { return this->flag; };
	void setFlag(flag_t flag) { this->flag = flag; };
	int getLength() const { return this->length; };

	//* A Destructor.
	~MessageSkeleton(){};
};

#endif /* messageSkeleton_h */
