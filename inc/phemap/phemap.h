//
//  phemap.h
//  phemap
//
//  Created by Mario Barbareschi on 27/04/17.
//  Copyright © 2017 Mario Barbareschi. All rights reserved.
//

#ifndef phemap_h
#define phemap_h

#include "puf.h"

#define DEFAULT_SENTINEL 4
#define DEFAULT_PUF_LENGTH 16

#define BC_BOUND_DURATION_MS 1

namespace phemap
{
enum DeviceState
{
	DISCONNECTED, /**< Enum value DISCONNECTED */
	INITIATED, /**< Enum value INITIATED */
	AUTHENTICATED, /**< Enum value AUTHENTICATED */
	UNKNOWN, /**< Enum value UNKNOWN */
	UNSAFE, /**< Enum value UNSAFE */
	ERROR /**< Enum value ERROR */
};
}

#endif /* phemap_h */
