//===- deviceManager.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file deviceManager.h
/// \author Mario Barbareschi
/// \brief This file defines the header of the DeviceManager class
//===----------------------------------------------------------------------===//

#ifndef deviceManager_h
#define deviceManager_h

#include "phemap.h"
#include "deviceProxy.h"

namespace phemap
{
class DeviceManager;
}

class phemap::DeviceManager
{
  protected:
	phemap::DeviceProxy *deviceProxy; /**< pointer to the deviceProxy attribute*/

  public:
	//* A constructor.
	DeviceManager(){};
	DeviceManager *setDeviceProxy(phemap::DeviceProxy *);
	DeviceProxy *getDeviceProxy() {return this->deviceProxy; };
	bool PHEMAP_startInitiation(void);
	bool PHEMAP_startVerification(string);
	bool PHEMAP_verificationFromChain(const puf_t *, puf_t *);
	bool SALT_PHEMAP_verificationFromChain(const puf_t *, puf_t *);
	bool SALT_PHEMAP_startVerification(string);

	void getNextPufLink(puf_t *, bool);
	void peekNextPufLink(puf_t *, bool);

  private:
	void PHEMAP_initiation_phase1(Message1 *);
	bool PHEMAP_initiation_phase3(const Message2 *, Message3 *);
};

#endif /* deviceManager_h */
