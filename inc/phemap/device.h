//===- device.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file device.h
/// \author Mario Barbareschi
/// \brief This file defines the header of the Device class
//===----------------------------------------------------------------------===//

#ifndef device_h
#define device_h

#include "deviceSkeleton.h"
#include "verifierProxy.h"
#include "deviceManager.h"
#include "saltTicketRequest.h"

namespace phemap
{
class Device;

enum authModeFlag
{
	AMODE_NORMAL, /**< Enum value AMODE_NORMAL */
	AMODE_BC_IMPLICIT, /**< Enum value AMODE_BC_IMPLICIT */
	AMODE_BC_EXPLICIT, /**< Enum value AMODE_BC_EXPLICIT */
	AMODE_SALTED /**< Enum value AMODE_SALTED */
};
} // namespace phemap

class phemap::Device : public phemap::DeviceSkeleton
{
  private:
	int BCTickets = 0; /**< Babel chain ticket*/
	DeviceManager *BCTargetManager = NULL; /**< pointer to the BCTargetManager attribute*/

  protected:
	phemap::Puf *puf; /**< pointer to the Puf attribute*/
	puf_t *storage; /**< pointer to the storage attribute*/
	puf_t * salt; /**< pointer to the salt attribute*/
	int saltCounter = 0; /**< ticket counter*/
	VerifierProxy *verifierProxy; /**< pointer to the verifierProxy attribute*/

  public:
	authModeFlag authenticationMode = phemap::AMODE_NORMAL;
	//* A constructor.
	Device(std::string, phemap::Puf *);
	//* A constructor.
	Device(std::string, phemap::Puf *, int);
	phemap::Puf *getPuf(void);
	phemap::Device *setPuf(phemap::Puf *);
	void setBCTickets(int v) { this->BCTickets = v; }
	void setBCTargetManager(DeviceManager *m) { this->BCTargetManager = m; }
	VerifierProxy *getVerifierProxy() { return this->verifierProxy; }
	void getNextPufLink(puf_t *, bool);
	int getBCTickets() { return this->BCTickets; }
	DeviceManager *getBCTargetManager() { return this->BCTargetManager; }
	bool verifyCRP(const puf_t *, const puf_t *);
	bool verifyCRPWithSentinel(const puf_t *, const puf_t *);
	void flushStorage();
	void flushSalt();
	void setSalt(puf_t*);
	puf_t *getSalt(void) {return this->salt; }
	void setSaltCounter(int c) {this->saltCounter = c; }
	int getSaltCounter() {return this->saltCounter; }
	phemap::Device *setStorage(puf_t *);
	bool PHEMAP_initiation_phase2(const phemap::Message1 *, phemap::Message2 *);
	bool PHEMAP_initiation_phase4(const phemap::Message3 *);
	void PHEMAP_sendProbe();
	bool PHEMAP_startVerification(string);
	bool PHEMAP_deviceAuthentication(MessageAuth *, MessageAuth *);
	int BC_PHEMAP_ticketRequest(phemap::MSG_FLG, string, int);
	bool BC_PHEMAP_startVerification(string);
	bool SALT_PHEMAP_startVerification(string);
	void SALT_PHEMAP_ticketRequestFromDevice(SaltTicketRequest *req);

	//* A Destructor.
	~Device();
};

#endif /* device_h */
