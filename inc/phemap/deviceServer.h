//===- deviceServer.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file deviceServer.h
/// \author Mario Barbareschi
/// \brief This file defines the header of the DeviceServer class
//===----------------------------------------------------------------------===//


#ifndef deviceServer_h
#define deviceServer_h

#include "device.h"
#include "string.h"
using namespace std;

namespace phemap{
    class DeviceServer;
}

class phemap::DeviceServer{
	private:
		string deviceHost = "localhost"; /**< device host*/
		int devicePort = -1; /**< device port*/
protected:
    phemap::Device* device; /**< pointer to the Li attribute*/
public:
    //* A constructor.
    DeviceServer() {};
    //* A constructor.
    DeviceServer (phemap::Device*);
    DeviceServer* setDevice(Device*);
	DeviceServer* setDeviceHost(string host) { this->deviceHost = host; return this; }
	DeviceServer* setDevicePort(int port) {this->devicePort = port; return this; }
	string getDeviceHost()  { return this->deviceHost; }
	int getDevicePort() { return this->devicePort; }
    void runDeviceServer();

    //* A Destructor.
    ~DeviceServer ();
};

#endif /* deviceServer_h */
