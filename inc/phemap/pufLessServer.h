//===- pufLessServer.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file pufLessServer.h
/// \author Erasmo La Montagna
/// \brief This file defines the header of the PufLessServer class
//===----------------------------------------------------------------------===//


#ifndef pufLessServer_h
#define pufLessServer_h

#include "pufLess.h"
#include "string.h"
using namespace std;

namespace phemap{
    class PufLessServer;
}

class phemap::PufLessServer{
	private:
		string pufLessHost = "localhost"; /**< PufLess host*/
		int pufLessPort = -1; /**< PuffLess port*/
protected:
    phemap::PufLess* pufLess; /**< pointer to the pufLess attribute*/
public:
	//* A constructor.
    PufLessServer() {};
    PufLessServer* setPufLess(PufLess*);
	PufLessServer* setPufLessHost(string host) { this->pufLessHost = host; return this; }
	PufLessServer* setPufLessPort(int port) {this->pufLessPort = port; return this; }
	string getPufLessHost()  { return this->pufLessHost; }
	int getPufLessPort() { return this->pufLessPort; }
    void runPufLessServer();

	//* A Destructor.
    ~PufLessServer ();
};

#endif /* pufLessServer_h */
