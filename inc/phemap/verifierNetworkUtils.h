//===- verifierNetworkUtils.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file verifierNetworkUtils.h
/// \author Andrea Aletto
/// \brief This file defines the declaration of the VerifierNetworkUtils class
//===----------------------------------------------------------------------===//

#ifndef verifierNetworkUtils_h
#define verifierNetworkUtils_h


#include "utils.h"
#include <iostream>
#include <iomanip>
#include <assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "verifierSkeleton.h"
#include "deviceSkeleton.h"
#include "deviceException.h"
#include "string.h"
using namespace std;

namespace phemap
{
class VerifierNetworkUtils;
}

class phemap::VerifierNetworkUtils
{

  private:
	string verifierHost = "localhost"; /**< verifier host*/
	int verifierPort = -1; /**< verifier port*/

  public:
	//* A constructor.
  	VerifierNetworkUtils(){};
  	VerifierNetworkUtils* setVerifierHost(string host) { this->verifierHost = host; return this; }
	VerifierNetworkUtils* setVerifierPort(int port) { this->verifierPort = port; return this; }
	string getVerifierHost() {return this->verifierHost; }
	int getVerifierPort() { return this->verifierPort; }
	int createListenerSocket();
	int acceptConnectionFrom(const int);
	int createSocketToDevice(string, int);
	int receiveFromDevice(int, uint8_t *, int);
	int sendToDevice(int, uint8_t *, int);
	void closeSocket(int);
};

#endif