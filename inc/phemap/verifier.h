//===- verifier.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file verifier.h
/// \author Mario Barbareschi
/// \brief This file defines the header of the Verifier class
//===----------------------------------------------------------------------===//

#ifndef verifier_h
#define verifier_h

#include <iostream>
#include <string>
#include <map>
#include <tuple>
#include <unordered_map>
#include <vector>
#include <chrono>

#include "phemap.h"
#include "deviceManager.h"
#include "verifierSkeleton.h"

#define BC_MAX_TCK 5000
#define SALT_MAX_TCK 5000

typedef unordered_map<string, phemap::DeviceManager *> DevicesMap_t;
typedef tuple<string, string, chrono::milliseconds> BCBound_t;
typedef vector<BCBound_t> BabelMap_t;

namespace phemap
{
class Verifier;
}

class phemap::Verifier : public phemap::VerifierSkeleton
{
  protected:
	DevicesMap_t *devicesMap; /**< pointer to the devicesMap attribute*/
	BabelMap_t *babelMap; /**< pointer to the babelMap attribute*/

  public:
	//* A constructor.
	Verifier(int CRByteLength);
	DevicesMap_t *getDevicesMap(void);
	BabelMap_t *getBabelMap(void);
	void BC_addToBabelMap(string, string);

	int BC_searchBound(string);
	bool BC_hasExpired(int);
	bool PHEMAP_startInitiation(string);
	bool PHEMAP_startVerification(string, string);
	bool PHEMAP_verifierAuthentication(const string, MessageAuth *, MessageAuth *);

	//* A Destructor.
	~Verifier();
};

#endif /* verifier_h */
