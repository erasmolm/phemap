//===- deviceProxy.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file deviceProxy.h
/// \author Mario Barbareschi
/// \brief This file defines the header of the DeviceProxy class
//===----------------------------------------------------------------------===//

#ifndef deviceProxy_h
#define deviceProxy_h

#include "deviceSkeleton.h"
#include "pufChain.h"
#include "verifierNetworkUtils.h"

namespace phemap{
    class DeviceProxy;
}

class phemap::DeviceProxy : public phemap::DeviceSkeleton{
protected:
    phemap::PufChain* pufChain; /**< pointer to the pufChain attribute*/
	int devicePort = -1; /**< device port*/
	string deviceHost = "localhost"; /**< device host*/
	bool directMode = false; /**< direct mode flag */
	int directSocket = 0; /**< socket descriptor for direct mode */
public:
    //* A constructor.
    DeviceProxy (std::string, string, int, phemap::PufChain*);
    //* A constructor.
    DeviceProxy (std::string, std::string, int, phemap::PufChain*, int);
    phemap::PufChain* getPufChain(void);
	void setDeviceHost(string host) {this->deviceHost = host; }
	string getDeviceHost(){ return this->deviceHost; }
	void setDevicePort(int port){ this->devicePort = port; }
	int getDevicePort(){ return this->devicePort; }
    void getNextPufLink(puf_t*, bool);
    void setPufChain(phemap::PufChain*);
	void setDirectMode(bool m){ this->directMode = m;}
	void setDirectSocket(int s){ this->directSocket = s;}
	bool getDirectMode(){return this->directMode; }
	int getDirectSocket(){return this->directSocket; }
    bool PHEMAP_initiation_phase2(const phemap::Message1*, phemap::Message2*);
    bool PHEMAP_initiation_phase4(const phemap::Message3*);
	bool PHEMAP_deviceAuthentication(MessageAuth*, MessageAuth*);
	bool PHEMAP_deviceAuthentication(MessageChangeAddress*, MessageAuth*);
	bool PHEMAP_deviceAuthentication(SaltInstall*, MessageAuth*);

	
    //~DeviceProxy ();

};

#endif /* deviceProxy_h */
