//===- pufLess.h ----------------------------------------------------*- C++ -*-
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file pufLess.h
/// \author Erasmo La Montagna
/// \brief This file defines the header of the PufLess class
//===----------------------------------------------------------------------===//

#ifndef pufless_h
#define pufless_h

#include <unordered_map>
#include <string>
#include "deviceManager.h"
#include "saltVerifierProxy.h"

typedef unordered_map<string, phemap::DeviceManager *> DevicesMap_t;
typedef unordered_map<string, int> SaltMap_t;


namespace phemap
{
class PufLess;
}

class phemap::PufLess
{
  protected:
	DevicesMap_t *devicesMap; /**< pointer to the devicesMap attribute*/
	SaltMap_t *saltMap; /**< pointer to the saltMap attribute*/
	SaltVerifierProxy *saltVerifierProxy; /**< pointer to the saltVerifierProxy attribute*/
	string nodeID; /**< node Id*/

  public:
	//* A constructor.
	PufLess(string);

	SaltMap_t *getSaltMap(void) { return this->saltMap; }
	DevicesMap_t *getDevicesMap(void) { return this->devicesMap; }
	SaltVerifierProxy *getSaltVerifierProxy() { return this->saltVerifierProxy; }

	PufLess *setNodeID(string id) { this->nodeID = id; return this; }
	string getNodeID(void) { return this->nodeID; }

	bool PHEMAP_verifierAuthentication(const string, MessageAuth *, MessageAuth *);
	int SALT_PHEMAP_ticketRequest(string, int, string, int);
	bool SALT_PHEMAP_startVerification(string, string);

	//* A Destructor.
	~PufLess();
};

#endif /* pufless_h */
