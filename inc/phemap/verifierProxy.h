//===- verifierProxy.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file verifierProxy.h
/// \author Mario Barbareschi
/// \brief This file defines the header of the DeviceProxy class
//===----------------------------------------------------------------------===//

#ifndef verifierProxy_h
#define verifierProxy_h

#include "verifierSkeleton.h"
#include "messageProbe.h"
#include "bcTicketRequest.h"
#include "bcTicketResponse.h"
#include "phemap.h"

namespace phemap
{
class VerifierProxy;
}

class phemap::VerifierProxy : phemap::VerifierSkeleton
{
  private:
	string verifierHost = "localhost"; /**< verifier host*/
	int verifierPort = -1; /**< verifier port*/
	string deviceID = ""; /**< device Id*/
	string proxyAddress = "localhost"; /**< proxy address*/
	int proxyPort = -1; /**< proxy port*/

  public:
	bool useProxy = false;
	//* A constructor.
	VerifierProxy(int CRByteLength);
	VerifierProxy *setVerifierHost(string host)
	{
		this->verifierHost = host;
		return this;
	}
	VerifierProxy *setVerifierPort(int port)
	{
		this->verifierPort = port;
		return this;
	}
	VerifierProxy *setDeviceID(string id)
	{
		this->deviceID = id;
		return this;
	}
	VerifierProxy *setProxyPort(int p)
	{
		this->proxyPort = p;
		return this;
	}
	VerifierProxy *setProxyAddress(string a)
	{
		this->proxyAddress = a;
		return this;
	}
	string getVerifierHost() { return this->verifierHost; }
	int getVerifierPort() { return this->verifierPort; }

	string getProxyAddress() { return this->proxyAddress; }
	int getProxyPort() { return this->proxyPort; }

	string getDeviceID() { return this->deviceID; }
	void PHEMAP_sendProbe(const MessageProbe *);
	bool PHEMAP_verifierAuthentication(const std::string, MessageAuth *, MessageAuth *);
	bool BC_PHEMAP_ticketRequest(const string, const BCTicketRequest *, BCTicketResponse *);
};

#endif /* verifierProxy_h */
