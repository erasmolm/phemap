//===- deviceNetworkUtils.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file deviceNetworkUtils.h
/// \author Andrea Aletto
/// \brief This file defines the declaration of the DeviceNetworkUtils class
//===----------------------------------------------------------------------===//
#ifndef deviceNetworkUtils_h
#define deviceNetworkUtils_h

#include "utils.h"
#include <iostream>
#include <iomanip>
#include <assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <iostream>
#include <iomanip>
#include <arpa/inet.h>
#include "verifierSkeleton.h"
#include "deviceSkeleton.h"


namespace phemap
{
class DeviceNetworkUtils;
}

class phemap::DeviceNetworkUtils
{

  private:
  	string deviceHost = "localhost"; /**< device host*/
	int devicePort = -1; /**< device port*/
	string verifierHost = "localhost"; /**< verifier host*/
	int verifierPort = -1; /**< verifier port*/
	string deviceID = ""; /**< device Id*/
	

  public:
	//* A constructor.
	DeviceNetworkUtils(){};
  	DeviceNetworkUtils *setVerifierHost(string host) { this->verifierHost = host; return this; }
	DeviceNetworkUtils *setVerifierPort(int port) { this->verifierPort = port; return this; }
	DeviceNetworkUtils *setDeviceID(string id) { this->deviceID = id; return this; }
	DeviceNetworkUtils* setDeviceHost(string host) { this->deviceHost = host; return this; }
	DeviceNetworkUtils* setDevicePort(int port) {this->devicePort = port; return this; }
	string getVerifierHost() {return this->verifierHost; }
	int getVerifierPort() { return this->verifierPort; }
	string getDeviceHost()  { return this->deviceHost; }
	int getDevicePort() { return this->devicePort; }
	string getDeviceID() { return this->deviceID; }

	int createListenerSocket();
	int acceptConnectionFrom(int);
	int createSocketToVerifier();
	int receiveFromVerifier(int, uint8_t *, int);
	int sendToVerifier(int, uint8_t *, int);
	void closeSocket(int);
};

#endif