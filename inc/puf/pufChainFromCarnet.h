//===- pufChainFromCarnet.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file pufChain.h
/// \author Erasmo La Montagna
/// \brief This file defines the header of the PufChainFromCarnet class
//===----------------------------------------------------------------------===//

#ifndef PufChainFromCarnet_h
#define PufChainFromCarnet_h

#include "puf.h"
#include "pufChain.h"

namespace phemap
{
class PufChainFromCarnet;
};

class phemap::PufChainFromCarnet : public PufChain
{
  private:
	int carnetLength; /**< carnet length*/
	puf_t *carnet; /**< pointer to the carnet attribute*/

  public:
	//* A constructor.
	PufChainFromCarnet(puf_t *, int, int);
	bool getNextLink(puf_t *);
	bool peekLink(puf_t *, int);

	//* A decostructor.
	virtual ~PufChainFromCarnet();
};

#endif /* PufChainFromCarnet_h */
