//===- phemapException.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file phemapException.h
/// \author Andrea Aletto
/// \brief This file defines the declaration of the PHEMAP_Exception class
//===----------------------------------------------------------------------===//
#ifndef _phemap_exception_h_
#define _phemap_exception_h_

#include <iostream>
#include <exception>
#include <cstring>
using namespace std;

namespace phemap {
	class PHEMAP_Exception;
}

class phemap::PHEMAP_Exception: public exception
{
	protected:
		string errcode = ""; /**< error message*/
		string errdesc = "Unknown PHEMAP exception"; /**< error message*/

	public:
		//* A constructor.
		PHEMAP_Exception() {};
		const string getErrcode() const { return errcode; }
		const string getErrdesc() const { return errdesc; }
		void setErrcode(string s) { this->errcode = s; }
		void setErrdesc(string s) { this->errdesc = s; }
		virtual const char* what() const throw() = 0;

		//* A Destructor.
		~PHEMAP_Exception() {};
  
};

#endif