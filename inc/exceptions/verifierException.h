//===- verifierException.h ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file verifierException.h
/// \author Andrea Aletto
/// \brief This file defines the declaration of the PHEMAP_Verifier_Exception class
//===----------------------------------------------------------------------===//

#ifndef _verifier_exception_h_
#define _verifier_exception_h_

#include "phemapException.h"

namespace phemap {
	class PHEMAP_Verifier_Exception;
}

class phemap::PHEMAP_Verifier_Exception : public phemap::PHEMAP_Exception {

	public:
		//* A constructor.
		PHEMAP_Verifier_Exception(string errcode, string errdesc) {this->errcode=errcode; this->errdesc=errdesc;}
		const char* what() const throw() { return "\nPHEMAP Verifier Exception. Use cout or cerr for details."; }
		friend std::ostream& operator<<(std::ostream& os, const PHEMAP_Verifier_Exception& e) { os << "\n[ERROR] Verifier " << "caused an exception " <<e.getErrcode() << ": " <<e.getErrdesc(); return os; }
};

#endif