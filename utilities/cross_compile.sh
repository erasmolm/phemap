#!/bin/bash

cd ~/phemap

rm -Rf cross
mkdir cross
cd cross

cmake .. -DCMAKE_TOOLCHAIN_FILE=../cross-arm-linux-gnueabihf.cmake
make pufLessMain
#make deviceMain
