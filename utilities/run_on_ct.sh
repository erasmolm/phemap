#!/bin/bash

VER_IP=52.47.192.210
CT_IP=192.168.1.103
VER_PT=8765
PL_PT=8766

NUM_OF_DEVICES=$1

set -x

rm -rf ~/phemap
mkdir phemap
cd $HOME/phemap

tar -xf ~/puf.tar -C ~/phemap
#rm ~/puf.tar

export LD_LIBRARY_PATH=./src/communication:./src/phemap:./src/puf

tmux kill-server
tmux new -d -s gateway ./pufLessMain -l $CT_IP -p $PL_PT -v $VER_IP -f $VER_PT

for ((J=0; j<$NUM_OF_DEVICES; j++))
do
	tmux send-keys -t gateway.0 -l "" -sr " " device_ $((j + 1)) " " 100 
	tmux send-keys -t gateway.0 ENTER
done

