#!/bin/bash

NUM_OF_DEVICES=100
BASE_PORT=9876
NODES=2000
IP="192.168.0.107"

cd ../build

for ((j=0; j<$NUM_OF_DEVICES; j++))
do
	./chainMaker -d $'device_'"$((j + 1))" -n $NODES >> /dev/null
	echo "device_$((j + 1)) $IP $((BASE_PORT + j))" >> ../resources/deviceList.txt
done
