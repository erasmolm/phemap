#!/bin/bash

NUM_OF_DEVICES=$1
BASE_PORT=9876
V_HOST="52.47.192.210"
V_PORT=8765

set -x

rm -f ../log/*
cd ../build

tmux kill-server
#tmux new -s verifier ./verifierMain -v $V_HOST -f $V_PORT

for ((j=0; j<$NUM_OF_DEVICES; j++))
do

	sleep 0.2s
	tmux new -d -s $'device_'"$((j + 1))" ./deviceMain -d $'device_'"$((j + 1))" -p $((BASE_PORT + j)) -v $V_HOST -f $V_PORT

done
