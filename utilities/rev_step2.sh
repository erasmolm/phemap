#!/bin/bash

NUM_OF_DEVICES=$1
BASE_PORT=9876
V_HOST="192.168.1.67"
V_PORT=8765

set -x

r=$2

for ((j=0; j<$NUM_OF_DEVICES; j++))
do

	tmux send-keys -t $'device_'"$((j + 1))".0 -l "" "testr" " " $r
	tmux send-keys -t $'device_'"$((j + 1))".0 ENTER
done
