//===- message1.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file message1.cpp
/// \author Mario Barbareschi
/// \brief This file defines the implementation of the Message1 class
//===----------------------------------------------------------------------===//

#include "message1.h"
#include <iomanip>
#include <cstring>

/**
 * @brief	Constructor for Message1.
 * @param 	None
 * @retval 	None
*/
phemap::Message1::Message1()
{
	pufLength = DEFAULT_PUF_LENGTH;
	flag = phemap::INIT_M1;
	li = (puf_t *)malloc(pufLength * sizeof(puf_t));
	v1 = (puf_t *)malloc(pufLength * sizeof(puf_t));
	v2 = (puf_t *)malloc(pufLength * sizeof(puf_t));

	this->length = sizeof(li) + sizeof(v1) + sizeof(v2) + sizeof(flag);
}

/**
 * @brief	Constructor for Message1.
 * @param 	pufLength
 * @retval 	None
*/
phemap::Message1::Message1(int pufLength)
{
	this->pufLength = pufLength;
	flag = phemap::INIT_M1;
	li = (puf_t *)malloc(pufLength * sizeof(puf_t));
	v1 = (puf_t *)malloc(pufLength * sizeof(puf_t));
	v2 = (puf_t *)malloc(pufLength * sizeof(puf_t));

	this->length = sizeof(li) + sizeof(v1) + sizeof(v2) + sizeof(flag);

}

/**
 * @brief	Constructor for Message1 from serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @param	pufLength 
 * @retval 	None
*/
phemap::Message1::Message1(puf_t *buffer, int pufLength)
{
	this->pufLength = pufLength;
	flag = phemap::INIT_M1;
	li = (puf_t *)malloc(pufLength * sizeof(puf_t));
	v1 = (puf_t *)malloc(pufLength * sizeof(puf_t));
	v2 = (puf_t *)malloc(pufLength * sizeof(puf_t));
	this->length = sizeof(li) + sizeof(v1) + sizeof(v2) + sizeof(flag);

	memcpy(li, buffer, pufLength);
	memcpy(v1, buffer + pufLength, pufLength);
	memcpy(v2, buffer + 2 * pufLength, pufLength);
}

/**
 * @brief	Set the Li value for the Message1.	
 * @param 	Li: pointer to the value to write to the Li attribute.
 * @retval 	this
*/
phemap::Message1 *phemap::Message1::setLi(puf_t *li)
{
	memcpy(this->li, li, pufLength);
	return this;
}

/**
 * @brief	Set the V1 value for the Message1.
 * @param 	V1: pointer to the value to write to the V1 attribute.
 * @retval 	this
*/
phemap::Message1 *phemap::Message1::setV1(puf_t *v1)
{
	memcpy(this->v1, v1, pufLength);
	return this;
}

/**
 * @brief	Set the V2 value for the Message1.
 * @param 	V2:  pointer to the value to write to the V2 attribute.
 * @retval 	this
*/
phemap::Message1 *phemap::Message1::setV2(puf_t *v2)
{
	memcpy(this->v2, v2, pufLength);
	return this;
}

/**
 * @brief	Returns the Puf length.
 * @param 	None
 * @retval 	pufLength
*/
int phemap::Message1::getPufLength() const
{
	return pufLength;
}

/**
 * @brief	Returns a pointer to the Li attribute.
 * @param 	None
 * @retval 	Li field of the message.
*/
puf_t *phemap::Message1::getLi() const
{
	return li;
}

/**
 * @brief	Returns a pointer to the V1 attribute.
 * @param 	None
 * @retval 	V1 field of the message.
*/
puf_t *phemap::Message1::getV1() const
{
	return v1;
}

/**
 * @brief	Returns a pointer to the V2 attribute.
 * @param 	None
 * @retval 	V2 field of the message.
*/
puf_t *phemap::Message1::getV2() const
{
	return v2;
}

/**
 * @brief	Copy a Puf link from the Li field to the given pointer.
 * @param 	Li: pointer that will contain the Li field value.
 * @retval 	None
*/
void phemap::Message1::getLi(puf_t *li) const
{
	memcpy(li, this->li, pufLength);
}

/**
 * @brief	Copy a Puf link to the V1 field to the given pointer.
 * @param 	V1: pointer that will contain the V1 field value.
 * @retval 	None
*/
void phemap::Message1::getV1(puf_t *v1) const
{
	memcpy(v1, this->v1, pufLength);
}

/**
 * @brief	Copy a Puf link to the V2 field to the given pointer.
 * @param 	V2: pointer that will contain the V2 field value.
 * @retval 	None
*/
void phemap::Message1::getV2(puf_t *v2) const
{
	memcpy(v2, this->v2, pufLength);
}

/**
 * @brief	Copy class attributes to serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @retval 	size of the message.
*/
int phemap::Message1::serialize(puf_t *buffer) const
{
	memcpy(buffer, &flag, sizeof(flag_t));
	memcpy(buffer + sizeof(flag_t), li, pufLength);
	memcpy(buffer + pufLength + sizeof(flag_t), v1, pufLength);
	memcpy(buffer + 2 * pufLength + sizeof(flag_t), v2, pufLength);
	return 3 * pufLength + sizeof(flag_t);
}

/**
 * @brief	Copy class attributes from the serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @param	length
 * @retval 	true if length equals buffer length, false otherwise.
*/
bool phemap::Message1::deserialize(puf_t *buffer, int length)
{
	if (length != 3 * pufLength + sizeof(flag_t))
	{
		return false;
	}
	memcpy(li, buffer + sizeof(flag_t), pufLength);
	memcpy(v1, buffer + pufLength + sizeof(flag_t), pufLength);
	memcpy(v2, buffer + 2 * pufLength + sizeof(flag_t), pufLength);
	return true;
}

/**
 * @brief	Overloading of the << operator.
 * @param 	os: reference to the output stream.
 * @param	message1: Message1 instance.
 * @retval 	Output stream.
*/
std::ostream &phemap::operator<<(std::ostream &os, const phemap::Message1 &message1)
{
	os << "Message1 (";
	int i;
	for (i = 0; i < message1.getPufLength(); i++)
	{
		os << std::hex << std::setw(2) << std::uppercase << std::setfill('0') << static_cast<int>(*(message1.getLi() + i));
	}
	os << ", ";
	for (i = 0; i < message1.getPufLength(); i++)
	{
		os << std::hex << std::setw(2) << std::uppercase << std::setfill('0') << static_cast<int>(*(message1.getV1() + i));
	}
	os << ", ";
	for (i = 0; i < message1.getPufLength(); i++)
	{
		os << std::hex << std::setw(2) << std::uppercase << std::setfill('0') << static_cast<int>(*(message1.getV2() + i));
	}
	os << ");";
	return os;
}

/**
 * @brief	Destructor for Message1.
 * @param 	None
 * @retval 	None
*/
phemap::Message1::~Message1()
{
	free(li);
	free(v1);
	free(v2);
}
