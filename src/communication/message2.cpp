//===- message2.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file message2.cpp
/// \author Mario Barbareschi
/// \brief This file defines the implementation of the Message2 class
//===----------------------------------------------------------------------===//

#include "message2.h"
#include <iomanip>
#include <cstring>


/**
 * @brief	Constructor for Message2.
 * @param 	None
 * @retval 	None
*/
phemap::Message2::Message2()
{
	pufLength = DEFAULT_PUF_LENGTH;
	flag = phemap::INIT_M2;
	d1 = (puf_t *)malloc(pufLength * sizeof(puf_t));
	d2 = (puf_t *)malloc(pufLength * sizeof(puf_t));
	this->length = sizeof(d1) + sizeof(d2) + sizeof(flag);
}

/**
 * @brief	Constructor for Message2.
 * @param 	pufLength
 * @retval 	None
*/
phemap::Message2::Message2(int pufLength)
{
	this->pufLength = pufLength;
	flag = phemap::INIT_M2;
	d1 = (puf_t *)malloc(pufLength * sizeof(puf_t));
	d2 = (puf_t *)malloc(pufLength * sizeof(puf_t));
	this->length = sizeof(d1) + sizeof(d2) + sizeof(flag);
}

/**
 * @brief	Constructor for Message2 from serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @param	pufLength 
 * @retval 	None
*/
phemap::Message2::Message2(puf_t *buffer, int pufLength)
{
	this->pufLength = pufLength;
	flag = phemap::INIT_M2;
	d1 = (puf_t *)malloc(pufLength * sizeof(puf_t));
	d2 = (puf_t *)malloc(pufLength * sizeof(puf_t));
	this->length = sizeof(d1) + sizeof(d2) + sizeof(flag);

	memcpy(d1, buffer, pufLength);
	memcpy(d2, buffer + pufLength, pufLength);
}

/**
 * @brief	Set the D1 value for the Message2.	
 * @param 	D1: pointer to the value to write to the D1 attribute.
 * @retval 	this
*/
phemap::Message2 *phemap::Message2::setD1(puf_t *d1)
{
	memcpy(this->d1, d1, pufLength);
	return this;
}

/**
 * @brief	Set the D2 value for the Message2.	
 * @param 	D2: pointer to the value to write to the D2 attribute.
 * @retval 	this
*/
phemap::Message2 *phemap::Message2::setD2(puf_t *d2)
{
	memcpy(this->d2, d2, pufLength);
	return this;
}

/**
 * @brief	Returns the Puf length.
 * @param 	None
 * @retval 	pufLength
*/
int phemap::Message2::getPufLength() const
{
	return pufLength;
}

/**
 * @brief	Copy class attributes to serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @retval 	size of the message.
*/
int phemap::Message2::serialize(puf_t *buffer) const
{
	memcpy(buffer, &flag, sizeof(flag_t));
	memcpy(buffer + sizeof(flag_t), d1, pufLength);
	memcpy(buffer + sizeof(flag_t) + pufLength, d2, pufLength);
	return 2 * pufLength + sizeof(flag_t);
}

/**
 * @brief	Copy class attributes from the serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @param	length
 * @retval 	true if length equals buffer length, false otherwise.
*/
bool phemap::Message2::deserialize(puf_t *buffer, int length)
{
	if (length != 2 * pufLength + sizeof(flag_t))
	{
		return false;
	}
	memcpy(d1, buffer + sizeof(flag_t), pufLength);
	memcpy(d2, buffer + sizeof(flag_t) + pufLength, pufLength);
	return true;
}

/**
 * @brief	Returns a pointer to the D1 attribute.
 * @param 	None
 * @retval 	D1 field of the message.
*/
puf_t *phemap::Message2::getD1() const
{
	return d1;
}

/**
 * @brief	Returns a pointer to the D2 attribute.
 * @param 	None
 * @retval 	D2 field of the message.
*/
puf_t *phemap::Message2::getD2(void) const
{
	return d2;
}

/**
 * @brief	Copy a Puf link from the D1 field to the given pointer.
 * @param 	D1: pointer that will contain the D1 field value.
 * @retval 	None
*/
void phemap::Message2::getD1(puf_t *d1) const
{
	memcpy(d1, this->d1, pufLength);
}

/**
 * @brief	Copy a Puf link from the D2 field to the given pointer.
 * @param 	D2: pointer that will contain the D2 field value.
 * @retval 	None
*/
void phemap::Message2::getD2(puf_t *d2) const
{
	memcpy(d2, this->d2, pufLength);
}

/**
 * @brief	Overloading of the << operator.
 * @param 	os: reference to the output stream.
 * @param	message2: Message2 instance.
 * @retval 	Output stream.
*/
std::ostream &phemap::operator<<(std::ostream &os, const phemap::Message2 &message2)
{
	os << "Message2 (";
	int i;
	for (i = 0; i < message2.getPufLength(); i++)
	{
		os << std::hex << std::setw(2) << std::uppercase << std::setfill('0') << static_cast<int>(*(message2.getD1() + i));
	}
	os << ", ";
	for (i = 0; i < message2.getPufLength(); i++)
	{
		os << std::hex << std::setw(2) << std::uppercase << std::setfill('0') << static_cast<int>(*(message2.getD2() + i));
	}
	os << ");";
	return os;
}

/**
 * @brief	Destructor for Message2.
 * @param 	None
 * @retval 	None
*/
phemap::Message2::~Message2()
{
	free(d1);
	free(d2);
}
