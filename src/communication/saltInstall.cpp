//===- saltInstall.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file saltInstall.cpp
/// \author Erasmo La Montagna
/// \brief This file defines the implementation of the SaltInstall class
//===----------------------------------------------------------------------===//

#include "saltInstall.h"
#include <iomanip>
#include <iostream>

/**
 * @brief	Constructor for SaltInstall.
 * @param 	None
 * @retval 	None
*/
phemap::SaltInstall::SaltInstall() : phemap::MessageChangeAddress()
{
	this->flag = phemap::SALT_INSTALL;
	this->ticketCounter = 0;
	this->salt = (puf_t *)malloc(this->pufLength);
	this->length = sizeof(flag_t) + 2 * sizeof(int) + 2 * this->pufLength + address.size() + 1;
}

/**
 * @brief 	Set the Address value for the SaltInstall.
 * @param	a: address to assign.
 * @retval 	None
 */
void phemap::SaltInstall::setAddress(string a)
{
	this->address = a;
	this->length = sizeof(flag_t) + 2 * sizeof(int) + 2 * this->pufLength + address.size() + 1;
}

/**
 * @brief	Copy class attributes to serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @retval 	None
*/
void phemap::SaltInstall::serialize(uint8_t *buffer) const
{
	memcpy(buffer, &flag, sizeof(flag_t));
	memcpy(buffer + sizeof(flag_t), this->li, this->pufLength);
	memcpy(buffer + sizeof(flag_t) + this->pufLength, &port, sizeof(int));
	memcpy(buffer + sizeof(flag_t) + this->pufLength + sizeof(int), address.c_str(), address.size() + 1);
	memcpy(buffer + sizeof(flag_t) + this->pufLength + sizeof(int) + address.size() + 1, &ticketCounter, sizeof(int));
	memcpy(buffer + sizeof(flag_t) + this->pufLength + sizeof(int) + address.size() + 1 + sizeof(int), this->salt, pufLength);

}

/**
 * @brief	Copy class attributes from the serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @retval 	None
*/
void phemap::SaltInstall::deserialize(uint8_t *buffer)
{
	memcpy(this->li, buffer + sizeof(flag_t), pufLength);
	memcpy(&port, buffer + sizeof(flag_t) + pufLength, sizeof(int));
	address = string((const char *)(buffer + sizeof(flag_t) + pufLength + sizeof(int)));

	memcpy(&ticketCounter, buffer + sizeof(flag_t) + pufLength + sizeof(int) + address.size() + 1, sizeof(int));
	memcpy(this->salt, buffer + sizeof(flag_t) + pufLength + 2 * sizeof(int) + address.size() + 1, pufLength);

	this->length = sizeof(flag_t) + 2 * sizeof(int) + 2 * this->pufLength + address.size() + 1;

}

/**
 * @brief	Overloading of the << operator.
 * @param 	os: reference to the output stream.
 * @param	msg: SaltInstall instance.
 * @retval 	Output stream.
*/
std::ostream &phemap::operator<<(std::ostream &os, const phemap::SaltInstall &msg)
{
	os << "SaltInstall (";

	int i;
	for (i = 0; i < msg.getPufLength(); i++)
	{
		os << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(*(msg.getLi() + i));
	}
	os << "   -   ";
	for (i = 0; i < msg.getPufLength(); i++)
	{
		os << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(*(msg.getSalt() + i));
	}
	os << "   -   ";
	os << std::dec << msg.ticketCounter;

	os << ");";
	os << "\nAddress: " << msg.address;
	os << "\nPort: " << std::dec << msg.port;
	return os;
}

/**
 * @brief	Destructor for SaltInstall.
 * @param 	None
 * @retval 	None
*/
phemap::SaltInstall::~SaltInstall()
{
	free(this->salt);
	free(this->li);
}
