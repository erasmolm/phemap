//===- saltTicketResponse.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file saltTicketResponse.cpp
/// \author Erasmo La Montagna
/// \brief This file defines the implementation of the SaltTicketResponse class
//===----------------------------------------------------------------------===//

#include <iostream>
#include <iomanip>
#include "saltTicketResponse.h"
#include "puf.h"
#include "phemap.h"

using namespace std;

/**
 * @brief	Constructor for SaltTicketResponse.
 * @param 	None
 * @retval 	None
*/
phemap::SaltTicketResponse::SaltTicketResponse()
{
	this->flag = phemap::SALT_TCK_RES;
	this->pufLength = DEFAULT_PUF_LENGTH;
	this->carnetLength = 0;
	this->length = sizeof(flag_t) + sizeof(int) + targetHost.size() + 1 + sizeof(int);
}

/**
 * @brief	Set the carnet value for the SaltTicketResponse.
 * @param 	carnet: pointer to the value to write to the carnet attribute.
 * @param 	carnetLength
 * @retval 	this
*/
phemap::SaltTicketResponse *phemap::SaltTicketResponse::setCarnet(puf_t *carnet, int carnetLength)
{
	this->carnetLength = carnetLength;
	if (this->carnet == NULL)
	{
		this->carnet = (puf_t *)malloc(carnetLength * pufLength);
	}
	memcpy(this->carnet, carnet, carnetLength * pufLength);
	this->length = sizeof(flag_t) + sizeof(int) + (pufLength * carnetLength)+ targetHost.size() + 1 + sizeof(int);
	return this;
}

/**
 * @brief 	Set the targetHost value for the SaltTicketResponse.
 * @param	a: address to assign.
 * @retval 	None
 */
phemap::SaltTicketResponse *phemap::SaltTicketResponse::setTargetHost(string a)
{
	this->targetHost = a;
	this->length = sizeof(flag_t) + sizeof(int) + (pufLength * carnetLength)+ targetHost.size() + 1 + sizeof(int);

	return this;
}

/**
 * @brief	Copy class attributes to serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @retval 	None
*/
void phemap::SaltTicketResponse::serialize(uint8_t *buffer) const
{
	memcpy(buffer, &flag, sizeof(flag_t));
	memcpy(buffer + sizeof(flag_t), &carnetLength, sizeof(int));
	memcpy(buffer + sizeof(flag_t) + sizeof(int), this->carnet, carnetLength * pufLength);
	memcpy(buffer + sizeof(flag_t) + sizeof(int) + (carnetLength * pufLength), &targetPort, sizeof(int));
	memcpy(buffer + sizeof(flag_t) + sizeof(int) + (carnetLength * pufLength) + sizeof(int), targetHost.c_str(), targetHost.size() + 1);

}

/**
 * @brief	Copy class attributes from the serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @retval 	None 
*/
void phemap::SaltTicketResponse::deserialize(uint8_t *buffer)
{
	memcpy(&carnetLength, buffer + sizeof(flag_t), sizeof(int));
	this->carnet = (puf_t *)malloc(carnetLength * pufLength);
	memcpy(this->carnet, buffer + sizeof(flag_t) + sizeof(int), carnetLength * pufLength);
	memcpy(&targetPort, buffer + sizeof(flag_t) + sizeof(int) + (carnetLength * pufLength), sizeof(int));
	this->targetHost = string((const char *)(buffer + sizeof(flag_t) + sizeof(int) + (carnetLength * pufLength) + sizeof(int)));

	this->length = sizeof(flag_t) + sizeof(int) + (pufLength * carnetLength)+ targetHost.size() + 1 + sizeof(int);
}

/**
 * @brief	Overloading of the << operator.
 * @param 	os: reference to the output stream.
 * @param	msg: SaltTicketResponse instance.
 * @retval 	Output stream.
*/
std::ostream &phemap::operator<<(std::ostream &os, const phemap::SaltTicketResponse &msg)
{
	os << "SaltTicketResponse (";
	os << std::dec << msg.carnetLength;
	os << ");";
	os << endl;
	
	os << "\nTickets released: \n";
	for (int i = 0; i < msg.carnetLength; i++)
	{
		os << "T" << i+1 <<": ";
		for (int j = 0; j < msg.getPufLength(); j++)
		{
			os << std::hex << std::setw(2) << std::uppercase << std::setfill('0') << static_cast<int>(*(msg.carnet + j + i*msg.pufLength));
		}
		os << std::dec << endl;
	}

	os << "\nTargetHost: " << msg.targetHost;
	os << "\nTargetPort: " << std::dec << msg.targetPort;

	return os;
}

/**
 * @brief	Destructor for SaltTicketResponse.
 * @param 	None
 * @retval 	None
*/
phemap::SaltTicketResponse::~SaltTicketResponse()
{
	free(this->carnet);
}