//===- messageAuth.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file messageAuth.cpp
/// \author Andrea Aletto
/// \brief This file defines the implementation of the MessageAuth class
//===----------------------------------------------------------------------===//

#include "messageAuth.h"
#include <iomanip>
#include <iostream>

using namespace std;

/**
 * @brief 	MessageAuth constructor.
 * @param 	None
 * @retval 	None
 */
phemap::MessageAuth::MessageAuth()
{
	this->pufLength = DEFAULT_PUF_LENGTH;
	flag = phemap::VER_STR;
	li = (puf_t *)malloc(pufLength * sizeof(puf_t));
	this->bodyLength = 0;
	this->length = pufLength * sizeof(puf_t) + sizeof(flag_t);
}

/**
 * @brief 	MessageAuth constructor.
 * @param 	pufLength: length of the puf to be set.
 * @retval 	None
 */
phemap::MessageAuth::MessageAuth(int pufLength)
{
	this->pufLength = pufLength;
	flag = phemap::VER_STR;
	li = (puf_t *)malloc(pufLength * sizeof(puf_t));
	this->bodyLength = 0;
	this->length = pufLength * sizeof(puf_t) + sizeof(flag_t);
}

/**
 * @brief 	Constructor for MessageAuth from serial buffer.
 * @param 	link: pointer to the serial buffer.
 * @param	pufLength
 * @retval 	None
 */
phemap::MessageAuth::MessageAuth(const puf_t *link, int pufLength)
{
	this->pufLength = pufLength;
	flag = phemap::VER_STR;
	li = (puf_t *)malloc(pufLength * sizeof(puf_t));
	this->bodyLength = 0;
	this->length = pufLength * sizeof(puf_t) + sizeof(flag_t);

	memcpy(li, link, pufLength);
}

/**
 * @brief 	MessageAuth constructor. 
 * @param 	link: pointer to the serial buffer.
 * @param	pufLength
 * @param	body: body of the message.
 * @retval 	None
 */
phemap::MessageAuth::MessageAuth(const puf_t *link, int pufLength, string body)
{
	this->pufLength = pufLength;
	flag = phemap::VER_STR;
	li = (puf_t *)malloc(pufLength * sizeof(puf_t));
	this->body = body;
	this->bodyLength = body.size() + 1;
	this->length = pufLength * sizeof(puf_t) + sizeof(flag_t) + bodyLength;

	memcpy(li, link, pufLength);
}

/**
 * @brief	Returns the Puf length.
 * @param 	None
 * @retval 	pufLength
*/
int phemap::MessageAuth::getPufLength() const
{
	return pufLength;
}

/**
 * @brief 	Returns the body.
 * @param	None
 * @retval 	Value of the body field.
 */
string phemap::MessageAuth::getBody()
{
	return this->body;
}

/**
 * @brief 	Set the body value for the MessageAuth.
 * @param	body: body to assign.
 * @retval 	None
 */
void phemap::MessageAuth::setBody(string body)
{
	this->body = body;
	this->bodyLength = body.size() + 1;
	this->length = pufLength * sizeof(puf_t) + sizeof(flag_t) + bodyLength;
}

/**
 * @brief 	Returns true if body is empty, false otherwise.
 * @param	None
 * @retval 	bool
 */
bool phemap::MessageAuth::bodyIsEmpty()
{
	return this->body.empty();
}

/**
 * @brief 	Returns the body length.
 * @param	None
 * @retval 	Value of the bodyLength field.
 */
int phemap::MessageAuth::getBodyLength() const
{
	return this->bodyLength;
}

/**
 * @brief	Copy class attributes to serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @retval 	None
*/
void phemap::MessageAuth::serialize(uint8_t *buffer) const
{
	memcpy(buffer, &flag, sizeof(flag_t));
	memcpy(buffer + sizeof(flag_t), this->li, this->pufLength);
	memcpy(buffer + sizeof(flag_t) + this->pufLength, body.c_str(), this->bodyLength);

}

/**
 * @brief	Copy class attributes from the serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @retval 	None
*/
void phemap::MessageAuth::deserialize(uint8_t *buffer)
{
	memcpy(&flag, buffer, sizeof(flag_t));
	memcpy(this->li, buffer + sizeof(flag_t), pufLength);
	this->body = string((const char*)(buffer + sizeof(flag_t) + pufLength*sizeof(puf_t)));
	this->bodyLength = body.size() + 1;
	this->length = pufLength * sizeof(puf_t) + sizeof(flag_t) + bodyLength;

}

/**
 * @brief 	Returns a pointer to the Li attribute.
 * @param	None
 * @retval 	Li field of the message.
 */
puf_t *phemap::MessageAuth::getLi() const
{
	return li;
}

/**
 * @brief 	Set the Li value for the MessageAuth.	
 * @param	Li: pointer to the value to write to the Li attribute.
 * @retval 	None
 */
void phemap::MessageAuth::setLi(const puf_t *li) const
{
	memcpy(this->li, li, this->pufLength);
}

/**
 * @brief	Overloading of the << operator.
 * @param 	os: reference to the output stream.
 * @param	MessageAuth: MessageAuth instance.
 * @retval 	Output stream.
*/
std::ostream &phemap::operator<<(std::ostream &os, const phemap::MessageAuth &MessageAuth)
{
	os << "MessageAuth (";
	int i;
	for (i = 0; i < MessageAuth.getPufLength(); i++)
	{
		os << std::hex << std::setw(2) << std::uppercase << std::setfill('0') << static_cast<int>(*(MessageAuth.getLi() + i));
	}
	os << "   -   ";
	os << "\033[1;33m" << MessageAuth.body << "\033[0m";
	os << ");";
	return os;
}

/**
 * @brief 	MessageAuth destructor.
 * @param 	None
 * @retval 	None
 */
phemap::MessageAuth::~MessageAuth()
{
	free(li);
}
