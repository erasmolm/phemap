//===- bcTicketRequest.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file bcTicketRequest.cpp
/// \author Andrea Aletto
/// \brief This file defines the implementation of the BCTicketRequest class
//===----------------------------------------------------------------------===//

#include "bcTicketRequest.h"
#include <iomanip>
#include <iostream>

/**
 * @brief	Constructor for BCTicketRequest.
 * @param 	requestedTickets
 * @param	targetID: must be xored with a puf link.
 * @param 	Li: pointer to the authentication puf link. 
 * @retval 	None
*/
phemap::BCTicketRequest::BCTicketRequest(int requestedTickets, puf_t* targetID, puf_t* li) : phemap::MessageAuth(){
	this->requestedTickets = requestedTickets;
	memcpy(this->li, li, this->pufLength);
	this->targetID = (puf_t*)malloc(this->pufLength);
	memcpy(this->targetID, targetID, this->pufLength);
	this->flag = phemap::BC_IM_TCK_REQ;
	this->length = sizeof(flag_t) + sizeof(int) + this->pufLength*2; 
}

/**
 * @brief	Constructor for BCTicketRequest.
 * @param 	None
 * @retval 	None
*/
phemap::BCTicketRequest::BCTicketRequest() : phemap::MessageAuth(){
	this->requestedTickets = 0;
	this->targetID = (puf_t*)malloc(this->pufLength);
	this->flag = phemap::BC_IM_TCK_REQ;
	this->length = sizeof(flag_t) + sizeof(int) + this->pufLength*2; 
}

/**
 * @brief	Copy class attributes to serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @retval 	None
*/
void phemap::BCTicketRequest::serialize(uint8_t *buffer) const{

	memcpy(buffer, &flag, sizeof(flag_t));
	memcpy(buffer + sizeof(flag_t), this->li, this->pufLength);
	memcpy(buffer + sizeof(flag_t) + this->pufLength, &requestedTickets, sizeof(int));
	memcpy(buffer + sizeof(flag_t) + this->pufLength + sizeof(int), this->targetID, pufLength);
}

/**
 * @brief	Copy class attributes from the serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @retval 	None 
*/
void phemap::BCTicketRequest::deserialize(uint8_t *buffer){
	
	memcpy(this->li, buffer + sizeof(flag_t), pufLength);
	memcpy(&requestedTickets, buffer + sizeof(flag_t) + pufLength, sizeof(int));
	memcpy(this->targetID, buffer + sizeof(flag_t) + pufLength + sizeof(int), pufLength);
}

/**
 * @brief	Overloading of the << operator.
 * @param 	os: reference to the output stream.
 * @param	msg: BCTicketRequest instance.
 * @retval 	Output stream.
*/
std::ostream &phemap::operator<<(std::ostream &os, const phemap::BCTicketRequest &msg)
{
	os << "BCTicketRequest (";
	int i;
	for (i = 0; i < msg.getPufLength(); i++)
	{
		os << std::hex << std::setw(2) << std::uppercase << std::setfill('0') << static_cast<int>(*(msg.getLi() + i));
	}
	os << "   -   ";
	for (i = 0; i < msg.getPufLength(); i++)
	{
		os << std::hex << std::setw(2) << std::uppercase << std::setfill('0') << static_cast<int>(*(msg.getTargetID() + i));
	}
	os << "   -   ";
	os << std::dec << msg.requestedTickets;

	os << ");";
	return os;
}

/**
 * @brief	Destructor for BCTicketRequest.
 * @param 	None
 * @retval 	None
*/
phemap::BCTicketRequest::~BCTicketRequest(){
	free(this->targetID);
}