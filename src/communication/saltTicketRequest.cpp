//===- saltTicketRequest.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file saltTicketRequest.cpp
/// \author Erasmo La Montagna
/// \brief This file defines the implementation of the SaltTicketRequest class
//===----------------------------------------------------------------------===//

#include "saltTicketRequest.h"
#include <iomanip>
#include <string.h>

using namespace std;

/**
 * @brief	Constructor for SaltTicketRequest.
 * @param 	None
 * @retval 	None
*/
phemap::SaltTicketRequest::SaltTicketRequest()
{
	this->requestedTickets = 0;
	this->flag = phemap::SALT_TCK_REQ;
	this->length = sizeof(flag_t) + 2 * sizeof(int) + targetID.size() + originID.size() + originHost.size() + 3;
}

/**
 * @brief	Set the original ID value for the SaltTicketRequest.
 * @param 	ID
 * @retval 	Pointer to this.
*/
phemap::SaltTicketRequest *phemap::SaltTicketRequest::setOriginID(string id)
{
	this->originID = id;
	this->length = sizeof(flag_t) + 2 * sizeof(int) + targetID.size() + originID.size() + originHost.size() + 3;
	return this;
}

/**
 * @brief	Set the target ID value for the SaltTicketRequest.
 * @param 	ID
 * @retval 	this
*/
phemap::SaltTicketRequest *phemap::SaltTicketRequest::setTargetID(string id)
{
	this->targetID = id;
	this->length = sizeof(flag_t) + 2 * sizeof(int) + targetID.size() + originID.size() + originHost.size() + 3;
	return this;
}

/**
 * @brief	Set the original host value for the SaltTicketRequest.
 * @param 	h: Host of the originator.
 * @retval 	this
*/
phemap::SaltTicketRequest *phemap::SaltTicketRequest::setOriginHost(string h)
{
	this->originHost = h;
	this->length = sizeof(flag_t) + 2 * sizeof(int) + targetID.size() + originID.size() + originHost.size() + 3;
	return this;
}

/**
 * @brief	Set the original port value for the SaltTicketRequest.
 * @param 	p: Port of the originator.
 * @retval 	this
*/
phemap::SaltTicketRequest *phemap::SaltTicketRequest::setOriginPort(int p)
{
	this->originPort = p;
	this->length = sizeof(flag_t) + 2 * sizeof(int) + targetID.size() + originID.size() + originHost.size() + 3;
	return this;
}

/**
 * @brief	Copy class attributes to serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @retval  None
*/
void phemap::SaltTicketRequest::serialize(uint8_t *buffer) const
{

	memcpy(buffer, &flag, sizeof(flag_t));
	memcpy(buffer + sizeof(flag_t), originID.c_str(), originID.size() + 1);
	memcpy(buffer + sizeof(flag_t) + originID.size() + 1, originHost.c_str(), originHost.size() + 1);
	memcpy(buffer + sizeof(flag_t) + originID.size() + originHost.size() + 2, &originPort, sizeof(int));
	memcpy(buffer + sizeof(flag_t) + originID.size() + originHost.size() + 2 + sizeof(int), targetID.c_str(), targetID.size() + 1);
	memcpy(buffer + sizeof(flag_t) + originID.size() + originHost.size() + targetID.size() + 3 + sizeof(int), &requestedTickets, sizeof(int));

}

/**
 * @brief	Copy class attributes from the serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @param	length
 * @retval 	None
*/
void phemap::SaltTicketRequest::deserialize(uint8_t *buffer)
{

	this->originID = string((const char *)(buffer + sizeof(flag_t)));
	this->originHost = string((const char *)(buffer + sizeof(flag_t) + originID.size() + 1));
	memcpy(&originPort, buffer + sizeof(flag_t) + originID.size() + originHost.size() + 2, sizeof(int));
	this->targetID = string((const char *)(buffer + sizeof(flag_t) + originID.size() + originHost.size() + 2 + sizeof(int)));
	memcpy(&requestedTickets, buffer + sizeof(flag_t) + originID.size() + originHost.size() + targetID.size() + 3 + sizeof(int), sizeof(int));
	this->length = sizeof(flag_t) + 2 * sizeof(int) + targetID.size() + originID.size() + originHost.size() + 3;

}

/**
 * @brief	Overloading of the << operator.
 * @param 	os: reference to the output stream.
 * @param	msg: SaltTicketRequest instance.
 * @retval 	Output stream.
*/
std::ostream &phemap::operator<<(std::ostream &os, const phemap::SaltTicketRequest &msg)
{
	os << "SaltTicketRequest (";
	os << msg.originID;
	os << " --> ";
	os << msg.targetID;
	os << "   -   ";
	os << std::dec << msg.requestedTickets;
	os << ");";
	os << "\noriginHost: " << msg.getOriginHost();
	os << "\noriginPort: " << std::dec << msg.getOriginPort();
	return os;
}