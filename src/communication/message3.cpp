//===- message3.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file message3.cpp
/// \author Mario Barbareschi
/// \brief This file defines the implementation of the Message3 class
//===----------------------------------------------------------------------===//

#include "message3.h"
#include <iomanip>
#include <cstring>

/**
 * @brief	Constructor for Message3.
 * @param 	None
 * @retval 	None
*/
phemap::Message3::Message3()
{
	pufLength = DEFAULT_PUF_LENGTH;
	flag = phemap::INIT_M3;
	li = (puf_t *)malloc(pufLength * sizeof(puf_t));
	v3 = (puf_t *)malloc(pufLength * sizeof(puf_t));
	this->length = sizeof(li) + sizeof(v3) + sizeof(flag);
}

/**
 * @brief	Constructor for Message3.
 * @param 	pufLength
 * @retval 	None
*/
phemap::Message3::Message3(int pufLength)
{
	this->pufLength = pufLength;
	flag = phemap::INIT_M3;
	li = (puf_t *)malloc(pufLength * sizeof(puf_t));
	v3 = (puf_t *)malloc(pufLength * sizeof(puf_t));
	this->length = sizeof(li) + sizeof(v3) + sizeof(flag);
}

/**
 * @brief	Constructor for Message3 from serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @param	pufLength 
 * @retval 	None
*/
phemap::Message3::Message3(puf_t *buffer, int pufLength)
{
	this->pufLength = pufLength;
	flag = phemap::INIT_M3;
	li = (puf_t *)malloc(pufLength * sizeof(puf_t));
	v3 = (puf_t *)malloc(pufLength * sizeof(puf_t));
	this->length = sizeof(li) + sizeof(v3) + sizeof(flag);

	memcpy(li, buffer, pufLength);
	memcpy(v3, buffer + pufLength, pufLength);
}

/**
 * @brief	Set the Li value for the Message3.	
 * @param 	Li: pointer to the value to write to the Li attribute.
 * @retval 	this
*/
phemap::Message3 *phemap::Message3::setLi(puf_t *li)
{
	memcpy(this->li, li, pufLength);
	return this;
}

/**
 * @brief	Set the V3 value for the Message3.
 * @param 	V3: pointer to the value to write to the V3 attribute.
 * @retval 	this
*/
phemap::Message3 *phemap::Message3::setV3(puf_t *v3)
{
	memcpy(this->v3, v3, pufLength);
	return this;
}

/**
 * @brief	Returns the Puf length.
 * @param 	None
 * @retval 	pufLength
*/
int phemap::Message3::getPufLength() const
{
	return pufLength;
}

/**
 * @brief	Copy class attributes to serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @retval 	size of the message.
*/
int phemap::Message3::serialize(puf_t *buffer) const
{
	memcpy(buffer, &flag, sizeof(flag_t));
	memcpy(buffer + sizeof(flag_t), li, pufLength);
	memcpy(buffer + sizeof(flag_t) + pufLength, v3, pufLength);
	return 2 * pufLength + sizeof(flag_t);
}

/**
 * @brief	Copy class attributes from the serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @param	length
 * @retval 	true if length equals buffer length, false otherwise.
*/
bool phemap::Message3::deserialize(puf_t *buffer, int length)
{
	if (length != 2 * pufLength + sizeof(flag_t))
	{
		return false;
	}
	memcpy(li, buffer + sizeof(flag_t), pufLength);
	memcpy(v3, buffer + sizeof(flag_t) + pufLength, pufLength);
	return true;
}

/**
 * @brief	Returns a pointer to the Li attribute.
 * @param 	None
 * @retval 	Li field of the message.
*/
puf_t *phemap::Message3::getLi() const
{
	return li;
}

/**
 * @brief	Returns a pointer to the V3 attribute.
 * @param 	None
 * @retval 	V3 field of the message.
*/
puf_t *phemap::Message3::getV3(void) const
{
	return v3;
}

/**
 * @brief	Copy a Puf link from the Li field to the given pointer.
 * @param 	Li: pointer that will contain the Li field value.
 * @retval 	None
*/
void phemap::Message3::getLi(puf_t *li) const
{
	memcpy(li, this->li, pufLength);
}

/**
 * @brief	Copy a Puf link from the V3 field to the given pointer.
 * @param 	V3: pointer that will contain the V3 field value.
 * @retval 	None
*/
void phemap::Message3::getV3(puf_t *v3) const
{
	memcpy(v3, this->v3, pufLength);
}

/**
 * @brief	Overloading of the << operator.
 * @param 	os: reference to the output stream.
 * @param	message3: Message3 instance.
 * @retval 	Output stream
*/
std::ostream &phemap::operator<<(std::ostream &os, const phemap::Message3 &Message3)
{
	os << "Message3 (";
	int i;
	for (i = 0; i < Message3.getPufLength(); i++)
	{
		os << std::hex << std::setw(2) << std::uppercase << std::setfill('0') << static_cast<int>(*(Message3.getLi() + i));
	}
	os << ", ";
	for (i = 0; i < Message3.getPufLength(); i++)
	{
		os << std::hex << std::setw(2) << std::uppercase << std::setfill('0') << static_cast<int>(*(Message3.getV3() + i));
	}
	os << ");";
	return os;
}

/**
 * @brief	Destructor for Message3.
 * @param 	None
 * @retval 	None
*/
phemap::Message3::~Message3()
{
	free(li);
	free(v3);
}
