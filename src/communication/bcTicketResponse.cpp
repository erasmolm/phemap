//===- bcTicketResponse.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file bcTicketResponse.cpp
/// \author Andrea Aletto
/// \brief This file defines the implementation of the BCTicketResponse class
//===----------------------------------------------------------------------===//

#include "bcTicketResponse.h"
#include <iomanip>
#include <iostream>
#include "utils.h"

/**
 * @brief	Constructor for BCTicketResponse.
 * @param 	None
 * @retval 	None
*/
phemap::BCTicketResponse::BCTicketResponse() : phemap::MessageAuth()
{
	this->flag = phemap::BC_TCK_RES;
	this->carnetLength = 0;
	this->length = sizeof(flag_t) + sizeof(int) + this->pufLength + targetHost.size() + 1 + sizeof(int);
}

/**
 * @brief	Constructor for BCTicketResponse from serial buffer.
 * @param 	carnetLength
 * @param	carnet: pointer to the value to write to the carnet attribute.
 * @retval 	None
*/
phemap::BCTicketResponse::BCTicketResponse(int carnetLength, puf_t *carnet) : phemap::MessageAuth()
{
	this->flag = phemap::BC_TCK_RES;
	this->carnetLength = carnetLength;
	this->carnet = (puf_t *)malloc(this->pufLength * carnetLength);
	memcpy(this->carnet, carnet, this->pufLength * carnetLength);
	this->length = sizeof(flag_t) + sizeof(int) + this->pufLength * (1 + carnetLength) + targetHost.size() + 1 + sizeof(int);
}

/**
 * @brief	Set the carnet value for the BCTicketResponse.	
 * @param 	carnet: pointer to the value to write to the carnet attribute.
 * @param	carnetLength
 * @retval 	this
*/
phemap::BCTicketResponse* phemap::BCTicketResponse::setCarnet(puf_t* carnet, int carnetLength)
{
	this->carnetLength = carnetLength;
	if(this->carnet == NULL){
		this->carnet = (puf_t*)malloc(carnetLength * pufLength);
	}
	memcpy(this->carnet, carnet, carnetLength * pufLength);
	this->length = sizeof(flag_t) + sizeof(int) + this->pufLength * (1 + carnetLength) + targetHost.size() + 1 + sizeof(int);
	return this;
}

/**
 * @brief 	Set the targetHost value for the BCTicketResponse.
 * @param	a: address to assign.
 * @retval 	None
 */
phemap::BCTicketResponse *phemap::BCTicketResponse::setTargetHost(string a)
{
	this->targetHost = a;
	this->length = sizeof(flag_t) + sizeof(int) + this->pufLength * (1 + carnetLength) + targetHost.size() + 1 + sizeof(int);

	return this;
}

/**
 * @brief	Copy class attributes to serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @retval 	None
*/
void phemap::BCTicketResponse::serialize(uint8_t *buffer) const
{
	memcpy(buffer, &flag, sizeof(flag_t));
	memcpy(buffer + sizeof(flag_t), this->li, this->pufLength);
	memcpy(buffer + sizeof(flag_t) + this->pufLength, &carnetLength, sizeof(int));
	memcpy(buffer + sizeof(flag_t) + this->pufLength + sizeof(int), this->carnet, carnetLength * pufLength);
	memcpy(buffer + sizeof(flag_t) + this->pufLength + sizeof(int) + (carnetLength * pufLength), &targetPort, sizeof(int));
	memcpy(buffer + sizeof(flag_t) + this->pufLength + sizeof(int) + (carnetLength * pufLength) + sizeof(int), targetHost.c_str(), targetHost.size() + 1);
}

/**
 * @brief	Copy class attributes from the serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @retval 	None
*/
void phemap::BCTicketResponse::deserialize(uint8_t *buffer)
{
	memcpy(this->li, buffer + sizeof(flag_t), pufLength);
	memcpy(&carnetLength, buffer + sizeof(flag_t) + pufLength, sizeof(int));
	this->carnet = (puf_t *)malloc(carnetLength * pufLength);
	memcpy(this->carnet, buffer + sizeof(flag_t) + pufLength + sizeof(int), carnetLength * pufLength);
	memcpy(&targetPort, buffer + sizeof(flag_t) + pufLength + sizeof(int) + (carnetLength * pufLength), sizeof(int));
	this->targetHost = string((const char *)(buffer + sizeof(flag_t) + pufLength + sizeof(int) + (carnetLength * pufLength) + sizeof(int)));

	
	this->length = sizeof(flag_t) + sizeof(int) + this->pufLength * (1 + carnetLength) + targetHost.size() + 1 + sizeof(int);
}

/**
 * @brief	Overloading of the << operator.
 * @param 	os: reference to the output stream.
 * @param	msg: BCTicketResponse instance.
 * @retval 	Output stream.
*/
std::ostream &phemap::operator<<(std::ostream &os, const phemap::BCTicketResponse &msg)
{
	os << "BCTicketResponse (";
	int i, j;
	for (i = 0; i < msg.getPufLength(); i++)
	{
		os << std::hex << std::setw(2) << std::uppercase << std::setfill('0') << static_cast<int>(*(msg.getLi() + i));
	}
	os << "   -   ";
	os << std::dec << msg.carnetLength;
	os << ");";
	os << endl;
	
	os << "\nTickets released: \n";
	for (i = 0; i < msg.carnetLength; i++)
	{
		os << "T" << i+1 <<": ";
		for (j = 0; j < msg.getPufLength(); j++)
		{
			os << std::hex << std::setw(2) << std::uppercase << std::setfill('0') << static_cast<int>(*(msg.carnet + j + i*msg.pufLength));
		}
		os << std::dec << endl;
	}

	os << "\nTargetHost: " << msg.targetHost;
	os << "\nTargetPort: " << std::dec << msg.targetPort;

	return os;
}

/**
 * @brief	Destructor for BCTicketResponse.
 * @param 	None
 * @retval 	None
*/
phemap::BCTicketResponse::~BCTicketResponse()
{
	free(this->carnet);
}