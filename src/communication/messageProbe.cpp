//===- messageProbe.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file messageProbe.cpp
/// \author Andrea Aletto
/// \brief This file defines the implementation of the MessageProbe class
//===----------------------------------------------------------------------===//

#include "messageProbe.h"
#include "utils.h"

/**
 * @brief	Copy class attributes to serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @retval 	None
*/
void phemap::MessageProbe::serialize(uint8_t* buffer) const{
	memcpy(buffer, &(this->flag), sizeof(flag_t));
	memcpy(buffer + sizeof(flag_t), this->deviceId.c_str(), this->deviceId.size());

}

/**
 * @brief	Copy class attributes from the serial buffer.
 * @param 	buffer: pointer to the serial buffer.
 * @param	size
 * @retval 	None
*/
void phemap::MessageProbe::deserialize(uint8_t* buffer, int size){
	this->deviceId = string((char*)buffer + sizeof(flag_t), size-1); //deviceId length is the length of the buffer - the length of the flag that is 1 byte
	this->length = sizeof(flag_t) + deviceId.size(); 

}

/**
 * @brief	Overloading of the << operator.
 * @param 	os: reference to the output stream.
 * @param	msg: MessageProbe instance.
 * @retval 	Output stream.
*/
std::ostream &phemap::operator<<(std::ostream &os, const phemap::MessageProbe &msg)
{
	os  << "MessageProbe ("
		<< "id = "
		<< msg.deviceId
		<< ");";
	return os;
}