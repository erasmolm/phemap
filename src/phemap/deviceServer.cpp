//===- deviceServer.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file deviceServer.cpp
/// \author Mario Barbareschi
/// \brief This file defines the implementation of the DeviceServer class
//===----------------------------------------------------------------------===//

#include "utils.h"

#include "deviceServer.h"

#include <iostream>
#include <iomanip>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "deviceNetworkUtils.h"
#include "deviceException.h"

using namespace std;

/**		
 * @brief	Constructor for DeviceServer.
 * @param 	device: pointer to the device object to attach to the server.
 * @retval 	None
*/
phemap::DeviceServer::DeviceServer(phemap::Device *device) : device(device)
{
}

/**			
 * @brief	Set the device value for the DeviceServer.
 * @param	device: pointer to the device object to attach to the server.
 * @retval 	this
*/
phemap::DeviceServer *phemap::DeviceServer::setDevice(Device *device)
{
	this->device = device;
	return this;
}

/**
 * @brief 	Main task of Device Server
 * @param 	None
 * @retval 	None
 */
void phemap::DeviceServer::runDeviceServer()
{
	int sockfd;
	phemap::DeviceNetworkUtils *networkManager = new phemap::DeviceNetworkUtils();
	networkManager->setDeviceID(this->device->getDeviceID())->setDeviceHost(this->deviceHost)->setDevicePort(this->devicePort);
	try
	{
		sockfd = networkManager->createListenerSocket();
	}
	catch (phemap::PHEMAP_Device_Exception ex)
	{
		cerr << ex;
	};

	while (true)
	{
		int newsockfd, n;
		try
		{
			newsockfd = networkManager->acceptConnectionFrom(sockfd);
		}
		catch (phemap::PHEMAP_Device_Exception ex)
		{
			cerr << ex;
		}

		uint8_t *buffer = (uint8_t *)malloc(2048);

		try
		{
			n = networkManager->receiveFromVerifier(newsockfd, buffer, 2048);
		}
		catch (phemap::PHEMAP_Device_Exception ex)
		{
			cerr << ex;
		}

		/** Read the message flag from buffer */
		flag_t msg_flag = 0xFF;
		memcpy(&msg_flag, buffer, sizeof(flag_t));

		switch (msg_flag)
		{
		case (phemap::INIT_M1):
		{
			Message1 *message1 = new Message1(buffer + sizeof(flag_t), device->getPuf()->getResponseByteLength());
			cout << "DeviceServer: I'm receiving\t" << *message1 << endl;

			Message2 *message2 = new Message2(device->getPuf()->getResponseByteLength());
			device->PHEMAP_initiation_phase2(message1, message2);

			message2->serialize(buffer);
			cout << "DeviceServer: I'm sending\t" << *message2 << endl;

			try
			{
				networkManager->sendToVerifier(newsockfd, buffer, device->getPuf()->getResponseByteLength() * 2 + sizeof(flag_t));
			}
			catch (phemap::PHEMAP_Device_Exception ex)
			{
				cerr << ex;
			}

			free(message1);
			free(message2);
		}
		break;

		case (phemap::INIT_M3):
		{
			Message3 *message3 = new Message3(buffer + sizeof(flag_t), device->getPuf()->getResponseByteLength());
			cout << "DeviceServer: I'm receiving\t" << *message3 << endl;
			device->PHEMAP_initiation_phase4(message3);
		}
		break;

		case (phemap::SALT_INSTALL):
		{
			SaltInstall *req = new SaltInstall();
			MessageAuth *res = new MessageAuth();
			req->deserialize(buffer);

			cout << "\nDeviceServer: I'm receiving\tsalt install request: \n"
				 << *req << endl;

			/** Authenticate request */
			if (device->PHEMAP_deviceAuthentication(req, res))
			{
				puf_t *salt = (puf_t*)malloc(req->getPufLength());
				
				/** At this point, res->getLi() returns the link to be xored with the salt */
				xorVector(salt, res->getLi(), req->getSalt(), req->getPufLength());
				device->setSalt(salt);
				free(salt);

				/** get the right link for the response */
				device->getNextPufLink(res->getLi(), false);

				/** Set proxy parameters */
				device->getVerifierProxy()->setProxyAddress(req->getAddress());
				device->getVerifierProxy()->setProxyPort(req->getPort());
				device->getVerifierProxy()->useProxy = true;
				device->setSaltCounter(req->getTicketCounter());
				device->authenticationMode = AMODE_SALTED;

				/** Create response */
				bzero(buffer, 2048);
				res->serialize(buffer);
				try
				{
					networkManager->sendToVerifier(newsockfd, buffer, res->getLength());
				}
				catch (phemap::PHEMAP_Device_Exception ex)
				{
					cerr << ex;
				}

				cout << "\nDeviceServer: I'm sending\tsalt install response\n"
					 << *res << endl;
			}
			else
			{
				cerr << "\n\nAuthentication from verifier failed for salt install request\n\n";
			}
		}
		break;

		case (phemap::CHANGE_ADDR):
		{
			MessageChangeAddress *req = new MessageChangeAddress();
			MessageChangeAddress *res = new MessageChangeAddress();
			req->deserialize(buffer);

			cout << "\nDeviceServer: I'm receiving\tchange address request: \n"
				 << *req << endl;

			/** Authenticate request */
			if (device->PHEMAP_deviceAuthentication(req, res))
			{
				/** Set proxy parameters */
				device->getVerifierProxy()->setProxyAddress(req->getAddress());
				device->getVerifierProxy()->setProxyPort(req->getPort());
				device->getVerifierProxy()->useProxy = true;

				/** Create response */
				bzero(buffer, 2048);
				res->serialize(buffer);
				try
				{
					networkManager->sendToVerifier(newsockfd, buffer, res->getLength());
				}
				catch (phemap::PHEMAP_Device_Exception ex)
				{
					cerr << ex;
				}

				cout << "\nDeviceServer: I'm sending\tchange address response\n"
					 << *res << endl;
			}
			else
			{
				cerr << "\n\nAuthentication from verifier failed for change address request\n\n";
			}
		}
		break;

		case (phemap::UNSET_PROXY):
		{
			device->getVerifierProxy()->setProxyPort(-1);
			device->getVerifierProxy()->setProxyAddress("localhost");
			device->getVerifierProxy()->useProxy = false;
		}
			//break; go to VER_STR case

		case (phemap::VER_STR):
		{
			MessageAuth *req = new MessageAuth();

			req->deserialize(buffer);

			cout << "\nDeviceServer: I'm receiving\t" << *req << endl;

			MessageAuth *res = new MessageAuth();

			/** In case of Babelchain, calculate Ti XOR Bi = Ai */
			if (device->authenticationMode == phemap::AMODE_BC_IMPLICIT ||
				device->authenticationMode == phemap::AMODE_BC_EXPLICIT)
			{
				puf_t *carnetLink = (puf_t *)malloc(sizeof(puf_t));
				device->getBCTargetManager()->getDeviceProxy()->getPufChain()->getNextLink(carnetLink);
				if (device->authenticationMode == phemap::AMODE_BC_IMPLICIT)
				{
					xorVector(req->getLi(), req->getLi(), carnetLink, res->getPufLength());
				}

				free(carnetLink);
			}

			/** Authenticate request */
			xorVector(req->getLi(), req->getLi(), device->getSalt(), req->getPufLength());
			if (device->PHEMAP_deviceAuthentication(req, res))
			{
				xorVector(res->getLi(), res->getLi(), device->getSalt(), res->getPufLength());
				/** In case of Babelchain, calculate Ti XOR Ai = Bi */
				if (device->authenticationMode == phemap::AMODE_BC_IMPLICIT ||
					device->authenticationMode == phemap::AMODE_BC_EXPLICIT)
				{
					puf_t *carnetLink = (puf_t *)malloc(sizeof(puf_t));
					device->getBCTargetManager()->getDeviceProxy()->getPufChain()->getNextLink(carnetLink);

					xorVector(res->getLi(), res->getLi(), carnetLink, res->getPufLength());
					free(carnetLink);

					/** Update remaining tickets */
					int AvailableTickets = device->getBCTickets();
					device->setBCTickets(AvailableTickets - 2);
					AvailableTickets = device->getBCTickets();

					if (AvailableTickets == 0)
					{
						device->authenticationMode = phemap::AMODE_NORMAL;
						delete device->getBCTargetManager();
						res->setFlag(UNSET_PROXY);
					}
				}
				else if (device->authenticationMode == phemap::AMODE_SALTED)
				{ /** In case of salted phemap */
					device->setSaltCounter(device->getSaltCounter() - 2);
					if (device->getSaltCounter() == 0)
					{ /** For last message, unset the proxy and switch to normal mode */
						device->authenticationMode = phemap::AMODE_NORMAL;
						device->getVerifierProxy()->setProxyPort(-1);
						device->getVerifierProxy()->setProxyAddress("localhost");
						device->flushSalt();
						device->getVerifierProxy()->useProxy = false;
					}
				}

				/** Write response */
				bzero(buffer, 2048);
				res->serialize(buffer);
				try
				{
					networkManager->sendToVerifier(newsockfd, buffer, res->getLength());
				}
				catch (phemap::PHEMAP_Device_Exception ex)
				{
					cerr << ex;
				}

				cout << "DeviceServer: I'm sending\t" << *res << endl;
			}
			else
			{
				cerr << "Authentication from verifier failed" << endl;
			}
		}
		break;

		default:
		{
			cerr << "\n[ERROR] Unexpected message type received on Device server.";
		}
		}
		free(buffer);
		networkManager->closeSocket(newsockfd);
	}
	networkManager->closeSocket(sockfd);
	delete networkManager;
}

/**
 * @brief	Destructor for DeviceServer.
 * @param 	None
 * @retval 	None
*/
phemap::DeviceServer::~DeviceServer()
{
}
