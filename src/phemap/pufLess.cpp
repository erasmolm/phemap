//===- pufLess.cpp ----------------------------------------------------*- C++ -*-
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file pufLess.cpp
/// \author Erasmo La Montagna
/// \brief This file defines the implementation of the PufLess class
//===----------------------------------------------------------------------===//

#include "pufLess.h"
#include "pufChainFromCarnet.h"

#include "utils.h"

/**			
 * @brief	Constructor for PufLess.
 * @param 	pufLessID
 * @retval 	None
*/
phemap::PufLess::PufLess(string pufLessID)
{
	this->nodeID = pufLessID;
	saltVerifierProxy = new phemap::SaltVerifierProxy();
	devicesMap = new DevicesMap_t();
	saltMap = new SaltMap_t();
}

/**
 * @brief	Authenticates the verifier.
 * @param	deviceID
 * @param 	req: pointer to the request message.
 * @param 	res: pointer to the response message.
 * @retval 	true if communication ends successfully, false otherwise.
*/
bool phemap::PufLess::PHEMAP_verifierAuthentication(const string deviceID, MessageAuth *req, MessageAuth *res)
{

	/*Search for the device in the devices map*/
	DevicesMap_t::const_iterator devices = devicesMap->find(deviceID);
	if (devices == devicesMap->end())
	{
		std::cerr << "Device " << deviceID << " not found" << std::endl;
		return false;
	}

	/*Verify the device using device manager and puf chain stored*/
	return devices->second->SALT_PHEMAP_verificationFromChain(req->getLi(), res->getLi());
	//return devices->second->PHEMAP_verificationFromChain(req->getLi(), res->getLi());
}

/**			
 * @brief	Sends a SALTED PHEMAP ticket request to the PuffLess.	
 * @param	targetID
 * @param 	numOfTickets: number of tickets to request.
 * @param 	originHost: origin host.
 * @param 	originPort: origin port.
 * @retval 	true if request ends successfully, false otherwise.
*/
int phemap::PufLess::SALT_PHEMAP_ticketRequest(string targetID, int numOfTickets, string originHost, int originPort)
{
	int outcome = 0;
	string targetHost = "";
	int targetPort = -1;

	SaltTicketRequest *req = new SaltTicketRequest();
	SaltTicketResponse *res = new SaltTicketResponse();

	req->setOriginID(this->nodeID)->setTargetID(targetID)->setRequestedTickets(numOfTickets)->setOriginHost(originHost)->setOriginPort(originPort);

	/** Send ticket request to the verifier */
	if (this->saltVerifierProxy->SALT_PHEMAP_ticketRequest(req, res))
	{
		outcome = res->getCarnetLength();
		targetHost = res->getTargetHost();
		targetPort = res->getTargetPort();

		if (outcome > 1)
		{
			/** Create an occurrence of the device if it does not exist */
			DevicesMap_t::iterator devices = devicesMap->find(targetID);
			PufChainFromCarnet *pufChainFromCarnet = new phemap::PufChainFromCarnet(res->getCarnet(), outcome, res->getPufLength());
			if (devices == devicesMap->end())
			{
				pair<string, phemap::DeviceManager *> pair(targetID, (new phemap::DeviceManager())->setDeviceProxy(new phemap::DeviceProxy(targetID, targetHost, targetPort, pufChainFromCarnet)));
				devicesMap->insert(pair);
			}
			else
			{
				devices->second->setDeviceProxy(new phemap::DeviceProxy(targetID, targetHost, targetPort, pufChainFromCarnet));
			}

			/** Create a ticket counter if it does not exist */
			SaltMap_t::iterator counters = saltMap->find(targetID);
			if (counters == saltMap->end())
			{
				pair<string, int> pair(targetID, outcome);
				saltMap->insert(pair);
			}
			else
			{ /** just simply update the counter */
				counters->second = outcome;
			}
		}

		cout << std::dec << "\nSALTED PHEMAP - Obtained tickets: " << outcome << endl;
	}
	else
	{
		cerr << "\n\nError sending salt ticket request\n\n";
		outcome = -1;
	}

	delete req;
	delete res;

	return outcome;
}


/**
 * @brief	Starts a verification message exchange using SALT PHEMAP.
 * @param 	body: body of the message.
 * @retval 	true if verification ends successfully, false otherwise.
*/
bool phemap::PufLess::SALT_PHEMAP_startVerification(string deviceID, string body)
{
	bool outcome = false;

	DevicesMap_t::const_iterator devices = devicesMap->find(deviceID);
	if (devices == devicesMap->end())
	{
		std::cerr << "Device " << deviceID << " not found" << std::endl;
	}
	else
	{
		SaltMap_t::iterator counters = saltMap->find(deviceID);
		if (counters == saltMap->end())
		{
			std::cerr << "Device " << deviceID << " counter not found" << std::endl;
		}
		else if (counters->second > 1)
		{
			counters->second = counters->second - 2;
			cout << "\nsalt counter updated: " << counters->second << endl;
			outcome = devices->second->SALT_PHEMAP_startVerification(body);
		}
		else
		{
			cout << "\n no salt tickets remained. Ask for a new carnet.\n";
		}
	}
	return outcome;
}

phemap::PufLess::~PufLess()
{
	free(devicesMap);
}