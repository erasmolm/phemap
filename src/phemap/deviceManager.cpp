//===- deviceManager.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file deviceManager.cpp
/// \author Mario Barbareschi
/// \brief This file defines the implementation of the DeviceManager class
//===----------------------------------------------------------------------===//

#include "utils.h"
#include <cstring>
#include <iostream>
#include "deviceManager.h"
using namespace std;

/**
 * @brief	Set the deviceProxy value for the DeviceManager.	
 * @param 	deviceProxy: pointer to the value to write to the deviceProxy attribute.
 * @retval 	this
*/
phemap::DeviceManager *phemap::DeviceManager::setDeviceProxy(phemap::DeviceProxy *deviceProxy)
{
	this->deviceProxy = deviceProxy;
	return this;
}

/**
 * @brief	Start PHEMAP Initiation
 * @param	None
 * @retval 	True if it ends successfully, false otherwise.
*/
bool phemap::DeviceManager::PHEMAP_startInitiation()
{
	bool outcome = true;

	/* Allocate memory for msg1 and msg2 */
	Message1 *message1 = new Message1(deviceProxy->getPufChain()->getCRByteLength());
	Message2 *message2 = new Message2(deviceProxy->getPufChain()->getCRByteLength());

	/* Start PHEMAP Initiation Phase 1*/
	PHEMAP_initiation_phase1(message1);

	/* Send message1 to the device and start Phase 2*/
	outcome &= deviceProxy->PHEMAP_initiation_phase2(message1, message2);

	if (!outcome)
	{
		deviceProxy->setState(UNKNOWN);
	}
	else
	{
		deviceProxy->setState(INITIATED);

		/* Allocate memory for msg3 and set the root as the one in the previous message */
		Message3 *message3 = new Message3(deviceProxy->getPufChain()->getCRByteLength());
		message3->setLi(message1->getLi());

		/* Start Init Phase 3*/
		outcome &= PHEMAP_initiation_phase3(message2, message3);

		if (!outcome)
		{
			deviceProxy->setState(UNKNOWN);
		}
		else
		{

			/* Send msg3 and start Phase 4*/
			outcome &= deviceProxy->PHEMAP_initiation_phase4(message3);
			if (!outcome)
			{
				deviceProxy->setState(ERROR);
				outcome = false;
			}
			else
			{
				/* If here, Init has been completed successfully */
				deviceProxy->setState(AUTHENTICATED);

				/*Jump to the next link, such that it is the first link for auth*/
				puf_t *pufChainTemp = (puf_t *)malloc(deviceProxy->getPufChain()->getCRByteLength() * sizeof(puf_t));
				deviceProxy->getPufChain()->getNextLink(pufChainTemp);
				free(pufChainTemp);
				deviceProxy->resetlinkCounter();
			}
		}

		free(message3);
	}

	free(message1);
	free(message2);

	return outcome;
}

/**
 * @brief	Start PHEMAP Verification.
 * @param 	body: body of the message.
 * @retval 	true if verification ends successfully, false otherwise.
*/
bool phemap::DeviceManager::PHEMAP_startVerification(string body)
{
	puf_t *li2Computed = (puf_t *)malloc(deviceProxy->getPufChain()->getCRByteLength() * sizeof(puf_t));

	MessageAuth *req = new MessageAuth();
	MessageAuth *res = new MessageAuth();

	req->setBody(body);
	getNextPufLink(req->getLi(), false);

	bool outcome = deviceProxy->PHEMAP_deviceAuthentication(req, res);
	if (!outcome)
	{
		deviceProxy->setState(UNKNOWN);
	}
	else
	{
		getNextPufLink(li2Computed, false);
		if (0 == memcmp(li2Computed, res->getLi(), deviceProxy->getPufChain()->getCRByteLength()))
		{
			deviceProxy->setState(AUTHENTICATED);
		}
		else
		{
			deviceProxy->setState(ERROR);
			outcome = false;
		}
	}
	free(li2Computed);
	return outcome;
}

/**
 * @brief	Starts a verification message exchange using salt phemap.
 * @param 	body: body of the message.
 * @retval 	true if verification ends successfully, false otherwise.
*/
bool phemap::DeviceManager::SALT_PHEMAP_startVerification(string body)
{
	puf_t *li2Computed = (puf_t *)malloc(deviceProxy->getPufChain()->getCRByteLength() * sizeof(puf_t));

	MessageAuth *req = new MessageAuth();
	MessageAuth *res = new MessageAuth();

	req->setBody(body);
	getNextPufLink(req->getLi(), true);

	bool outcome = deviceProxy->PHEMAP_deviceAuthentication(req, res);
	if (!outcome)
	{
		deviceProxy->setState(UNKNOWN);
	}
	else
	{
		getNextPufLink(li2Computed, true);
		if (0 == memcmp(li2Computed, res->getLi(), deviceProxy->getPufChain()->getCRByteLength()))
		{
			deviceProxy->setState(AUTHENTICATED);
		}
		else
		{
			deviceProxy->setState(ERROR);
			outcome = false;
		}
	}
	free(li2Computed);
	return outcome;
}

/**
 * @brief	Given a received Puf link compares the latter with the respective taken from the Puf chain.
 * 			If comparation ends successfully, then writes the successive Puf link to Li2. 
 * @param	Li1Received: pointer to the received Puf link.
 * @param	Li2: pointer to Li2 Puf link.
 * @retval	true if verification ends successfully, false otherwise.
 */
bool phemap::DeviceManager::PHEMAP_verificationFromChain(const puf_t *li1Received, puf_t *li2)
{
	bool outcome = false;
	puf_t *li1 = (puf_t *)malloc(deviceProxy->getPufChain()->getCRByteLength() * sizeof(puf_t));
	peekNextPufLink(li1, false);
	if (0 == memcmp(li1, li1Received, deviceProxy->getPufChain()->getCRByteLength()))
	{
		outcome = true;
		getNextPufLink(0, false);
		getNextPufLink(li2, false);
	}
	else
	{
		outcome = false;
	}
	free(li1);

	return outcome;
}

/**
 * @brief	Given a received Puf link compares the latter with the respective taken from the carnet.
 * 			If comparation ends successfully, then writes the successive Puf link to Li2. 
 * @param	Li1Received: pointer to the received Puf link.
 * @param	Li2: pointer to Li2 Puf link.
 * @retval	true if verification ends successfully, false otherwise.
 */
bool phemap::DeviceManager::SALT_PHEMAP_verificationFromChain(const puf_t *li1Received, puf_t *li2)
{
	bool outcome = false;
	puf_t *li1 = (puf_t *)malloc(deviceProxy->getPufChain()->getCRByteLength() * sizeof(puf_t));
	peekNextPufLink(li1, true);
	if (0 == memcmp(li1, li1Received, deviceProxy->getPufChain()->getCRByteLength()))
	{
		outcome = true;
		getNextPufLink(0, true);
		getNextPufLink(li2, true);
	}
	else
	{
		outcome = false;
	}
	free(li1);

	return outcome;
}

/**			
 * @brief	Increase the link counter and put next link in the storage. If Li is not-null
 * 			then it will contain the next link at the end of execution.
 * @param 	link: starting link from which to calculate the next one. 
 * @param	ignoreSentinel: determines whether to test sentinels or not.
 * @retval 	None
*/
void phemap::DeviceManager::getNextPufLink(puf_t *link, bool ignoreSentinel)
{
	puf_t *chain = (puf_t *)malloc(deviceProxy->getPufChain()->getCRByteLength() * sizeof(puf_t));
	if (false == ignoreSentinel && false == deviceProxy->countLink(true))
	{
		deviceProxy->getPufChain()->getNextLink(chain);
		deviceProxy->countLink(true);
	}
	deviceProxy->getPufChain()->getNextLink(chain);
	if (link != 0)
	{
		memcpy(link, chain, deviceProxy->getPufChain()->getCRByteLength());
	}
	free(chain);
}

/**
 * @brief	Given a Puf link, looks for the successive one. It lets the counter unmodified.
 * @param 	link: starting link from which to calculate the next one. 
 * @param	ignoreSentinel: determines whether to test sentinels or not.	
 * @retval 	None
*/
void phemap::DeviceManager::peekNextPufLink(puf_t *link, bool ignoreSentinel)
{
	if (false == ignoreSentinel && false == deviceProxy->countLink(false))
	{
		deviceProxy->getPufChain()->peekLink(link, 2);
	}
	else
	{
		deviceProxy->getPufChain()->peekLink(link, 1);
	}
}

/**
 * @brief	Starts the first phase of the PHEMAP initiation. 
 * @param 	Message1: pointer to the first message of PHEMAP initiation.
 * @retval 	None
*/
void phemap::DeviceManager::PHEMAP_initiation_phase1(Message1 *message1)
{
	puf_t *root = (puf_t *)malloc(deviceProxy->getPufChain()->getCRByteLength() * sizeof(puf_t));
	puf_t *v1 = (puf_t *)malloc(deviceProxy->getPufChain()->getCRByteLength() * sizeof(puf_t));
	puf_t *v2 = (puf_t *)malloc(deviceProxy->getPufChain()->getCRByteLength() * sizeof(puf_t));
	puf_t *nonce = (puf_t *)malloc(deviceProxy->getPufChain()->getCRByteLength() * sizeof(puf_t));
	puf_t *pufChainTemp = (puf_t *)malloc(deviceProxy->getPufChain()->getCRByteLength() * sizeof(puf_t)); //variable to temporary store current puf chain

	/*Initializes root and v1 with the next link in the chain file*/
	deviceProxy->getPufChain()->getNextLink(root);
	getRandomNonce(nonce, deviceProxy->getPufChain()->getCRByteLength());
	deviceProxy->getPufChain()->getNextLink(v1);

	for (int i = 0; i < deviceProxy->getSentinel() - 4; i++)
	{
		deviceProxy->getPufChain()->getNextLink(pufChainTemp);
		xorVector(v1, v1, pufChainTemp, deviceProxy->getPufChain()->getCRByteLength());
	}
	xorVector(v1, v1, nonce, deviceProxy->getPufChain()->getCRByteLength());

	deviceProxy->getPufChain()->getNextLink(v2);
	deviceProxy->getPufChain()->getNextLink(pufChainTemp);
	xorVector(v2, xorVector(v2, v2, nonce, deviceProxy->getPufChain()->getCRByteLength()),
			  pufChainTemp,
			  deviceProxy->getPufChain()->getCRByteLength());

	message1->setLi(root);
	message1->setV1(v1);
	message1->setV2(v2);

	free(nonce);
	free(pufChainTemp);
}

/**
 * @brief	Starts the third phase of the PHEMAP initiation, given message2, message3 will be created. 
 * @param 	Message2: pointer to the second message of PHEMAP initiation.
 * @param 	Message3: pointer to the third message of PHEMAP initiation.
 * @retval 	true if verification ends successfully, false otherwise.
*/
bool phemap::DeviceManager::PHEMAP_initiation_phase3(const Message2 *message2, Message3 *message3)
{

	puf_t *lis = (puf_t *)malloc(deviceProxy->getPufChain()->getCRByteLength() * sizeof(puf_t));
	puf_t *lis1 = (puf_t *)malloc(deviceProxy->getPufChain()->getCRByteLength() * sizeof(puf_t));
	puf_t *d1xord2 = (puf_t *)malloc(deviceProxy->getPufChain()->getCRByteLength() * sizeof(puf_t));
	puf_t *pufChainTemp = (puf_t *)malloc(deviceProxy->getPufChain()->getCRByteLength() * sizeof(puf_t)); //variable to temporary store current puf chain

	deviceProxy->getPufChain()->getNextLink(lis);
	deviceProxy->getPufChain()->getNextLink(lis1);
	xorVector(pufChainTemp, lis, lis1, deviceProxy->getPufChain()->getCRByteLength());
	xorVector(d1xord2, message2->getD1(), message2->getD2(), deviceProxy->getPufChain()->getCRByteLength());

	if (0 == memcmp(d1xord2, pufChainTemp, deviceProxy->getPufChain()->getCRByteLength()))
	{
		puf_t *v3 = (puf_t *)malloc(deviceProxy->getPufChain()->getCRByteLength() * sizeof(puf_t));
		xorVector(v3, message2->getD1(), lis, deviceProxy->getPufChain()->getCRByteLength());
		deviceProxy->getPufChain()->getNextLink(pufChainTemp);
		xorVector(v3, v3, pufChainTemp, deviceProxy->getPufChain()->getCRByteLength());
		message3->setV3(v3);

		return true;
	}
	else
	{
		return false;
	}

	free(lis);
	free(lis1);
	free(d1xord2);
	free(pufChainTemp);
}
