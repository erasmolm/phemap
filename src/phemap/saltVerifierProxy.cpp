//===- saltVerifierProxy.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file saltVerifierProxy.cpp
/// \author Erasmo La Montagna
/// \brief This file defines the implementation of the SaltVerifierProxy class
//===----------------------------------------------------------------------===//

#include "utils.h"

#include "saltVerifierProxy.h"
#include "deviceNetworkUtils.h"
#include "deviceException.h"

/**		
 * @brief	Sends a SALTED PHEMAP ticket request to the verifier.		
 * @param	req: pointer to the request message.
 * @param 	res: pointer to the response message.
 * @retval 	true if request ends successfully, false otherwise.
*/
bool phemap::SaltVerifierProxy::SALT_PHEMAP_ticketRequest(const SaltTicketRequest *req, SaltTicketResponse *res)
{
	int sockfd, n;
	phemap::DeviceNetworkUtils *networkManager = new phemap::DeviceNetworkUtils();
	networkManager->setDeviceID(req->getOriginID())->setVerifierHost(this->verifierHost)->setVerifierPort(this->verifierPort);
	
	try
	{
		sockfd = networkManager->createSocketToVerifier();
	}
	catch (phemap::PHEMAP_Device_Exception ex)
	{
		cout << ex;
		return false;
	}

	uint8_t *buffer = (uint8_t *)malloc(2048);

	req->serialize(buffer);

	try
	{
		n = networkManager->sendToVerifier(sockfd, buffer, req->getLength());
	}
	catch (phemap::PHEMAP_Device_Exception ex)
	{
		cout << ex;
		return false;
	}
	cout << "Salt Ticket Request: I'm sending\t\t";
	cout << *req;
	cout << endl;

	bzero(buffer, 2048);
	try
	{
		n = networkManager->receiveFromVerifier(sockfd, buffer, 2048);
	}
	catch (phemap::PHEMAP_Device_Exception ex)
	{
		cout << ex;
		return false;
	}
	res->deserialize(buffer);

	cout << "Salt Ticket response: I'm receiving\t";
	cout << *res;
	cout << endl;

	networkManager->closeSocket(sockfd);
	free(buffer);
	delete networkManager;
	return true;
}