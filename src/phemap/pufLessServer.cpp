//===- pufLessServer.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file pufLessServer.cpp
/// \author Erasmo La Montagna
/// \brief This file defines the implementation of the PufLessServer class
//===----------------------------------------------------------------------===//

#include "pufLessServer.h"
#include "verifierNetworkUtils.h"
#include "verifierException.h"

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

using namespace std;

/** 
 * @brief	Set the device Id value for the DeviceSkeleton.
 * @param 	pufLess: pointer to the value to write to the pufLess attribute.
 * @retval 	this
*/
phemap::PufLessServer *phemap::PufLessServer::setPufLess(PufLess *pufLess)
{
	this->pufLess = pufLess;
	return this;
}

/**		
 * @brief 	Main task of PufLess Server
 * 			This function opens a TCP Socket onto the pufLess port and
 * 			first waits for a packet from a device.
 * @param 	None
 * @retval 	None
 */
void phemap::PufLessServer::runPufLessServer()
{
	int sockfd;
	VerifierNetworkUtils *networkManager = new VerifierNetworkUtils();
	networkManager->setVerifierHost(this->pufLessHost)->setVerifierPort(this->pufLessPort);
	try
	{
		sockfd = networkManager->createListenerSocket();
	}
	catch (phemap::PHEMAP_Verifier_Exception ex)
	{
		cerr << ex;
	}

	while (true)
	{
		int newsockfd, n;
		try
		{
			newsockfd = networkManager->acceptConnectionFrom(sockfd);
		}
		catch (phemap::PHEMAP_Verifier_Exception ex)
		{
			cerr << ex;
		}

		uint8_t *buffer = (uint8_t *)malloc(2048);

		try
		{
			n = networkManager->receiveFromDevice(newsockfd, buffer, 2048);
		}
		catch (phemap::PHEMAP_Verifier_Exception ex)
		{	
			cerr << ex;
		}

		/** Read the message flag from buffer */
		flag_t msg_flag = 0xFF;
		memcpy(&msg_flag, buffer, sizeof(flag_t));

		switch (msg_flag)
		{
		case (phemap::VER_STR):
		{
			MessageAuth *req = new MessageAuth();
			MessageAuth *res = new MessageAuth();
			req->deserialize(buffer);

			int deviceIDSize = n - req->getLength();
			string deviceID((char *)(buffer + req->getLength()), deviceIDSize);

			cout << "PufLessServer: I'm receiving\t"
				 << "\ndeviceID: " << deviceID << endl
				 << *req
				 << endl;

			if (pufLess->PHEMAP_verifierAuthentication(deviceID, req, res))
			{

				bzero(buffer, 2048);
				res->serialize(buffer);
				try
				{
					networkManager->sendToDevice(newsockfd, buffer, res->getLength());
				}
				catch (phemap::PHEMAP_Verifier_Exception ex)
				{
					cerr << ex;
				}

				cout << "PufLessServer: I'm sending\t\t"
					 << "\ndeviceID: " << deviceID << endl
					 << *res
					 << endl;
			}
			else
			{
				cerr << "Authentication from device failed" << endl;
			}

			/** Update remaining tickets */
			SaltMap_t::iterator counters = pufLess->getSaltMap()->find(deviceID);
			if (counters->second > 1)
			{
				counters->second = counters->second - 2;
			}
		}
		break;

		case (phemap::SALT_TCK_REQ):
		{
			SaltTicketRequest *req = new SaltTicketRequest();
			req->deserialize(buffer);

			string path = "../log/setup_r2_" + req->getTargetID() + ".txt";

			/** Write current timestamp to the log file */
			chrono::nanoseconds start = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());

			pufLess->SALT_PHEMAP_ticketRequest(	req->getTargetID(),
												req->getRequestedTickets(),
												req->getOriginHost(),
												req->getOriginPort());
			/** Write current timestamp to the log file */
			chrono::nanoseconds end = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());

			/** Write to file */
			ofstream logFile(path, std::ofstream::out | std::ofstream::app);
			logFile << end.count() - start.count() << endl;
			logFile.close();

			req->~SaltTicketRequest();
		}
		break;

		default:
		{
			cerr << "\n[ERROR] Unexpected message type received on PufLess server.";
		}
		}
		free(buffer);
		networkManager->closeSocket(newsockfd);
	}
	networkManager->closeSocket(sockfd);
	delete networkManager;
}

/**
 * @brief	Destructor for PufLessServer.
 * @param 	None
 * @retval 	None
*/
phemap::PufLessServer::~PufLessServer()
{
}