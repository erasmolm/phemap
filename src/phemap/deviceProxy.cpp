//===- deviceProxy.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file deviceProxy.cpp
/// \author Mario Barbareschi
/// \brief This file defines the implementation of the DeviceProxy class
//===----------------------------------------------------------------------===//

#include "utils.h"

#include <iostream>
#include <iomanip>
#include <assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>

#include "deviceProxy.h"
#include "verifierException.h"

using namespace std;

/**			
 * @brief	Constructor for DeviceProxy.
 * @param 	deviceID
 * @param	host: device host.
 * @param	port: device port.
 * @param	pufChain: pointer to the Puf chain.
 * @retval 	None
*/
phemap::DeviceProxy::DeviceProxy(string deviceID, string host, int port, phemap::PufChain *pufChain) : DeviceSkeleton(deviceID), pufChain(pufChain)
{
	this->devicePort = port;
	this->deviceHost = host;
	setSentinel(DEFAULT_SENTINEL);
}

/**			
 * @brief	Constructor for DeviceProxy.
 * @param 	deviceID
 * @param	host: device host.
 * @param	port: device port.
 * @param	pufChain: pointer to the Puf chain.
 * @param	sentinel: sentinel period.
 * @retval 	None
*/
phemap::DeviceProxy::DeviceProxy(string deviceID, string host, int port, phemap::PufChain *pufChain, int sentinel) : DeviceSkeleton(deviceID), pufChain(pufChain)
{
	this->devicePort = port;
	this->deviceHost = host;
	setSentinel(sentinel);
}

/**
 * @brief	Returns a pointer to the Puf chain attribute.
 * @param 	None
 * @retval 	PufChain field of the message.
*/
phemap::PufChain *phemap::DeviceProxy::getPufChain()
{
	return pufChain;
}

/**
 * @brief	Increase the link counter and put next link in the storage.
 * @param 	link: starting link from which to calculate the next one. 
 * @param	ignoreSentinel: determines whether to test sentinels or not.
 * @retval 	None
*/
void phemap::DeviceProxy::getNextPufLink(puf_t *link, bool ignoreSentinel)
{
	if (false == ignoreSentinel && true == countLink(true))
	{
		puf_t *tmp = (puf_t *)malloc(this->pufChain->getCRByteLength() * sizeof(puf_t));
		this->pufChain->getNextLink(tmp);
		free(tmp);
	}
	this->pufChain->getNextLink(link);
	countLink(true);
}

/**
 * @brief	Set the Puf chain value for the Device Proxy.
 * @param	PufChain: pointer to the value to write to the Puf chain attribute.
 * @retval 	this
*/
void phemap::DeviceProxy::setPufChain(phemap::PufChain *pufChain)
{
	this->pufChain = pufChain;
}

/**
 * @brief	Starts the second phase of the PHEMAP initiation, given message1, message2 will be created. 
 * @param 	Message1: pointer to the first message of PHEMAP initiation.
 * @param 	Message2: pointer to the second message of PHEMAP initiation.
 * @retval 	always true
*/
bool phemap::DeviceProxy::PHEMAP_initiation_phase2(const phemap::Message1 *message1, phemap::Message2 *message2)
{
	int n, sockfd;
	uint8_t *buffer = (uint8_t *)malloc(pufChain->getCRByteLength() * 3 + sizeof(flag_t));
	VerifierNetworkUtils *networkManager = new VerifierNetworkUtils();

	if (false == this->directMode)
	{
		try
		{
			sockfd = networkManager->createSocketToDevice(this->getDeviceHost(), this->getDevicePort());
		}
		catch (phemap::PHEMAP_Verifier_Exception ex)
		{
			cout << ex;
		}
	}
	else
	{
		sockfd = this->directSocket;
	}

	/*Write the message fields on the buffer, flag included*/
	message1->serialize(buffer);

	try
	{
		n = networkManager->sendToDevice(sockfd, buffer, pufChain->getCRByteLength() * 3 + sizeof(flag_t));
	}
	catch (phemap::PHEMAP_Verifier_Exception ex)
	{
		cout << ex;
	}

	cout << "Initiation: I'm sending\t\t" << (*message1) << endl;

	bzero(buffer, pufChain->getCRByteLength() * 2 + sizeof(flag_t));

	try
	{
		n = networkManager->receiveFromDevice(sockfd, buffer, pufChain->getCRByteLength() * 2 + sizeof(flag_t));
	}
	catch (phemap::PHEMAP_Verifier_Exception ex)
	{
		cout << ex;
	}

	if (!(message2->deserialize(buffer, n)))
	{
		//TODO: lanciare eccezione che è fallita la deserializzazione del messaggio
	}

	cout << "Initiation: I'm receiving\t" << (*message2) << endl;

	free(buffer);

	if (false == this->directMode)
	{
		networkManager->closeSocket(sockfd);
	}
	delete networkManager;
	return true;
}

/**
 * @brief	Starts the fourth phase of the PHEMAP initiation.  
 * @param 	Message3: pointer to the third message of PHEMAP initiation.
 * @retval 	always true
*/
bool phemap::DeviceProxy::PHEMAP_initiation_phase4(const phemap::Message3 *message3)
{
	int n, sockfd;
	VerifierNetworkUtils *networkManager = new VerifierNetworkUtils();

	if (false == this->directMode)
	{
		try
		{
			sockfd = networkManager->createSocketToDevice(this->getDeviceHost(), this->getDevicePort());
		}
		catch (phemap::PHEMAP_Verifier_Exception ex)
		{
			cout << ex;
		}
	}
	else
	{
		sockfd = this->directSocket;
	}

	uint8_t *buffer = (uint8_t *)malloc(pufChain->getCRByteLength() * 2 + sizeof(flag_t));

	message3->serialize(buffer);

	try
	{
		n = networkManager->sendToDevice(sockfd, buffer, pufChain->getCRByteLength() * 2 + sizeof(flag_t));
	}
	catch (phemap::PHEMAP_Verifier_Exception ex)
	{
		cout << ex;
	}

	cout << "Initiation: I'm sending\t\t" << (*message3) << endl;
	cout << endl;

	free(buffer);

	if (false == this->directMode)
	{
		networkManager->closeSocket(sockfd);
	}
	delete networkManager;
	return true;
}

/**
 * @brief	Checks the received link li1 with the one expected. If it ends successfully,
 * 			then li2 contains the next link li2.
 * @param 	li1: the link received.
 * @param 	li2: the new generated link by the puf.
 * @retval 	true if verification ends successfully, false otherwise.
*/
bool phemap::DeviceProxy::PHEMAP_deviceAuthentication(MessageAuth *req, MessageAuth *res)
{
	int n, sockfd;
	VerifierNetworkUtils *networkManager = new VerifierNetworkUtils();

	if (false == this->directMode)
	{
		try
		{
			sockfd = networkManager->createSocketToDevice(this->getDeviceHost(), this->getDevicePort());
		}
		catch (phemap::PHEMAP_Verifier_Exception ex)
		{
			cout << ex;
			return false;
		}
	}
	else
	{
		sockfd = this->directSocket;
	}

	uint8_t *buffer = (uint8_t *)malloc(req->getLength());
	req->serialize(buffer);

	try
	{
		n = networkManager->sendToDevice(sockfd, buffer, req->getLength());
	}
	catch (phemap::PHEMAP_Verifier_Exception ex)
	{
		cout << ex;
		return false;
	}

	cout << "verification: I'm sending\t";
	cout << *req;
	cout << endl;

	//res length = flag + Li2
	bzero(buffer, req->getLength());
	try
	{
		n = networkManager->receiveFromDevice(sockfd, buffer, res->getLength());
	}
	catch (phemap::PHEMAP_Verifier_Exception ex)
	{
		cout << ex;
		return false;
	}

	res->deserialize(buffer);

	cout << "verification: I'm receiving\t";
	cout << *res;
	cout << endl;

	if (false == this->directMode)
	{
		networkManager->closeSocket(sockfd);
	}
	free(buffer);
	delete networkManager;
	return true;
}

/**
 * @brief	Authenticates the received ChangeAddress message.
 * 			If authentication ends successfully, then sends the response message to the verifier.
 * @param 	req: pointer to the request message.
 * @param 	res: pointer to the response message.
 * @retval 	true if communication ends successfully, false otherwise.
*/
bool phemap::DeviceProxy::PHEMAP_deviceAuthentication(MessageChangeAddress *req, MessageAuth *res)
{
	int n, sockfd;
	VerifierNetworkUtils *networkManager = new VerifierNetworkUtils();
	try
	{
		sockfd = networkManager->createSocketToDevice(this->getDeviceHost(), this->getDevicePort());
	}
	catch (phemap::PHEMAP_Verifier_Exception ex)
	{
		cout << ex;
		return false;
	}

	uint8_t *buffer = (uint8_t *)malloc(req->getLength());
	req->serialize(buffer);

	try
	{
		n = networkManager->sendToDevice(sockfd, buffer, req->getLength());
	}
	catch (phemap::PHEMAP_Verifier_Exception ex)
	{
		cout << ex;
		return false;
	}

	cout << "I'm sending\t\tchange address request" << endl;
	cout << *req;
	cout << endl;

	//res length = flag + Li2
	bzero(buffer, req->getLength());
	try
	{
		n = networkManager->receiveFromDevice(sockfd, buffer, res->getLength());
	}
	catch (phemap::PHEMAP_Verifier_Exception ex)
	{
		cout << ex;
		return false;
	}

	res->deserialize(buffer);

	cout << "I'm receiving\tchange address response" << endl;
	cout << *res;
	cout << endl;

	networkManager->closeSocket(sockfd);
	free(buffer);
	delete networkManager;
	return true;
}

/**
 * @brief	Authenticates the received SaltInstall message.
 * 			If authentication ends successfully, then sends the response message to the verifier.
 * @param 	req: pointer to the request message.
 * @param 	res: pointer to the response message.
 * @retval 	true if communication ends successfully, false otherwise.
*/
bool phemap::DeviceProxy::PHEMAP_deviceAuthentication(SaltInstall *req, MessageAuth *res)
{
	int n, sockfd;
	VerifierNetworkUtils *networkManager = new VerifierNetworkUtils();
	try
	{
		sockfd = networkManager->createSocketToDevice(this->getDeviceHost(), this->getDevicePort());
	}
	catch (phemap::PHEMAP_Verifier_Exception ex)
	{
		cout << ex;
		return false;
	}

	uint8_t *buffer = (uint8_t *)malloc(req->getLength());
	req->serialize(buffer);

	try
	{
		n = networkManager->sendToDevice(sockfd, buffer, req->getLength());
	}
	catch (phemap::PHEMAP_Verifier_Exception ex)
	{
		cout << ex;
		return false;
	}

	cout << "I'm sending\t\tsalt install message" << endl;
	cout << *req;
	cout << endl;

	//res length = flag + Li2
	bzero(buffer, req->getLength());
	try
	{
		n = networkManager->receiveFromDevice(sockfd, buffer, res->getLength());
	}
	catch (phemap::PHEMAP_Verifier_Exception ex)
	{
		cout << ex;
		return false;
	}

	res->deserialize(buffer);

	cout << "I'm receiving\tsalt install response" << endl;
	cout << *res;
	cout << endl;

	networkManager->closeSocket(sockfd);
	free(buffer);
	delete networkManager;
	return true;
}
