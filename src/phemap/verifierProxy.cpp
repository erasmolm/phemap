//===- verifierProxy.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file verifierProxy.cpp
/// \author Mario Barbareschi
/// \brief This file defines the implementation of the VerifierProxy class
//===----------------------------------------------------------------------===//

#include "utils.h"

#include <iostream>
#include <iomanip>
#include <assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "verifierProxy.h"
#include "deviceNetworkUtils.h"
#include "deviceException.h"

using namespace std;

/**			
 * @brief 	Constructor for VerifierProxy.
 * @param 	CRBytelength: length in bytes of the Challenge and Response
 * @retval 	this A whole new VerifierProxy
 */
phemap::VerifierProxy::VerifierProxy(int CRByteLength) : VerifierSkeleton(CRByteLength)
{
}

/**			
 * @brief	Sends a probe message to the verifier, so that it will start the initiation.
 * @param	probe: pointer to the MessageProbe object.
 * retval	None
 */
void phemap::VerifierProxy::PHEMAP_sendProbe(const phemap::MessageProbe *probe)
{

	int sockfd, n;
	phemap::DeviceNetworkUtils *networkManager = new phemap::DeviceNetworkUtils();
	networkManager->setDeviceID(this->deviceID)->setVerifierHost(this->verifierHost)->setVerifierPort(this->verifierPort);

	try
	{
		sockfd = networkManager->createSocketToVerifier();
	}
	catch (phemap::PHEMAP_Device_Exception ex)
	{
		cout << ex;
	}

	uint8_t *buffer = (uint8_t *)malloc(probe->getLength());

	probe->serialize(buffer);

	//Send the packet *to* the Verifier
	try
	{
		n = networkManager->sendToVerifier(sockfd, buffer, probe->getLength());
	}
	catch (phemap::PHEMAP_Device_Exception ex)
	{
		cout << ex;
	}
	cout << "Probe: I'm sending\t\t";
	cout << *probe;
	cout << endl;

	networkManager->closeSocket(sockfd);
	free(buffer);
	delete networkManager;
	//return true;
}

/**
 * @brief 	Implements the network logic to send li1 link TO the device and receive li2 link FROM the device.
 * 			This function is used <s>by the Device</s> in order to start and complete the Verification Phase of PHEMAP Protocol.
 * 			A TCP socket with IPv4 addresses is opened and configured to commuinicate with the Verifier. The Device first creates
 * 			a new message in which puts the link li1 and its ID; then it wait for a response from the Verifier to obtain the value of
 * 			link l2.
 * @param 	deviceID A string representing the ID of the device.
 * @param 	Li1: The first link that the device wants <b>to</b> send to the Verifier according to PHEMAP Verification Phase. 
 * @param 	Li2: The second link that the device wants <b>from</b> the Verifier to complete PHEMAP Verification Phase.
 * @retval 	true if the communication goes fine, false otherwise.
 */
bool phemap::VerifierProxy::PHEMAP_verifierAuthentication(const string deviceID, MessageAuth *req, MessageAuth *res)
{

	int sockfd, n;
	phemap::DeviceNetworkUtils *networkManager = new phemap::DeviceNetworkUtils();

	if (this->useProxy == true)
	{
		networkManager->setDeviceID(this->deviceID)->setVerifierHost(this->proxyAddress)->setVerifierPort(this->proxyPort);
	}
	else
	{
		networkManager->setDeviceID(this->deviceID)->setVerifierHost(this->verifierHost)->setVerifierPort(this->verifierPort);
	}

	try
	{
		sockfd = networkManager->createSocketToVerifier();
	}
	catch (phemap::PHEMAP_Device_Exception ex)
	{
		cout << ex;
		return false;
	}

	//create a new message. It will be:
	//	|req|deviceID|
	//  0			MSB
	int messageSize = deviceID.size() + req->getLength();
	uint8_t *buffer = (uint8_t *)malloc(messageSize);

	//copy into the message li1 content
	req->serialize(buffer);

	//copy into the message the DeviceID string
	memcpy(buffer + req->getLength(), deviceID.c_str(), deviceID.size());

	//Send the packet *to* the Verifier
	try
	{
		n = networkManager->sendToVerifier(sockfd, buffer, messageSize);
	}
	catch (phemap::PHEMAP_Device_Exception ex)
	{
		cout << ex;
		return false;
	}
	cout << "verification: I'm sending\t" << deviceID << ";\t";
	cout << *req;
	cout << endl;

	//wait for a message *from* the Verifier. The device is waiting for li2
	bzero(buffer, messageSize);
	try
	{
		n = networkManager->receiveFromVerifier(sockfd, buffer, res->getLength());
	}
	catch (phemap::PHEMAP_Device_Exception ex)
	{
		cout << ex;
		return false;
	}
	res->deserialize(buffer);

	cout << "verification: I'm receiving\t\t\t";
	cout << *res;
	cout << endl;

	networkManager->closeSocket(sockfd);
	free(buffer);
	delete networkManager;
	return true;
}

/**
 * @brief 	Sends a BC PHEMAP ticket request to the verifier.											
 * @param	deviceID
 * @param 	req: pointer to the request message.
 * @param 	res: pointer to the response message.
 * @retval 	true if verification ends successfully, false otherwise.
 */
bool phemap::VerifierProxy::BC_PHEMAP_ticketRequest(const string deviceID, const BCTicketRequest *req, BCTicketResponse *res)
{
	int sockfd, n;
	phemap::DeviceNetworkUtils *networkManager = new phemap::DeviceNetworkUtils();
	networkManager->setDeviceID(this->deviceID)->setVerifierHost(this->verifierHost)->setVerifierPort(this->verifierPort);

	try
	{
		sockfd = networkManager->createSocketToVerifier();
	}
	catch (phemap::PHEMAP_Device_Exception ex)
	{
		cout << ex;
		return false;
	}

	//create a new message. It will be:
	//	|req|deviceID|
	//  0			MSB
	int messageSize = deviceID.size() + req->getLength();
	uint8_t *buffer = (uint8_t *)malloc(32035);

	//copy into the message li1 content
	req->serialize(buffer);

	//copy into the message the DeviceID string
	memcpy(buffer + req->getLength(), deviceID.c_str(), deviceID.size());

	//Send the packet *to* the Verifier
	try
	{
		n = networkManager->sendToVerifier(sockfd, buffer, messageSize);
	}
	catch (phemap::PHEMAP_Device_Exception ex)
	{
		cout << ex;
		return false;
	}
	cout << "BC Ticket Request: I'm sending\t\t";
	cout << *req;
	cout << endl;

	//wait for a message *from* the Verifier. The device is waiting for li2
	bzero(buffer, 32035);
	try
	{
		//n = networkManager->receiveFromVerifier(sockfd, buffer, 32035);
		int reps, rest;
		reps = 32035 / 2048;
		rest = 32035 % 2048;
		n = 0;
		for (int i = 0; i < reps; i++)
		{
			n = n + networkManager->receiveFromVerifier(sockfd, buffer + n, 2048);
		}

		n = n + networkManager->receiveFromVerifier(sockfd, buffer + n, rest);
	}
	catch (phemap::PHEMAP_Device_Exception ex)
	{
		cout << ex;
		return false;
	}
	res->deserialize(buffer);

	cout << "BC Ticket response: I'm receiving\t";
	cout << *res;
	cout << endl;

	networkManager->closeSocket(sockfd);
	free(buffer);
	delete networkManager;
	return true;
}