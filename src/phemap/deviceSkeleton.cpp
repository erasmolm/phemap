//===- deviceSkeleton.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file deviceSkeleton.cpp
/// \author Mario Barbareschi
/// \brief This file defines the implementation of the asbtract class
/// DeviceSkeleton
//===----------------------------------------------------------------------===//

#include "deviceSkeleton.h"
#include <assert.h>

/**
 * @brief	  Constructor for DeviceSkeleton.
 * @param 	None
 * @retval 	None
*/
phemap::DeviceSkeleton::DeviceSkeleton() : deviceID(""), linkCounter(0)
{
	state = DISCONNECTED;
	setSentinel(DEFAULT_SENTINEL);
}

/**
 * @brief	  Constructor for DeviceSkeleton.
 * @param 	deviceID
 * @retval 	None
*/
phemap::DeviceSkeleton::DeviceSkeleton(std::string deviceID) : deviceID(deviceID), linkCounter(0)
{
	state = DISCONNECTED;
	setSentinel(DEFAULT_SENTINEL);
}

/**
 * @brief	  Constructor for DeviceSkeleton.
 * @param 	deviceID
 * @param   sentinel
 * @retval 	None
*/
phemap::DeviceSkeleton::DeviceSkeleton(std::string deviceID, int sentinel) : deviceID(deviceID), linkCounter(0)
{
	state = DISCONNECTED;
	setSentinel(sentinel);
}

/** TODO erasmo, mancava deviceID, vedi se va bene
 * @brief	  Set the device Id value for the DeviceSkeleton.
 * @param 	DeviceID
 * @retval 	this
*/
phemap::DeviceSkeleton *phemap::DeviceSkeleton::setDeviceID(std::string deviceID)
{
	this->deviceID = deviceID;
	return this;
}

/**
 * @brief	  Returns the device Id attribute.
 * @param 	None
 * @retval 	deviceID
*/
std::string phemap::DeviceSkeleton::getDeviceID()
{
	return deviceID;
}

/**
 * @brief	  Returns the sentinel attribute.
 * @param 	None
 * @retval 	sentinel
*/
int phemap::DeviceSkeleton::getSentinel()
{
	return sentinel;
}

/**         
 * @brief	  Increments the link counter. Changes on the counter can be permanent or not.
 * @param 	permanent: sets whether to store changes on counter or not.
 * @retval 	Returns true if the link is not a sentinel
*/
bool phemap::DeviceSkeleton::countLink(bool permanent)
{
	if (permanent)
	{
		linkCounter = linkCounter + 1;
		return 0 != (linkCounter) % sentinel;
	}
	else
	{
		return 0 != (linkCounter + 1) % sentinel;
	}
}

/**         
 * @brief	  Reset link Counter.
 * @param 	None
 * @retval 	None
*/
void phemap::DeviceSkeleton::resetlinkCounter()
{
	linkCounter = 0;
}

/**
 * @brief	  Returns the state attribute.
 * @param 	None
 * @retval 	state
*/
phemap::DeviceState phemap::DeviceSkeleton::getState()
{
	return state;
}

/**       
 * @brief	  Set the state value for the DeviceSkeleton.
 * @param	  state
 * @retval 	this
*/
phemap::DeviceSkeleton *phemap::DeviceSkeleton::setState(phemap::DeviceState state)
{
	this->state = state;
	return this;
}

/**       
 * @brief	  Set the sentinel value for the DeviceSkeleton.
 * @param	  sentinel: sentinel period.
 * @retval 	this
*/
phemap::DeviceSkeleton *phemap::DeviceSkeleton::setSentinel(int sentinel)
{
	/*Sentinel must be greater than 3*/
	assert(sentinel > 3);
	this->sentinel = sentinel;
	return this;
}
