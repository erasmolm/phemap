//===- device.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file device.cpp
/// \author Mario Barbareschi
/// \brief This file defines the implementation of the Device class
//===----------------------------------------------------------------------===//

#include "utils.h"
#include <iostream>
#include <assert.h>
#include <cstring>
#include "device.h"
#include "deviceProxy.h"
#include "pufChainFromCarnet.h"
#include "deviceNetworkUtils.h"
#include "deviceException.h"

/**			
 * @brief	Constructor for Device.
 * @param 	deviceID
 * @param	puf: pointer to the Puf object.
 * @retval 	None
*/
phemap::Device::Device(std::string deviceID, phemap::Puf *puf) : DeviceSkeleton(deviceID)
{
	setPuf(puf);
	storage = (puf_t *)malloc(this->puf->getResponseByteLength() * sizeof(puf_t));
	salt = (puf_t *)malloc(this->puf->getResponseByteLength() * sizeof(puf_t));
	memset(salt, 0, this->puf->getResponseByteLength());
	verifierProxy = new phemap::VerifierProxy(this->puf->getResponseByteLength());
	this->verifierProxy->setDeviceID(deviceID);
}

/**
 * @brief	Constructor for Device.
 * @param 	deviceID
 * @param	puf: pointer to the Puf object.
 * @param	sentinel: sentinel period.
 * @retval 	None
*/
phemap::Device::Device(std::string deviceID, phemap::Puf *puf, int sentinel) : DeviceSkeleton(deviceID, sentinel)
{
	setPuf(puf);
	storage = (puf_t *)malloc(this->puf->getResponseByteLength() * sizeof(puf_t));
	salt = (puf_t *)malloc(this->puf->getResponseByteLength() * sizeof(puf_t));
	memset(salt, 0, this->puf->getResponseByteLength());
	verifierProxy = new phemap::VerifierProxy(this->puf->getResponseByteLength());
	this->verifierProxy->setDeviceID(deviceID);
}

/**
 * @brief	Returns a pointer to the Puf attribute.
 * @param	None
 * @retval 	Puf field of the message.
*/
phemap::Puf *phemap::Device::getPuf()
{
	return puf;
}

/**
 * @brief	Set the Puf value for the Device.
 * @param	Puf: pointer to the value to write to the Puf attribute.
 * @retval 	this
*/
phemap::Device *phemap::Device::setPuf(phemap::Puf *puf)
{
	/*Challenge and response must have the same length*/
	assert(puf->getChallengeByteLength() == puf->getResponseByteLength());
	this->puf = puf;
	return this;
}

/**
 * @brief	Tests if the response to the given challenge equals to the one expected.
 * 			If the next link is a sentinel, it will be tested anyway.
 * @param 	challenge: pointer to the challenge Puf object.
 * @param	response: pointer to the response Puf object.
 * @retval 	true if verification ends successfully, false otherwise.
*/
bool phemap::Device::verifyCRP(const puf_t *challenge, const puf_t *response)
{
	if (this->puf != NULL)
	{
		puf_t *pufResponse = (puf_t *)malloc(puf->getResponseByteLength() * sizeof(puf_t));
		this->puf->generateResponse(challenge, pufResponse);
		bool test = (0 == memcmp(pufResponse, response, puf->getResponseByteLength()));

		free(pufResponse);
		return test;
	}
	else
	{
		std::cerr << "The device has no a valid puf" << std::endl;
		return false;
	}
}

/**
 * @brief	Tests if the response to the given challenge equals to the one expected.
 * 			If the next link is a sentinel, it tests the successive link.
 * @param 	challenge: pointer to the challenge Puf link.
 * @param	response: pointer to the response Puf link.
 * @retval 	true if verification ends successfully, false otherwise.
*/
bool phemap::Device::verifyCRPWithSentinel(const puf_t *challenge, const puf_t *response)
{

	if (this->puf != NULL)
	{
		puf_t *pufResponse = (puf_t *)malloc(puf->getResponseByteLength() * sizeof(puf_t));
		memcpy(pufResponse, challenge, puf->getResponseByteLength());

		/*If next link is a sentinel, go further*/
		if (false == countLink(false))
		{
			this->puf->generateResponse(pufResponse, pufResponse);
		}
		this->puf->generateResponse(pufResponse, pufResponse);

		/*Compares the two responses*/
		bool test = (0 == memcmp(pufResponse, response, puf->getResponseByteLength()));

		free(pufResponse);
		return test;
	}
	else
	{
		std::cerr << "The device has no a valid puf" << std::endl;
		return false;
	}
}

/**
 * @brief	Increase the link counter and put next link in the storage. If Li is not-null
 * 			then it will contain the next link at the end of execution.
 * @param 	link: starting link from which to calculate the next one. 
 * @param	ignoreSentinel: determines whether to test sentinels or not.
 * @retval 	None
*/
void phemap::Device::getNextPufLink(puf_t *link, bool ignoreSentinel)
{
	/*If we must consider sentinels and next link is actually a sentinel, then go further*/
	if (false == ignoreSentinel && false == countLink(true))
	{
		this->puf->generateResponse(storage, storage);
		countLink(true);
	}
	this->puf->generateResponse(storage, storage);

	/*If specified, returns the storage content using the pointer to the link*/
	if (link != 0)
		memcpy(link, storage, puf->getResponseByteLength());
}

/**
 * @brief	Sends a probe message to the verifier, so that it will start the initiation.
 * @param 	None
 * @retval 	None
*/
void phemap::Device::PHEMAP_sendProbe()
{
	MessageProbe *probe = new MessageProbe(this->getDeviceID());
	verifierProxy->PHEMAP_sendProbe(probe);
}

/**
 * @brief	Starts the second phase of the PHEMAP initiation, given message1, message2 will be created. 
 * @param 	Message1: pointer to the first message of PHEMAP initiation.
 * @param 	Message2: pointer to the second message of PHEMAP initiation.
 * @retval 	true if verification ends successfully, false otherwise.
*/
bool phemap::Device::PHEMAP_initiation_phase2(const phemap::Message1 *message1, phemap::Message2 *message2)
{
	bool outcome = false;

	puf_t *xorAcc = (puf_t *)malloc(puf->getResponseByteLength() * sizeof(puf_t));
	xorVector(xorAcc, message1->getV1(), message1->getV2(), puf->getResponseByteLength());

	puf_t *pufChain = (puf_t *)malloc(puf->getResponseByteLength() * sizeof(puf_t));
	memcpy(pufChain, message1->getLi(), puf->getResponseByteLength());

	/*Instead of verifying the accumulation of xor, we can elide each link from the v1 xor v2*/
	for (int i = 0; i < this->sentinel - 2; i++)
	{
		this->puf->generateResponse(pufChain, pufChain);
		xorVector(xorAcc, xorAcc, pufChain, puf->getResponseByteLength());
	}
	/*At this point, xorAcc should contain the link l[i+S-1], that can be easily verified*/
	if (!verifyCRP(pufChain, xorAcc))
	{
		std::cerr << "Error:  check failed" << std::endl;
	}
	else
	{
		outcome = true;
		flushStorage();
		/*We need to go further!*/
		this->puf->generateResponse(pufChain, pufChain);

		puf_t *nonce = (puf_t *)malloc(puf->getResponseByteLength() * sizeof(puf_t));
		puf_t *d1 = (puf_t *)malloc(puf->getResponseByteLength() * sizeof(puf_t));
		puf_t *d2 = (puf_t *)malloc(puf->getResponseByteLength() * sizeof(puf_t));

		getRandomNonce(nonce, puf->getResponseByteLength());

		this->puf->generateResponse(pufChain, pufChain);
		xorVector(d1, pufChain, nonce, puf->getResponseByteLength());

		this->puf->generateResponse(pufChain, pufChain);
		xorVector(d2, pufChain, nonce, puf->getResponseByteLength());

		setStorage(d2);

		setState(INITIATED);

		message2->setD1(d1);
		message2->setD2(d2);
		free(d1);
		free(d2);
		free(nonce);
	}

	free(xorAcc);
	free(pufChain);
	return outcome;
}

/**
 * @brief	Starts the fourth phase of the PHEMAP initiation.  
 * @param 	Message3: pointer to the third message of PHEMAP initiation.
 * @retval 	true if verification ends successfully, false otherwise.
*/
bool phemap::Device::PHEMAP_initiation_phase4(const phemap::Message3 *message3)
{
	bool outcome = false;
	puf_t *pufChain = (puf_t *)malloc(puf->getResponseByteLength() * sizeof(puf_t));
	puf_t *xorAcc = (puf_t *)malloc(puf->getResponseByteLength() * sizeof(puf_t));
	puf_t *v3XorD2 = (puf_t *)malloc(puf->getResponseByteLength() * sizeof(puf_t));
	memcpy(pufChain, message3->getLi(), puf->getResponseByteLength());

	for (int i = 0; i < this->sentinel + 1; i++)
	{
		this->puf->generateResponse(pufChain, pufChain);
	}
	/*At this point, pufchain should contain L(i+s+1) link*/
	memcpy(xorAcc, pufChain, puf->getResponseByteLength());
	this->puf->generateResponse(pufChain, pufChain);
	xorVector(xorAcc, xorAcc, pufChain, puf->getResponseByteLength());
	xorVector(v3XorD2, this->storage, message3->getV3(), puf->getResponseByteLength());

	if (0 != memcmp(v3XorD2, xorAcc, puf->getResponseByteLength()))
	{
		std::cerr << "Error: message3 check failed" << std::endl;
	}
	else
	{
		outcome = true;
		flushStorage();
		this->puf->generateResponse(pufChain, pufChain);
		setStorage(pufChain);
		setState(AUTHENTICATED);
		/** Set sentinel from now */
		resetlinkCounter();
		/** Reset proxy parameters */
		this->getVerifierProxy()->setProxyPort(-1);
		this->getVerifierProxy()->setProxyAddress("localhost");
		this->getVerifierProxy()->useProxy = false;
		this->authenticationMode = AMODE_NORMAL;
		std::cout << "Initiation by the verifier completed!" << std::endl;
	}

	free(pufChain);
	free(v3XorD2);
	free(xorAcc);
	return outcome;
}

/**
 * @brief	Empty the secure register of the device.
 * @param 	None
 * @retval	None
*/
void phemap::Device::flushStorage()
{
	memset(storage, 0x00, puf->getResponseByteLength());
}

/**
 * @brief	Empty device salt register.
 * @param 	None
 * @retval	None
*/
void phemap::Device::flushSalt()
{
	memset(salt, 0x00, puf->getResponseByteLength());
}

/**
 * @brief	Set the value for the secure register of the device.
 * @param 	value: pointer to the value to write to the register.
 * @retval	this
*/
phemap::Device *phemap::Device::setStorage(puf_t *value)
{
	memcpy(storage, value, puf->getResponseByteLength());
	return this;
}

/**
 * @brief	Set the value for the secure register of the device.
 * @param 	value: pointer to the value to write to the register.
 * @retval	None
*/
void phemap::Device::setSalt(puf_t *value)
{
	memcpy(salt, value, puf->getResponseByteLength());
}

/**
 * @brief	Destructor for Device.
 * @param 	None
 * @retval 	None
*/
phemap::Device::~Device()
{
	free(storage);
	free(verifierProxy);
}

/**
 * @brief	Authenticates the received MessageAuth message.
 * 			If authentication ends successfully, then sends the response message to the verifier.
 * @param 	req: pointer to the request message.
 * @param 	res: pointer to the response message.
 * @retval 	true if communication ends successfully, false otherwise.
*/
bool phemap::Device::PHEMAP_deviceAuthentication(MessageAuth *req, MessageAuth *res)
{

	/*Compare given li1 with the one expected */
	if (verifyCRPWithSentinel(storage, req->getLi()))
	{
		/*Update the counter twice and return the li2 link*/
		getNextPufLink(0, false);
		getNextPufLink(res->getLi(), false);

		return true;
	}
	else
	{
		return false;
	}
}

/**
 * @brief	Sends a BC PHEMAP ticket request to the verifier.
 * @param 	flag: flag of the message.
 * @param	targetID
 * @param	numOfTickets: number of requested tickets.
 * @retval 	true if verification ends successfully, false otherwise.
*/
int phemap::Device::BC_PHEMAP_ticketRequest(phemap::MSG_FLG flag, string targetID, int numOfTickets)
{
	int outcome = 0;
	string targetHost = "";
	int targetPort = -1;

	puf_t *li = (puf_t *)malloc(puf->getResponseByteLength());
	puf_t *li2 = (puf_t *)malloc(puf->getResponseByteLength());

	/** Retrieve link Ai1 */
	this->getNextPufLink(li, false);

	/** Retrieve link Ai2 to XOR with targetID */
	this->getNextPufLink(li2, false);
	xorVector(li2, li2, (const unsigned char *)targetID.c_str(), targetID.size());

	BCTicketRequest *req = new BCTicketRequest(numOfTickets, li2, li);
	BCTicketResponse *res = new BCTicketResponse();

	if (flag == phemap::BC_IM_TCK_REQ || flag == phemap::BC_EX_TCK_REQ)
	{
		req->setFlag(flag);
	}
	else
	{
		cerr << "\n\nError in BC ticket request: flag not admitted: " << flag << "\n\n";
		return -1;
	}

	/** Send ticket request to the verifier */
	this->verifierProxy->BC_PHEMAP_ticketRequest(this->getDeviceID(), req, res);

	/** Update storage register with new puf response and increase link counter */
	getNextPufLink(0, false);

	/** Compare received link from the verifier with the one expected */
	if (0 == memcmp(res->getLi(), storage, puf->getResponseByteLength()))
	{
		outcome = res->getCarnetLength();
		targetHost = res->getTargetHost();
		targetPort = res->getTargetPort();

		/** If request ended succesfully, prepare for BC authentication */
		if (outcome > 0)
		{
			/** Create a device manager to handle bc tickets */
			phemap::DeviceManager *targetManager = new phemap::DeviceManager();
			phemap::PufChainFromCarnet *pufChainFromCarnet = new phemap::PufChainFromCarnet(res->getCarnet(), outcome, puf->getResponseByteLength());
			phemap::DeviceProxy *targetProxy = new phemap::DeviceProxy(targetID, targetHost, targetPort, pufChainFromCarnet);
			targetManager->setDeviceProxy(targetProxy);

			/** Save BC target status */
			this->setBCTickets(outcome);
			this->setBCTargetManager(targetManager);
			if (flag == phemap::BC_IM_TCK_REQ)
			{
				this->authenticationMode = phemap::AMODE_BC_IMPLICIT;
			}
			else if (flag == phemap::BC_EX_TCK_REQ)
			{
				this->authenticationMode = phemap::AMODE_BC_EXPLICIT;
			}
		}
	}
	else
	{
		cout << "\nBC PHEMAP - Ticket response NOT authenticated";
		outcome = -1;
	}

	delete req;
	delete res;

	return outcome;
}

/**
 * @brief	Starts a verification message exchange using BC PHEMAP.
 * @param 	body: body of the message.
 * @retval 	true if verification ends successfully, false otherwise.
*/
bool phemap::Device::BC_PHEMAP_startVerification(string body)
{
	bool outcome = false;
	authModeFlag status = this->authenticationMode;

	puf_t *ticket = (puf_t *)malloc(puf->getResponseByteLength());
	puf_t *liA = (puf_t *)malloc(puf->getResponseByteLength());
	BCTargetManager->getDeviceProxy()->getPufChain()->getNextLink(ticket);

	this->getNextPufLink(liA, false);

	xorVector(ticket, ticket, liA, puf->getResponseByteLength()); //now in ticket there is the link liB

	MessageAuth *req = new MessageAuth(ticket, puf->getResponseByteLength(), body);
	MessageAuth *res = new MessageAuth();

	/** Update remaining tickets */
	this->setBCTickets(this->BCTickets - 2);
	if (this->getBCTickets() == 0 && status == phemap::AMODE_BC_IMPLICIT)
	{
		this->authenticationMode = phemap::AMODE_NORMAL;
		/** For last message, send UNSET_PROXY */
		req->setFlag(UNSET_PROXY);
	}

	/** Send request to the device and receive response */
	BCTargetManager->getDeviceProxy()->PHEMAP_deviceAuthentication(req, res);

	/** Take next ticket */
	BCTargetManager->getDeviceProxy()->getPufChain()->getNextLink(ticket);

	/** Take next LiA link */
	this->getNextPufLink(liA, false);

	if (status == phemap::AMODE_BC_IMPLICIT)
	{
		/** Calculate xor between next LiA link and next ticket */
		xorVector(ticket, ticket, liA, puf->getResponseByteLength());
		req->setLi(ticket);
	}
	else if (status == phemap::AMODE_BC_EXPLICIT)
	{
		req->setLi(liA);
	}

	/** Compare received link from the verifier with the one expected */
	if (0 == memcmp(req->getLi(), res->getLi(), puf->getResponseByteLength()))
	{
		outcome = true;
	}
	else
	{
		outcome = false;
	}

	delete req;
	delete res;
	free(ticket);
	free(liA);

	return outcome;
}

/**
 * @brief	Starts a verification message exchange using classic phemap.
 * @param 	body: body of the message.
 * @retval 	true if verification ends successfully, false otherwise.
*/
bool phemap::Device::PHEMAP_startVerification(string body)
{
	bool outcome = false;

	/*Check if the device has been authenticated previously*/
	if (INITIATED == getState() || AUTHENTICATED == getState())
	{
		/*Update storage register with new puf response and increase link counter*/
		getNextPufLink(0, false);

		MessageAuth *req = new MessageAuth(storage, puf->getResponseByteLength(), body);
		MessageAuth *res = new MessageAuth();

		/*Authentication messages actually start from here*/
		verifierProxy->PHEMAP_verifierAuthentication(getDeviceID(), req, res);

		/** If it is an UNSET_PROXY response */
		if (res->getFlag() == phemap::UNSET_PROXY)
		{
			this->getVerifierProxy()->setProxyPort(-1);
			this->getVerifierProxy()->setProxyAddress("localhost");
			this->getVerifierProxy()->useProxy = false;
			this->authenticationMode = AMODE_NORMAL;
		}

		/*Update storage register with new puf response and increase link counter*/
		getNextPufLink(0, false);

		/*Compare received link with the one expected*/
		if (0 == memcmp(res->getLi(), storage, puf->getResponseByteLength()))
		{
			outcome = true;
		}
		else
		{
			outcome = false;
		}
	}
	else
	{
	std:
		std::cerr << "Device not initiated, cannot authenticate the verifier " << std::endl;
		outcome = false;
	}
	return outcome;
}

/**
 * @brief	Starts a verification message exchange using SALTED PHEMAP.
 * @param 	body: body of the message.
 * @retval 	true if verification ends successfully, false otherwise.
*/
bool phemap::Device::SALT_PHEMAP_startVerification(string body)
{
	bool outcome = false;

	/*Check if the device has been authenticated previously*/
	if (INITIATED == getState() || AUTHENTICATED == getState())
	{
		/*Update storage register with new puf response and increase link counter*/
		getNextPufLink(0, false);

		MessageAuth *req = new MessageAuth(storage, puf->getResponseByteLength(), body);
		MessageAuth *res = new MessageAuth();

		/** Xor current link with salt register value */
		xorVector(req->getLi(), req->getLi(), this->getSalt(), req->getPufLength());

		/*Authentication messages actually start from here*/
		verifierProxy->PHEMAP_verifierAuthentication(getDeviceID(), req, res);

		/** Xor received link with salt register value */
		xorVector(res->getLi(), res->getLi(), this->getSalt(), res->getPufLength());

		/** Update ticket counter */
		this->setSaltCounter(getSaltCounter() - 2);
		if (this->getSaltCounter() == 0 && authenticationMode == phemap::AMODE_SALTED)
		{
			/** For last message, unset the proxy and switch to normal mode */
			this->authenticationMode = phemap::AMODE_NORMAL;
			this->getVerifierProxy()->setProxyPort(-1);
			this->getVerifierProxy()->setProxyAddress("localhost");
			this->flushSalt();
			this->getVerifierProxy()->useProxy = false;
		}

		/*Update storage register with new puf response and increase link counter*/
		getNextPufLink(0, false);

		/*Compare received link with the one expected*/
		if (0 == memcmp(res->getLi(), storage, puf->getResponseByteLength()))
		{
			outcome = true;
		}
		else
		{
			outcome = false;
		}
	}
	else
	{
	std:
		std::cerr << "Device not initiated, cannot authenticate the verifier " << std::endl;
		outcome = false;
	}
	return outcome;
}
/**		
 * @brief	Sends a SALTED PHEMAP ticket request to the verifier.	
 * @param	req: pointer to the request message.
*/
void phemap::Device::SALT_PHEMAP_ticketRequestFromDevice(phemap::SaltTicketRequest *req)
{
	int sockfd, n;
	phemap::DeviceNetworkUtils *networkManager = new phemap::DeviceNetworkUtils();
	networkManager->setDeviceID(req->getOriginID())->setVerifierHost(req->getOriginHost())->setVerifierPort(req->getOriginPort());

	try
	{
		sockfd = networkManager->createSocketToVerifier();
	}
	catch (phemap::PHEMAP_Device_Exception ex)
	{
		cout << ex;
	}

	uint8_t *buffer = (uint8_t *)malloc(2048);

	req->serialize(buffer);

	try
	{
		n = networkManager->sendToVerifier(sockfd, buffer, req->getLength());
	}
	catch (phemap::PHEMAP_Device_Exception ex)
	{
		cout << ex;
	}
	cout << "Salt Ticket Request: I'm sending\t\t";
	cout << *req;
	cout << endl;

	networkManager->closeSocket(sockfd);
	free(buffer);
	delete networkManager;
}