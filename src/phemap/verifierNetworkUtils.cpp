//===- verifierNetworkUtils.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file verifierNetworkUtils.cpp
/// \author Andrea Aletto
/// \brief This file defines the implementation of the VerifierNetworkUtils class
//===----------------------------------------------------------------------===//
#include "verifierNetworkUtils.h"
#include <errno.h>
#include <string.h>
#include "verifierException.h"

/**
 * @brief	Create Listener Socket.
 * @param 	None
 * @retval 	Socket file descriptor.
*/
int phemap::VerifierNetworkUtils::createListenerSocket() 
{
	int sockfd, portno;

	struct sockaddr_in serv_addr, cli_addr;

	// clear address structure
	bzero((char *)&serv_addr, sizeof(serv_addr));

	// init port number for Verifier Server
	portno = this->verifierPort;

	// setup the host_addr (32bit address) structure for use in bind call
	serv_addr.sin_family = AF_INET;

	// automatically be filled with current host's IP address
	serv_addr.sin_addr.s_addr = INADDR_ANY;

	// convert short integer value for port must be converted into network byte order
	serv_addr.sin_port = htons(portno);

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0)
	{
		string desc = string("\nCan't open a listener socket on verifier server.");
		throw phemap::PHEMAP_Verifier_Exception("V001", desc);
	}

	int t = 1;
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &t, sizeof(int)) == -1)
	{
		string desc = string("\nCan't set the listener socket immediately reusable on verifier server.");
		throw phemap::PHEMAP_Verifier_Exception("V002", desc);
	}

	// bind(int fd, struct sockaddr *local_addr, socklen_t addr_length)
	// bind() passes file descriptor, the address structure,
	// and the length of the address structure
	// This bind() call will bind the socket to the current IP address on port, portno
	if (bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
	{
		string desc = string("\nCan't complete the bind of the socket on verifier server.");
		throw phemap::PHEMAP_Verifier_Exception("V003", desc);
	}

	// This listen() call tells the socket to listen to the incoming connections.
	// The listen() function places all incoming connection into a backlog queue
	// until accept() call accepts the connection.
	// Here, we set the maximum size for the backlog queue to 5.
	listen(sockfd, 5);

	return sockfd;
}

/**
 * @brief	Accept connection onto sockfd and generate a new socket file descriptor (newsockfd).
 * @param 	sockfd file descriptor.
 * @retval 	newsockfd file descriptor of the accepted connection.
*/
int phemap::VerifierNetworkUtils::acceptConnectionFrom(const int sockfd)
{

	struct sockaddr_in cli_addr;
	int newsockfd;
	socklen_t clilen;
	// The accept() call actually accepts an incoming connection
	clilen = sizeof(cli_addr);

	// This accept() function will write the connecting client's address info
	// into the the address structure and the size of that structure is clilen.
	// The accept() returns a new socket file descriptor for the accepted connection.
	// So, the original socket file descriptor can continue to be used
	// for accepting new connections while the new socker file descriptor is used for
	// communicating with the connected client.
	newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen);
	if (newsockfd < 0)
	{
		string desc = string("\nCan't accept incoming connection onto the listener socket on verifier server.");
		throw phemap::PHEMAP_Verifier_Exception("V004", desc);
	}

	//printf("\n\nVerifier server: got connection from %s port %d\n", inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port));

	return newsockfd;
}

/**			
 * @brief	Create a socket file descriptor to communicate with device.
 * @param 	host
 * @param	port
 * @retval 	sockfd file descriptor
*/
int phemap::VerifierNetworkUtils::createSocketToDevice(string host, int port)
{

	int sockfd, portno;
	struct sockaddr_in serv_addr;
	struct hostent *server;

	portno = port;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0)
	{
		string desc = string("\nCan't open a new socket on verifier server.");
		throw phemap::PHEMAP_Verifier_Exception("V005", desc);
	}

	server = gethostbyname(host.c_str());
	if (server == NULL)
	{
		string desc = string("\nHost unreachable.");
		throw phemap::PHEMAP_Verifier_Exception("V006", desc);
	}
	bzero((char *)&serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr,
		  (char *)&serv_addr.sin_addr.s_addr,
		  server->h_length);
	serv_addr.sin_port = htons(portno);
	if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
	{
		string desc = string("\nCan't connect to the socket on verifier server.");
		throw phemap::PHEMAP_Verifier_Exception("V007", desc);
	}

	return sockfd;
}

/**
 * @brief	The verifier receives from the device a stream of bytes. 
 * @param 	sockfd: socket to verifier.
 * @param	outputBuffer: stream of bytes.
 * @param	length: maximum number of bytes to read.
 * @retval 	n: number of received bytes.
*/
int phemap::VerifierNetworkUtils::receiveFromDevice(int sockfd, uint8_t *outputBuffer, int length)
{

	int n = read(sockfd, outputBuffer, length);
	if (n < 0)
	{
		string desc = string("\nCan't read from the socket on verifier server.");
		throw phemap::PHEMAP_Verifier_Exception("V008", desc);
	}
	return n;
}


/**
 * @brief	The verifier sends to the device a stream of bytes. 
 * @param 	sockfd: socket to verifier.
 * @param	outputBuffer: stream of bytes.
 * @param	length: maximum number of bytes to write.
 * @retval 	n: number of sent bytes.
*/
int phemap::VerifierNetworkUtils::sendToDevice(int sockfd, uint8_t *outputBuffer, int length)
{

	int n = write(sockfd, outputBuffer, length);
	if (n < 0)
	{
		string desc = string("\nCan't write to the socket on verifier server.");
		throw phemap::PHEMAP_Verifier_Exception("V009", desc);
	}
	return n;
}

/**
 * @brief	close the connection with device.
 * @param 	sockfd file descriptor
 * @retval 	none
*/
void phemap::VerifierNetworkUtils::closeSocket(int sockfd)
{
	close(sockfd);
}