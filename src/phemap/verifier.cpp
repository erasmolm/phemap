//===- verifier.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file verifier.cpp
/// \author Mario Barbareschi
/// \brief This file defines the implementation of the Verifier class
//===----------------------------------------------------------------------===//

#include "verifier.h"

/**			
 * @brief	Constructor for Verifier.
 * @param	CRBytelength: length in bytes of the Challenge and Response
 * @retval	None
 */
phemap::Verifier::Verifier(int CRByteLength) : VerifierSkeleton(CRByteLength)
{
	//Init the device map of the verifier to an empty structure <string, DeviceManager>
	devicesMap = new DevicesMap_t();
	babelMap = new BabelMap_t();
}

/**
 * @brief	Returns the Device map.
 * @param	None
 * @retval	deviceMap: A reference to a DeviceMap_t object 
 */
DevicesMap_t *phemap::Verifier::getDevicesMap()
{
	return devicesMap;
}

/**
 * @brief	Returns the Babel map.
 * @param	None
 * @retval	babelMap: A reference to a BabelMap_t object 
 */
BabelMap_t *phemap::Verifier::getBabelMap()
{
	return babelMap;
}

/**
 * @brief	Add a new entry to the babelMap. 
 * @param	targetID: ID of the babelchain target.
 * @param	deviceID: ID of the babelchain origin.
 * @retval	None
 */
void phemap::Verifier::BC_addToBabelMap(string targetID, string deviceID)
{
	chrono::milliseconds now = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch());
	getBabelMap()->push_back(BCBound_t(targetID, deviceID, now));
	cout << "\n\nNew bound inserted into babelMap: \n";
	cout << std::get<0>(getBabelMap()->back()) << " ";
	cout << std::get<1>(getBabelMap()->back()) << " ";
	cout << std::dec << std::get<2>(getBabelMap()->back()).count();
	cout << endl;
}

/**
 * @brief	Search for passed ID in the first and the second field of BabelMap
 * @param	id: the deviceID to search.
 * @retval	returns -1 if not found. Otherwise returns the index of the first occurence. 
 */
int phemap::Verifier::BC_searchBound(string id)
{
	bool found = false;
	int i = 0;

	while (i < babelMap->size() && !found)
	{
		if (std::get<0>(babelMap->at(i)).compare(id) == 0 ||
			std::get<1>(babelMap->at(i)).compare(id) == 0)
		{
			found = true;
		}
		else
		{
			i++;
		}
	}

	if (found == true)
	{
		return i;
	}
	else
	{
		return -1;
	}
}

/**
 * @brief	Checks if the timestamp for the given index has expired.
 * @param	index: occurrence of the babelMap to check.
 * @retval	returns true if expired. False otherwise.
 */
bool phemap::Verifier::BC_hasExpired(int index)
{
	bool outcome = true;

	chrono::milliseconds end = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch());
	chrono::milliseconds start = std::get<2>(babelMap->at(index));

	cout << "\n\n end:     " << end.count();
	cout << "\n start:   " << start.count();
	cout << "\n elapsed: " << end.count() - start.count();

	if (end.count() - start.count() > BC_BOUND_DURATION_MS)
	{
		cout << "\n timestamp expired\n\n";
		outcome = true;
	}
	else
	{
		cout << "\n timestamp NOT expired\n\n";
		outcome = false;
	}

	return outcome;
}

/**
 * @brief	Try to identify and authenticate a device in Initiation Phase.
 * 			This function first identify a particular device by the knowledge of its deviceID
 * 			given in input; then, if a device is actually found, tries to authenticate it using
 * 			the Initiation Phase of PHEMAP protocol, actually implemented into the init function.
 * @param 	deviceID: A string representing the id of the device to identify and authenticate
 * @retval 	true if a device of that given ID is found and authenticated, false otherwise.
 * 
 */
bool phemap::Verifier::PHEMAP_startInitiation(std::string deviceID)
{
	DevicesMap_t::const_iterator devices = devicesMap->find(deviceID);
	if (devices == devicesMap->end())
	{
		std::cerr << "Device " << deviceID << " not found" << std::endl;
		return false;
	}

	return devices->second->PHEMAP_startInitiation();
}

/**
 * @brief 	Try to identify and authenticate a device in Verification Phase.
 * 			This function first identify a particular device by the knowledge of its deviceID
 * 			given in input; then, if a device is actually found, tries to authenticate it using
 * 			the Verification Phase of PHEMAP protocol, actually implemented into the authenticateDevice
 * 			function.
 * @param 	deviceID: A string representing the id of the device to identify and authenticate
 * @param	body: body of the message.
 * @retval 	true if a device of that given ID is found and authenticated, false otherwise.
 * 
 */
bool phemap::Verifier::PHEMAP_startVerification(std::string deviceID, string body)
{
	DevicesMap_t::const_iterator devices = devicesMap->find(deviceID);
	if (devices == devicesMap->end())
	{
		std::cerr << "Device " << deviceID << " not found" << std::endl;
		return false;
	}
	return devices->second->PHEMAP_startVerification(body);
}

/**
 * @brief 	Given a device ID, searches the device in the devices map and calls the
 * 			corresponding device manager. The latter verifies the device by looking for
 * 			the puf chain stored within the verifier.
 * @param	deviceID: A string representing the id of the device to authenticate.
 * @param	li1: A reference to a link li1 of the current chain.
 * @param	li2: A reference to a link li2 of the current chain.
 * @retval	true if ends successfully, false otherwise.
 */
bool phemap::Verifier::PHEMAP_verifierAuthentication(const string deviceID, MessageAuth *req, MessageAuth *res)
{

	/*Search for the device in the devices map*/
	DevicesMap_t::const_iterator devices = devicesMap->find(deviceID);
	if (devices == devicesMap->end())
	{
		std::cerr << "Device " << deviceID << " not found" << std::endl;
		return false;
	}

	/*Verify the device using device manager and puf chain stored*/
	return devices->second->PHEMAP_verificationFromChain(req->getLi(), res->getLi());
}

/**
 * @brief 	Destructor for Verifier.
 * @param 	None
 * @retval 	None
 */
phemap::Verifier::~Verifier()
{
	free(devicesMap);
}
