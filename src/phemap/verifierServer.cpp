//===- verifierServer.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file verifierServer.cpp
/// \author Mario Barbareschi
/// \brief This file defines the implementation of the VerifierServer class
//===----------------------------------------------------------------------===//

#include "utils.h"
#include "verifierServer.h"

#include <iostream>
#include <fstream>
#include <iomanip>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "verifierException.h"
#include "messageProbe.h"
#include "bcTicketRequest.h"
#include "bcTicketResponse.h"
#include "saltTicketRequest.h"
#include "saltTicketResponse.h"
#include "saltInstall.h"
#include "messageChangeAddress.h"
#include "pufChainFromFile.h"

using namespace std;

/**
 * @brief	Constructor for VerifierServer.
 * @param 	verifier: pointer to the verifier object.
 * @retval 	None
*/
phemap::VerifierServer::VerifierServer(phemap::Verifier *verifier) : verifier(verifier)
{
}

/**
 * @brief	Set the verifier value for the VerifierServer.
 * @param	verifier: pointer to the value to write to the verifier attribute.
 * @retval 	this
*/
phemap::VerifierServer *phemap::VerifierServer::setVerifier(phemap::Verifier *verifier)
{
	this->verifier = verifier;
	return this;
}

/**
 * @brief 	Main task of Verifier Server
 * 			This function opens a TCP Socket onto the verifier port (statically written in VerifierSkeleton.h) and
 * 			first waits for a packet from a device. Then, obtained li1 link, it authenticates the device sending li2.
 * @param 	None
 * @retval 	None
 */
void phemap::VerifierServer::runVerifierServer()
{
	int sockfd;
	int directSock = 0;
	VerifierNetworkUtils *networkManager = new VerifierNetworkUtils();
	networkManager->setVerifierHost(this->verifierHost)->setVerifierPort(this->verifierPort);
	try
	{
		sockfd = networkManager->createListenerSocket();
	}
	catch (phemap::PHEMAP_Verifier_Exception ex)
	{
		cerr << ex;
	}

	while (true)
	{
		int newsockfd, n;
		try
		{
			newsockfd = networkManager->acceptConnectionFrom(sockfd);
		}
		catch (phemap::PHEMAP_Verifier_Exception ex)
		{
			cerr << ex;
		}

		uint8_t *buffer = (uint8_t *)malloc(32035);

		try
		{
			n = networkManager->receiveFromDevice(newsockfd, buffer, 32035);
		}
		catch (phemap::PHEMAP_Verifier_Exception ex)
		{
			cerr << ex;
		}

		//n is the number of characters read by the previous read call. The server expects to receive a packet like:
		//	|req|deviceID|
		//	0			MSB
		//Thus, the deviceID is long n-lengthOfLink (second parameter),
		//and starts from buffer+lengthOfTheLink  (first parameter)

		/** Read the message flag from buffer */
		flag_t msg_flag = 0xFF;
		memcpy(&msg_flag, buffer, sizeof(flag_t));

		switch (msg_flag)
		{
		case (phemap::VER_STR):
		{
			MessageAuth *req = new MessageAuth();
			req->deserialize(buffer);

			int deviceIDSize = n - req->getLength();
			string deviceID((char *)(buffer + req->getLength()), deviceIDSize);

			cout << "VerifierServer: I'm receiving\t"
				 << "\ndeviceID: " << deviceID << endl
				 << *req
				 << endl;

			//Server autheticates the device, passing to the verifier object the deviceID and the link received (first and second param).
			//It will receive the next link into the buffer variable (third param).

			MessageAuth *res = new MessageAuth();

			if (verifier->PHEMAP_verifierAuthentication(deviceID, req, res))
			{

				bzero(buffer, 32035);
				res->serialize(buffer);
				try
				{
					networkManager->sendToDevice(newsockfd, buffer, res->getLength());

					/** Write current timestamp to the log file */
					// chrono::milliseconds now = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch());
					// ofstream logFile("../resources/log.txt", std::ofstream::out | std::ofstream::app);
					// logFile << now.count() << endl;
					// logFile.close();
				}
				catch (phemap::PHEMAP_Verifier_Exception ex)
				{
					cerr << ex;
				}

				cout << "VerifierServer: I'm sending\t\t"
					 << "\ndeviceID: " << deviceID << endl
					 << *res
					 << endl;
			}
			else
			{
				cerr << "Authentication from device failed" << endl;
			}
		}
		break;

		case (phemap::PROBE):
		{
			MessageProbe *probe = new MessageProbe();
			probe->deserialize(buffer, n);
			cout << "\n[NOTE] Received probe message from device '" << probe->getDeviceId() << "'\n";

			/** Determine if device is STM32L4 (set direct mode) */
			if(probe->getDeviceId().compare("L4") == 0)
			{
				DevicesMap_t::const_iterator it = verifier->getDevicesMap()->find(probe->getDeviceId());
				
				it->second->getDeviceProxy()->setDirectMode(true);

				/** save this socket for direct mode */
				it->second->getDeviceProxy()->setDirectSocket(newsockfd);
				directSock = newsockfd;
				newsockfd = -1;
				cout << "\n[NOTE] Switching to direct mode for device '" << probe->getDeviceId() << "'\n";
			}

			if (verifier->PHEMAP_startInitiation(probe->getDeviceId()))
			{
				cout << probe->getDeviceId() << " initiated and authenticated\n\n";
			}
			else
			{
				cout << probe->getDeviceId() << " not initiated\n\n";
			}
		}
		break;

		case (phemap::BC_EX_TCK_REQ):
		{
			//	|req|deviceID|
			//	0			MSB

			BCTicketRequest *firstReq = new BCTicketRequest();
			BCTicketResponse *firstRes = new BCTicketResponse();
			firstReq->deserialize(buffer);

			/** Retrieve the ID of the origin device from the buffer. */
			int deviceIDSize = n - firstReq->getLength();
			string firstDeviceID((char *)(buffer + firstReq->getLength()), deviceIDSize);

			if (verifier->PHEMAP_verifierAuthentication(firstDeviceID, firstReq, firstRes))
			{
				puf_t *firstTmpLink = (puf_t *)malloc(firstReq->getPufLength());
				xorVector(firstTmpLink, firstReq->getTargetID(), firstRes->getLi(), firstRes->getPufLength());
				string firstTargetID((const char *)firstTmpLink);
				int targetIndex = verifier->BC_searchBound(firstTargetID);
				bool targetBounded = false;
				bool boundExpired = true;

				if (targetIndex > -1)
				{
					targetBounded = true;
					boundExpired = verifier->BC_hasExpired(targetIndex);
				}

				/** If bound has expired, than reinit the target and delete the bound from babelMap*/
				DevicesMap_t::const_iterator firstItTarget = verifier->getDevicesMap()->find(firstTargetID);
				DevicesMap_t::const_iterator firstItOrigin = verifier->getDevicesMap()->find(firstDeviceID);
				if (firstItTarget != verifier->getDevicesMap()->end() && targetBounded && boundExpired)
				{
					verifier->PHEMAP_startInitiation(firstTargetID);
					verifier->getBabelMap()->erase(verifier->getBabelMap()->begin() + targetIndex);
					targetBounded = false;
					boundExpired = true;

					cout << "\n\nBabelMap updated, last removed index: " << targetIndex << endl;
					for (unsigned i = 0; i < verifier->getBabelMap()->size(); ++i)
					{
						cout << std::get<0>(verifier->getBabelMap()->at(i)) << " ";
						cout << std::get<1>(verifier->getBabelMap()->at(i)) << " ";
						cout << std::get<2>(verifier->getBabelMap()->at(i)).count();
						cout << endl;
					}
				}
				if (targetBounded == false && boundExpired == true)
				{ /** Receive the second request */
					n = -1;
					int secondSockfd;
					try
					{
						secondSockfd = networkManager->acceptConnectionFrom(sockfd);
					}
					catch (phemap::PHEMAP_Verifier_Exception ex)
					{
						cerr << ex;
					}
					try
					{
						n = networkManager->receiveFromDevice(secondSockfd, buffer, verifier->getCRByteLength() + 1 + 32035);
					}
					catch (phemap::PHEMAP_Verifier_Exception ex)
					{
						cerr << ex;
					}

					BCTicketRequest *secondReq = new BCTicketRequest();
					BCTicketResponse *secondRes = new BCTicketResponse();
					secondReq->deserialize(buffer);

					/** Retrieve the ID of the origin device from the buffer. */
					deviceIDSize = n - secondReq->getLength();
					string secondDeviceID((char *)(buffer + secondReq->getLength()), deviceIDSize);

					DevicesMap_t::const_iterator secondItOrigin = verifier->getDevicesMap()->find(secondDeviceID);

					cout << endl
						 << *firstReq << endl
						 << *secondReq << endl;

					/** First of all, authenticate the requests
					 * 	firstReq->getLi()  returns 	   link Ai1
					 * 	firstRes->getLi()  will return link Ai2 (extracted from pufChain)
					 * 	secondReq->getLi() returns 	   link Bi1
					 *	secondRes->getLi() will return link Bi2 (extracted from pufChain)
					 */
					if (verifier->PHEMAP_verifierAuthentication(secondDeviceID, secondReq, secondRes))
					{
						/** Retrieve number of tickets to provide. */
						int carnetLength = min(firstReq->getRequestedTickets(), BC_MAX_TCK);
						carnetLength = min(secondReq->getRequestedTickets(), carnetLength);

						/** Check if it is dummy request */
						if (carnetLength > 1)
						{
							/** secondRes->getLi() returns Bi2. Copy the correct target ID within a temporary variable. */
							puf_t *secondTmpLink = (puf_t *)malloc(secondReq->getPufLength());
							xorVector(secondTmpLink, secondReq->getTargetID(), secondRes->getLi(), secondRes->getPufLength());

							string secondTargetID((const char *)secondTmpLink);

							DevicesMap_t::const_iterator secondItTarget = verifier->getDevicesMap()->find(secondTargetID);

							/** If both the targets of the two requests exist
					 		 * And, if the to requests correspond
							 */
							if (firstItTarget != verifier->getDevicesMap()->end() &&
								secondItTarget != verifier->getDevicesMap()->end() &&
								firstTargetID.compare(secondDeviceID) == 0 &&
								secondTargetID.compare(firstDeviceID) == 0)
							{ /** Then create the same response to send to both devices */

								/** Update link to Ai3/Bi3 for response */
								firstItOrigin->second->getNextPufLink(firstRes->getLi(), false);
								secondItOrigin->second->getNextPufLink(secondRes->getLi(), false);
								/** Now  firstRes->getLi() will return Ai3*/
								/** And secondRes->getLi() will return Bi3*/

								/** Copy Ai3 to a temporary variable for carnet generation */
								memcpy(firstTmpLink, firstRes->getLi(), firstRes->getPufLength());

								/** Generate a carnet with carnetLength tickets starting from (Ai4 xor Bi3) */
								puf_t *carnet = (puf_t *)malloc(carnetLength * firstReq->getPufLength());
								for (int i = 0; i < carnetLength; i++)
								{
									firstItTarget->second->getNextPufLink(&carnet[i * firstRes->getPufLength()], false); //Bi4
									firstItOrigin->second->getNextPufLink(firstTmpLink, false);							 //Ai4

									printHexBuffer(firstTmpLink, 16);
									cout << " XOR ";
									printHexBuffer(&carnet[i * firstRes->getPufLength()], 16);
									cout << endl;

									xorVector(&carnet[i * firstRes->getPufLength()], &carnet[i * firstRes->getPufLength()], firstTmpLink, firstRes->getPufLength());
								}
								firstRes->setTargetHost(firstItTarget->second->getDeviceProxy()->getDeviceHost());
								firstRes->setTargetPort(firstItTarget->second->getDeviceProxy()->getDevicePort());
								firstRes->setCarnet(carnet, carnetLength);
								secondRes->setTargetHost(secondItTarget->second->getDeviceProxy()->getDeviceHost());
								secondRes->setTargetPort(secondItTarget->second->getDeviceProxy()->getDevicePort());
								secondRes->setCarnet(carnet, carnetLength);
								free(carnet);
								verifier->BC_addToBabelMap(firstTargetID, firstDeviceID);
							}
							else
							{
								/** Send null ticket response */
								firstItOrigin->second->getNextPufLink(firstRes->getLi(), false);
								secondItOrigin->second->getNextPufLink(secondRes->getLi(), false);
								firstRes->setCarnet(NULL, 0);
								secondRes->setCarnet(NULL, 0);

								if (firstItTarget == verifier->getDevicesMap()->end() ||
									secondItTarget == verifier->getDevicesMap()->end())
								{
									std::cerr << "Device " << firstTargetID << " " << secondDeviceID << " not found" << std::endl;
								}
								else if (firstTargetID.compare(secondDeviceID) != 0 ||
										 secondTargetID.compare(firstDeviceID) != 0)
								{
									std::cerr << "The explicit babelchain requests don't correspond." << std::endl;
								}
								else if (targetBounded == true && boundExpired == false)
								{
									std::cerr << "Device " << firstTargetID << " is busy" << std::endl;
								}
							}
							free(secondTmpLink);
						}
						else
						{
							/** For null ticket request, send a null ticket response */
							firstItOrigin->second->getNextPufLink(firstRes->getLi(), false);
							secondItOrigin->second->getNextPufLink(secondRes->getLi(), false);
							firstRes->setCarnet(NULL, 0);
							secondRes->setCarnet(NULL, 0);
						}

						/** At first, send response to the second device */
						bzero(buffer, 32035);
						secondRes->serialize(buffer);

						try
						{
							networkManager->sendToDevice(secondSockfd, buffer, secondRes->getLength());
						}
						catch (phemap::PHEMAP_Verifier_Exception ex)
						{
							cerr << ex;
						}

						cout << "VerifierServer: I'm sending\t\tto the second device "
							 << secondDeviceID << endl
							 << *secondRes
							 << endl;
					}
					else
					{
						cerr << "BC Explicit PHEMAP: second request not authenticated" << endl;
					}
				}
				else
				{
					/** For null ticket request, send a null ticket response */
					firstItOrigin->second->getNextPufLink(firstRes->getLi(), false);
					firstRes->setCarnet(NULL, 0);
				}
				free(firstTmpLink);

				/** Then, send response to the first device */
				bzero(buffer, 32035);
				firstRes->serialize(buffer);

				try
				{
					networkManager->sendToDevice(newsockfd, buffer, firstRes->getLength());
				}
				catch (phemap::PHEMAP_Verifier_Exception ex)
				{
					cerr << ex;
				}

				cout << "VerifierServer: I'm sending\t\tto the first device "
					 << firstDeviceID << endl
					 << *firstRes
					 << endl;
			}
			else
			{
				cerr << "BC Explicit PHEMAP: first request not authenticated" << endl;
			}
		}
		break;

		case (phemap::BC_IM_TCK_REQ):
		{
			//	|req|deviceID|
			//	0			MSB

			BCTicketRequest *req = new BCTicketRequest();
			req->deserialize(buffer);

			/** Retrieve the ID of the origin device from the buffer. */
			int deviceIDSize = n - req->getLength();
			string deviceID((char *)(buffer + req->getLength()), deviceIDSize);

			BCTicketResponse *res = new BCTicketResponse();

			DevicesMap_t::const_iterator itOriginDevice = verifier->getDevicesMap()->find(deviceID);

			cout << endl
				 << *req << endl;

			/** First of all, authenticate the request
			 * 	req->getLi() returns 	 link Ai1
			 * 	res->getLi() will return link Ai2 (extracted from pufChain)
			 */
			if (verifier->PHEMAP_verifierAuthentication(deviceID, req, res))
			{

				/** Retrieve number of tickets to provide. */
				int carnetLength = min(req->getRequestedTickets(), BC_MAX_TCK);

				/** Check if it is dummy request */
				if (carnetLength > 1)
				{
					/** res->getLi() returns Ai2. Copy the correct target ID within a temporary variable. */
					puf_t *tmpLink = (puf_t *)malloc(req->getPufLength());
					xorVector(tmpLink, req->getTargetID(), res->getLi(), res->getPufLength());

					string targetID((const char *)tmpLink);

					DevicesMap_t::const_iterator itTargetDevice = verifier->getDevicesMap()->find(targetID);
					int targetIndex = verifier->BC_searchBound(targetID);
					bool targetBounded = false;
					bool boundExpired = true;

					if (targetIndex > -1)
					{
						targetBounded = true;
						boundExpired = verifier->BC_hasExpired(targetIndex);
					}

					/** If bound has expired, than reinit the target and delete the bound from babelMap*/
					if (itTargetDevice != verifier->getDevicesMap()->end() && targetBounded && boundExpired)
					{
						verifier->PHEMAP_startInitiation(targetID);
						verifier->getBabelMap()->erase(verifier->getBabelMap()->begin() + targetIndex);
						targetBounded = false;
						boundExpired = true;

						cout << "\n\nBabelMap updated, last removed index: " << targetIndex << endl;
						for (unsigned i = 0; i < verifier->getBabelMap()->size(); ++i)
						{
							cout << std::get<0>(verifier->getBabelMap()->at(i)) << " ";
							cout << std::get<1>(verifier->getBabelMap()->at(i)) << " ";
							cout << std::get<2>(verifier->getBabelMap()->at(i)).count();
							cout << endl;
						}
					}

					/** If targetID is known and target device is not busy on another BC bound */
					if (itTargetDevice != verifier->getDevicesMap()->end() && targetBounded == false && boundExpired == true)
					{

						/** Update link to Ai3 for response */
						itOriginDevice->second->getNextPufLink(res->getLi(), false);
						/** Now res->getLi() will return Ai3*/

						/** Copy Ai3 to a temporary variable for carnet generation */
						memcpy(tmpLink, res->getLi(), res->getPufLength());

						/** Store link Bi1 for change address message */
						puf_t *caLink = (puf_t *)malloc(sizeof(puf_t));

						/** Prepare change address message for target device */
						MessageChangeAddress *caReq = new MessageChangeAddress();
						MessageAuth *caRes = new MessageAuth();
						caReq->setAddress(itOriginDevice->second->getDeviceProxy()->getDeviceHost());
						caReq->setPort(itOriginDevice->second->getDeviceProxy()->getDevicePort());

						/** Put link Bi1 into the change address request */
						itTargetDevice->second->getNextPufLink(caReq->getLi(), false);

						/** Send authenticated change address message to the BC target device */
						if (itTargetDevice->second->getDeviceProxy()->PHEMAP_deviceAuthentication(caReq, caRes))
						{

							/** Calculate expected link Bi3 from B */
							itTargetDevice->second->getNextPufLink(caLink, false);

							/** Compare received link with the one expected
					 	 	 *  caLink contains calculated link from the pufChain
						 	 *  caRes contains received link from the target device
					 	 	 */
							if (0 == memcmp(caLink, caRes->getLi(), caRes->getPufLength()))
							{
								/** Generate a carnet with carnetLength tickets starting from (Ai4 xor Bi3) */
								puf_t *carnet = (puf_t *)malloc(carnetLength * res->getPufLength());
								for (int i = 0; i < carnetLength; i++)
								{
									itTargetDevice->second->getNextPufLink(&carnet[i * res->getPufLength()], false); //Bi3
									itOriginDevice->second->getNextPufLink(tmpLink, false);							 //Ai4

									printHexBuffer(tmpLink, 16);
									cout << " XOR ";
									printHexBuffer(&carnet[i * res->getPufLength()], 16);
									cout << endl;

									xorVector(&carnet[i * res->getPufLength()], &carnet[i * res->getPufLength()], tmpLink, res->getPufLength());
								}
								res->setTargetHost(itTargetDevice->second->getDeviceProxy()->getDeviceHost());
								res->setTargetPort(itTargetDevice->second->getDeviceProxy()->getDevicePort());
								res->setCarnet(carnet, carnetLength);
								free(carnet);

								/** Store the relationship between the two devices */
								verifier->BC_addToBabelMap(targetID, deviceID);
							}
							else
							{
								cerr << "BC PHEMAP: authentication of change address message failed" << endl;
							}

							free(caLink);
							free(tmpLink);
						}
						else
						{
							cerr << "BC PHEMAP: error sending change address message" << endl;
						}
					}
					else if (itTargetDevice == verifier->getDevicesMap()->end())
					{
						std::cerr << "Device " << targetID << " not found" << std::endl;

						/** Send null ticket response */
						itOriginDevice->second->getNextPufLink(res->getLi(), false);
						res->setCarnet(NULL, 0);
					}
					else if (targetBounded == true && boundExpired == false)
					{
						std::cerr << "Device " << targetID << " is busy" << std::endl;

						/** Send null ticket response */
						itOriginDevice->second->getNextPufLink(res->getLi(), false);
						res->setCarnet(NULL, 0);
					}
				}
				else
				{
					/** For null ticket request, send a null ticket response */
					itOriginDevice->second->getNextPufLink(res->getLi(), false);
					res->setCarnet(NULL, 0);
				}

				/** Actually buffer is bigger than 32035 bytes. We can assume carnetLength
				 *  to be at most 128 without reallocating buffer.
				 */
				bzero(buffer, 32035);
				res->serialize(buffer);

				try
				{
					//networkManager->sendToDevice(newsockfd, buffer, res->getLength());
					int reps, rest;
					reps = 32035 / 2048;
					rest = 32035 % 2048;
					n = 0;
					for (int i = 0; i < reps; i++)
					{
						n = n + networkManager->sendToDevice(newsockfd, buffer + n, 2048);
					}
					n = n + networkManager->sendToDevice(newsockfd, buffer + n, rest);
				}
				catch (phemap::PHEMAP_Verifier_Exception ex)
				{
					cerr << ex;
				}

				cout << "VerifierServer: I'm sending\t\tto "
					 << deviceID << endl
					 << *res
					 << endl;
			}
			else
			{
				cerr << "BC Implicit PHEMAP: Authentication from device failed" << endl;
			}
		}
		break;

		case (phemap::SALT_TCK_REQ):
		{
			SaltTicketRequest *req = new SaltTicketRequest();
			req->deserialize(buffer);
			string targetID = req->getTargetID();
			string originID = req->getOriginID();
			SaltTicketResponse *res = new SaltTicketResponse();

			DevicesMap_t::const_iterator itOriginDevice = verifier->getDevicesMap()->find(req->getTargetID());

			cout << endl
				 << *req << endl;

			/** Retrieve number of tickets to provide. */
			int carnetLength = min(req->getRequestedTickets(), SALT_MAX_TCK);

			/** Check if it is dummy request */
			if (carnetLength > 1)
			{
				DevicesMap_t::const_iterator itTargetDevice = verifier->getDevicesMap()->find(targetID);
				int targetIndex = verifier->BC_searchBound(targetID);
				bool targetBounded = false;
				bool boundExpired = true;

				//DISABILITATO PER I TEST @TODO
				// if (targetIndex > -1)
				// {
				// 	targetBounded = true;
				// 	boundExpired = verifier->BC_hasExpired(targetIndex);
				// }

				/** If bound has expired, then reinit the target and delete the bound from babelMap*/
				if (itTargetDevice != verifier->getDevicesMap()->end() && targetBounded && boundExpired)
				{
					verifier->PHEMAP_startInitiation(targetID);
					verifier->getBabelMap()->erase(verifier->getBabelMap()->begin() + targetIndex);
					targetBounded = false;
					boundExpired = true;

					cout << "\n\nBabelMap updated, last removed index: " << targetIndex << endl;
					for (unsigned i = 0; i < verifier->getBabelMap()->size(); ++i)
					{
						cout << std::get<0>(verifier->getBabelMap()->at(i)) << " ";
						cout << std::get<1>(verifier->getBabelMap()->at(i)) << " ";
						cout << std::get<2>(verifier->getBabelMap()->at(i)).count();
						cout << endl;
					}
				}

				/** If targetID is known and target device is not busy on another bound */
				if (itTargetDevice != verifier->getDevicesMap()->end() && targetBounded == false && boundExpired == true)
				{

					/** Store link Bi1 for change address message */
					puf_t *siLink = (puf_t *)malloc(sizeof(puf_t));

					/** Prepare change address message for target device */
					SaltInstall *siReq = new SaltInstall();
					MessageAuth *siRes = new MessageAuth();
					siReq->setAddress(req->getOriginHost());
					siReq->setPort(req->getOriginPort());

					/** Put link Bi1 into the salt install message */
					itTargetDevice->second->getNextPufLink(siReq->getLi(), false);

					/** Generate Salt and put it into the salt install message */
					puf_t *salt = (puf_t *)malloc(siReq->getPufLength());
					getRandomNonce(salt, siReq->getPufLength());
					itTargetDevice->second->getNextPufLink(siReq->getSalt(), false);
					xorVector(siReq->getSalt(), salt, siReq->getSalt(), siReq->getPufLength());
					siReq->setTicketCounter(carnetLength);

					/** Send authenticated salt install message to the target device */
					if (itTargetDevice->second->getDeviceProxy()->PHEMAP_deviceAuthentication(siReq, siRes))
					{
						/** Calculate expected link Bi3 from B */
						itTargetDevice->second->getNextPufLink(siLink, false);

						/** Compare received link with the one expected
					 	 	 *  siLink contains calculated link from the pufChain
						 	 *  siRes contains received link from the target device
					 	 	 */
						if (0 == memcmp(siLink, siRes->getLi(), siRes->getPufLength()))
						{
							/** Generate a carnet with carnetLength tickets starting from (Ai4 xor Bi3) */

							puf_t *carnet = (puf_t *)malloc(carnetLength * siRes->getPufLength());
							for (int i = 0; i < carnetLength; i++)
							{
								itTargetDevice->second->getNextPufLink(&carnet[i * siRes->getPufLength()], false); //Bi3
								cout << endl;
								printHexBuffer(salt, 16);
								cout << " XOR ";
								printHexBuffer(&carnet[i * res->getPufLength()], 16);

								xorVector(&carnet[i * res->getPufLength()], &carnet[i * res->getPufLength()], salt, res->getPufLength());
							}
							res->setTargetHost(itTargetDevice->second->getDeviceProxy()->getDeviceHost());
							res->setTargetPort(itTargetDevice->second->getDeviceProxy()->getDevicePort());
							res->setCarnet(carnet, carnetLength);
							free(carnet);

							/** Store the relationship between the two devices */
							verifier->BC_addToBabelMap(targetID, originID);
						}
						else
						{
							cerr << "SALT PHEMAP: authentication of salt install message failed" << endl;
						}
					}
					else
					{
						cerr << "SALT PHEMAP: error sending salt install message" << endl;
					}
					free(salt);
				}
				else if (itTargetDevice == verifier->getDevicesMap()->end())
				{
					std::cerr << "Device " << targetID << " not found" << std::endl;

					/** Send null ticket response */
					res->setCarnet(NULL, 0);
				}
				else if (targetBounded == true && boundExpired == false)
				{
					std::cerr << "Device " << targetID << " is busy" << std::endl;

					/** Send null ticket response */
					res->setCarnet(NULL, 0);
				}
			}
			else
			{
				/** For null ticket request, send a null ticket response */
				res->setCarnet(NULL, 0);
			}
			/** Actually buffer is bigger than 32035 bytes. We can assume carnetLength
			 *  to be at most 128 without reallocating buffer.
			 */
			bzero(buffer, 32035);
			res->serialize(buffer);

			try
			{
				networkManager->sendToDevice(newsockfd, buffer, res->getLength());
			}
			catch (phemap::PHEMAP_Verifier_Exception ex)
			{
				cerr << ex;
			}

			cout << "VerifierServer: I'm sending\t\tto "
				 << originID << endl
				 << *res
				 << endl;
		}
		break;

		default:
		{
			cerr << "\n[ERROR] Unexpected message type received on Verifier server.";
		}
		}
		free(buffer);

		networkManager->closeSocket(newsockfd);
	}
	networkManager->closeSocket(sockfd);
	delete networkManager;
}