//===- chainMaker.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file chainMaker.cpp
/// \author Mario Barbareschi
/// \brief This file defines the main function for generating chains
//===----------------------------------------------------------------------===//


#include "utils.h"
//#include <iostream>
#include <iomanip>
#include <fstream>
#include <unordered_map>
#include <array>
#include <vector>
#include <cstring>
#include <unistd.h>
#include "dummyPuf.h"

using namespace std;

typedef array<puf_t, 16> pufArray;

void initScreen(void);

/**
 * @struct 	PufHasher
 * @brief 	Simulates a PUF by mean of a hash function.
 */
struct PufHasher
{
	size_t operator()(const pufArray &a) const
	{
		size_t h = 0;

		for (auto e : a)
		{
			h ^= hash<puf_t>{}(e) + 0x9e3779b9 + (h << 6) + (h >> 2);
		}
		return h;
	}
};

/**
 * @brief 	Converts a given hexadecimal string in a vector of bytes.
 * @param	hex: string of hexadecimal digits.
 * @retval	bytes: vector of bytes (char).
 */
vector<char> HexToBytes(const string &hex)
{
	vector<char> bytes;

	for (unsigned int i = 0; i < hex.length(); i += 2)
	{
		string byteString = hex.substr(i, 2);
		char byte = (char)strtol(byteString.c_str(), NULL, 16);
		bytes.push_back(byte);
	}
	return bytes;
}

typedef unordered_map<pufArray, bool, PufHasher> CRPs_t;


/**
 * @brief	Main function simply calls chainMaker()
 */
int main(int argc, char *argv[])
{
	initScreen();

	puf_t seedPad[16] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF};
	puf_t seed[16];
	puf_t root[16];

	ofstream *chainFile;

	int i = 1;
	bool finiteNodes = false;
	bool rootPassed = false;
	int nodes = 1;
	string deviceID;
	string filename;

	int c = 0;
	while ((c = getopt(argc, argv, "d:r:n:h")) != -1)
	{
		switch (c)
		{
		case 'd':
		{
			deviceID = optarg;

			cout << "\nDeviceID: " << deviceID << endl;
			filename = deviceID + "Links.txt";
			chainFile = new ofstream(filename);
			if (!chainFile->is_open())
			{
				cerr << "Cannot open the file " << filename << endl;
			}
			else
			{
				cout << "Chain will be written onto " << filename << endl;
			}
		}
		break;
		case 'r':
		{
			vector<char> rootBytes = HexToBytes(string(optarg));
			memcpy(root, rootBytes.data(), 16);
			rootPassed = true;
		}
		break;
		case 'n':
		{
			nodes = atoi(optarg);
			finiteNodes = true;
			cout << nodes << " nodes will be created.\n";
		}
		break;
		case 'h':
		{
			//show help
			cout << "USAGE: chainMaker [-d <deviceID>] [-r <root>] [-n <nodes>]" << endl;
			return 0;
		}
		default:
		{
			cout << "USAGE: chainMaker [-d <deviceID>] [-r <root>] [-n <nodes>]" << endl;
			return -1;
		}
		}
	}

	if (!rootPassed)
	{
		getRandomNonce(root, 16);
	}

	int seed_index = 0;
	while (*(deviceID.c_str() + seed_index) != '\0')
	{
		seed[seed_index] = *(deviceID.c_str() + seed_index);
		seed_index++;
	}
	while (seed_index < 16)
	{
		seed[seed_index] = seedPad[seed_index];
		seed_index++;
	}

	phemap::DummyPuf puf = phemap::DummyPuf(&seed[0]);
	cout << "\nSeed: ";
	printHexBuffer(seed, 16);

	cout << "\nRoot: ";
	printHexBuffer(root, 16);
	cout << endl;

	pufArray *challenge, *response;
	challenge = new pufArray();
	response = new pufArray();
	memcpy(challenge->data(), root, 16);

	CRPs_t crp;
	long long numberOfCRP = 0;

	int m = 0;
	while (crp.end() == crp.find(*challenge) && m < nodes)
	{
		if (finiteNodes == true)
		{
			m++;
		}
		puf.generateResponse(challenge->data(), response->data());
		crp.insert(make_pair(*challenge, true));

		for (int k = 0; k < 16; k++)
		{
			cout << std::uppercase << std::hex << std::setw(2) << std::uppercase << std::uppercase << std::setfill('0') << static_cast<int>(*(challenge->data() + k));
			*chainFile << std::uppercase << std::hex << std::setw(2) << std::uppercase << std::setfill('0') << static_cast<int>(*(challenge->data() + k));
			//metti nel file
		}
		cout << endl;
		*chainFile << endl;

		memcpy(challenge->data(), response->data(), 16);
		numberOfCRP++;
		if (0 == numberOfCRP % 10000)
		{
			cout << "\rIterations: " << numberOfCRP;
			chainFile->flush();
		}
	}

	if (chainFile->is_open())
	{
		chainFile->close();
	}

	cout << endl;
	if (m == nodes)
	{
		cout << "Ended correctly ";
	}
	else
	{
		cout << "Found collision for ";
	}

	printHexBuffer(challenge->data(), 16);
	cout << " after " << std::dec << numberOfCRP << " links " << endl;

	return 0;
}

void initScreen(void)
{
	cout << "\n\n\n";
	cout << "	██████╗ ██╗  ██╗███████╗███╗   ███╗ █████╗ ██████╗ \n";
	cout << "	██╔══██╗██║  ██║██╔════╝████╗ ████║██╔══██╗██╔══██╗\n";
	cout << "	██████╔╝███████║█████╗  ██╔████╔██║███████║██████╔╝\n";
	cout << "	██╔═══╝ ██╔══██║██╔══╝  ██║╚██╔╝██║██╔══██║██╔═══╝ \n";
	cout << "	██║     ██║  ██║███████╗██║ ╚═╝ ██║██║  ██║██║     \n";
	cout << "	╚═╝     ╚═╝  ╚═╝╚══════╝╚═╝     ╚═╝╚═╝  ╚═╝╚═╝     \n";
	cout << "\n\n\n";
}
