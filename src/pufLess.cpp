//===- pufLess.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file pufLess.cpp
/// \author Erasmo La Montagna
/// \brief This file defines the main function for the pufLess software
//===----------------------------------------------------------------------===//

#include "utils.h"
#include <thread>
#include <vector>
#include <fstream>
#include <iostream>

#include "pufLessServer.h"

#define PUFL_DEFAULT_ID "fogNode"
#define VER_DEFAULT_HOST "localhost"
#define VER_DEFAULT_PORT 8765
#define PUFL_DEFAULT_HOST "localhost"
#define PUFL_DEFAULT_PORT 8766

void usage(void);
void initScreen(void);

using namespace std;

/** Funzione setup test review.
 * 	Invia una richiesta di salt install al verifier
 * 	con un carnet di lunghezza variabile e ne misura
 * 	i tempi di esecuzione.
 * 	Il valore r passato alla funzione serve solo per la notazione dei file di log.
 */
void setup_testr(phemap::PufLess *pufLess,string targetID,int numberOfTickets,string nodeHost, int nodePort, int r)
{
	cout << "\nSetup Test r per " << std::dec << targetID << " con T = " << numberOfTickets << endl;

	string path = "../log/review_setup_t1_r" + to_string(r) + "_" + targetID + ".txt";
	cout << endl << path << endl;

	/** Write current timestamp to the log file */
	chrono::nanoseconds start = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());

	/** Send Salted ticket request */
	int ticketsAvailable = pufLess->SALT_PHEMAP_ticketRequest(targetID, numberOfTickets, nodeHost, nodePort);
	cout << ticketsAvailable << " tickets obtained for " << targetID << endl;

	/** Write current timestamp to the log file */
	chrono::nanoseconds end = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());

	/** Write to file */
	ofstream logFile(path, std::ofstream::out | std::ofstream::app);
	logFile << end.count() - start.count() << endl;
	logFile.close();

}

void run_test_2(phemap::PufLess *pufLess, int tickets, int iterations, string nodeHost, int nodePort)
{
	cout << "\nRunning TEST2 for " << std::dec << tickets << " tickets and " << iterations << " iterations.\n";
	string targetID = "F7";
	int numberOfTickets = tickets;
	int msg_num = tickets / 2;
	int treq_num = (2 * iterations) / tickets;
	chrono::seconds now = chrono::duration_cast<chrono::seconds>(chrono::system_clock::now().time_since_epoch());
	string ts_dir = to_string(now.count());

	/** Write current timestamp to the log file */
	string plFile("../test_data/");
	plFile.append(ts_dir);
	plFile.append("_test2_payload.txt");
	ofstream plStream(plFile, std::ofstream::out | std::ofstream::app);

	for (int i = 0; i < treq_num; i++)
	{
		/** Write current timestamp to the log file */
		chrono::nanoseconds now = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());
		string startFile("../test_data/");
		startFile.append(ts_dir);
		startFile.append("_test2_setup_start.txt");
		ofstream stratStream(startFile, std::ofstream::out | std::ofstream::app);
		stratStream << now.count() << endl;
		stratStream.close();

		/** Send Salted ticket request */
		int ticketsAvailable = pufLess->SALT_PHEMAP_ticketRequest(targetID, numberOfTickets, nodeHost, nodePort);

		/** Write current timestamp to the log file */
		now = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());
		string endFile("../test_data/");
		endFile.append(ts_dir);
		endFile.append("_test2_setup_end.txt");
		ofstream endStream(endFile, std::ofstream::out | std::ofstream::app);
		endStream << now.count() << endl;
		endStream.close();

		cout << "\n\n Tickets obtained: " << ticketsAvailable << "\n\n";

		/** Find device */
		SaltMap_t::iterator counters = pufLess->getSaltMap()->find("F7");

		/*Loop on verification requests */
		for (int j = 0; j < msg_num; j++)
		{
			msleep(100);

			string body(BODY_BYTE_SIZE, 0);
			body = "message_" + to_string(j + 1 + i * msg_num);
			plStream << body << endl;

			int remainingTickets = counters->second;
			if (remainingTickets > 1)
			{
				/** Write current timestamp to the log file */
				now = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());
				string startFile("../test_data/");
				startFile.append(ts_dir);
				startFile.append("_test2_ver_start.txt");
				ofstream logFile(startFile, std::ofstream::out | std::ofstream::app);
				logFile << now.count() << endl;
				logFile.close();

				if (pufLess->SALT_PHEMAP_startVerification(targetID, body))
				{
					/** Write current timestamp to the log file */
					now = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());
					string startFile("../test_data/");
					startFile.append(ts_dir);
					startFile.append("_test2_ver_end.txt");
					ofstream logFile(startFile, std::ofstream::out | std::ofstream::app);
					logFile << now.count() << endl;
					logFile.close();

					cout << "\033[1;032m" << targetID << " authenticated\n\n\033[0m";
				}
				else
				{
					cout << "\033[1;031m" << targetID << " NOT authenticated\033[0m\n\n";
					continue;
				}
			}
			else
			{
				cout << "\n\nSalted target NOT initiated: send a Salt ticket request first.\n\n";
				//run_test_2(device, tickets, iterations -1 - msg_num);
			}
		}
	}
	plStream.close();
}

/**
 * @brief	Function passed to the thread which simulates the pufLess server.
 * @param	pufLessServer: pointer to the pufLessServer object.
 * @retval	None
 */
void functionForPuflessThread(phemap::PufLessServer *pufLessServer)
{
	pufLessServer->runPufLessServer();
}

/**
 * @brief	Pufless main task. Instantiates a new pufLess node. Starts a thread for the deviceServer.
 * 			Finally, waits for device authentication and then loops on verification requests.
 * @param	nodeID
 * @param	nodeHost: node host.
 * @param	nodePort: node port.
 * @param	verifierHost: verifier host.
 * @param	verifierPort: verifier port.
 * @retval	0
 */
int runPufLess(string nodeID, string nodeHost, int nodePort, string verifierHost, int verifierPort)
{
	phemap::PufLess *pufLess = new phemap::PufLess(nodeID);
	pufLess->getSaltVerifierProxy()->setVerifierHost(verifierHost)->setVerifierPort(verifierPort);

	phemap::PufLessServer *pufLessServer = new phemap::PufLessServer();
	pufLessServer->setPufLess(pufLess)->setPufLessPort(nodePort);

	cout << "\nRunning server" << endl;
	thread pufLessServerThread(functionForPuflessThread, pufLessServer);

	/** Loop on verification requests */
	bool loop = true;
	while (loop == true)
	{
		string body(BODY_BYTE_SIZE, 0);
		size_t pos = 0;
		vector<string> token;

		//cin.getline((char *)body.c_str(), BODY_BYTE_SIZE);
		getline(std::cin, body);

		/** Remove multiple white spaces */
		while (((pos = body.find("  ")) != string::npos))
		{
			body.erase(pos, 1);
		}

		/** Create vector of tokens */
		while (((pos = body.find(" ")) != string::npos))
		{
			token.push_back(body.substr(0, pos));
			body.erase(0, pos + 1);
		}
		token.push_back(body);

		/** Route request */
		if (token[0] == "exit")
		{
			cout << "Closing pufLess.\n\n";
			loop = false;
		}
		else if (token[0] == "sleep")
		{
			int ms = stoi(token[1]);
			cout << "Sleep for " << std::dec << ms << " milliseconds.\n\n";
			msleep(ms);
		}
		else if (token[0] == "test2")
		{
			run_test_2(pufLess, stoi(token[1]), stoi(token[2]), nodeHost, nodePort);
		}
		else if (token[0] == "setupr")
		{
			setup_testr(pufLess, token[1], stoi(token[2]), nodeHost, nodePort, stoi(token[3]));
		}
		else if (token[0] == "-sr")
		{
			cout << "\nsalted phemap ticket request\n";

			string targetID = token[1];
			int numberOfTickets = stoi(token[2]);

			int ticketsAvailable = pufLess->SALT_PHEMAP_ticketRequest(targetID, numberOfTickets, nodeHost, nodePort);

			cout << "\n\n Tickets obtained: " << ticketsAvailable << "\n\n";
		}
		else if (token[0] == "-sa")
		{
			cout << "\nsalted phemap authentication\n";
			if (token.size() > 2)
			{
				string targetID = token[1];
				body = token[2];

				if (pufLess->SALT_PHEMAP_startVerification(targetID, body))
					cout << targetID << " authenticated\n\n";
				else
					cout << targetID << " NOT authenticated. Send a new ticket request.\n\n";
			}
			else
			{
				cout << "\nToo few arguments, try again.\n";
			}
		}
	}
	//Finally, stop the server thread
	//pufLessServerThread.join();
	return 0;
}

/**
 * @brief	Main function simply calls runPufLess().
 */
int main(int argc, char *argv[])
{
	initScreen();

	std::string pufLessID = PUFL_DEFAULT_ID;
	int verifierPort = VER_DEFAULT_PORT;
	string verifierHost = VER_DEFAULT_HOST;
	int pufLessPort = PUFL_DEFAULT_PORT;
	string pufLessHost = PUFL_DEFAULT_HOST;

	int c = 0;

	while ((c = getopt(argc, argv, "d:l:p:v:f:h")) != -1)
	{
		switch (c)
		{
		case 'd':
			pufLessID = optarg;
			break;
		case 'l':
			pufLessHost = optarg;
			break;
		case 'p':
			pufLessPort = atoi(optarg);
			//value = strtol(optarg, NULL, 10);
			break;
		case 'v':
			verifierHost = optarg;
			break;
		case 'f':
			verifierPort = atoi(optarg);
			;
			break;
		case 'h':
			//HELP
			usage();
			return 0;
		default:
			cout << "\n\nInvalid option: \n\n";
			usage();
			return -1;
		}
	}

	cout << "\n\nRunning PufLess with ID: " << pufLessID;
	cout << "\nPufLess Host: " << pufLessHost;
	cout << "\nPufLess Port: " << pufLessPort;
	cout << "\nVerifier Host: " << verifierHost;
	cout << "\nVerifier Port: " << verifierPort;
	cout << "\n\n";

	return runPufLess(pufLessID, pufLessHost, pufLessPort, verifierHost, verifierPort);
}

/**
 * @brief	Prints usage instructions.
 */
void usage()
{
	//example: ./pufLessMain TODO continuare
	cout << "\n\n pufLessMain [OPTION] [VALUE]	\n";
	cout << "	-d	<VALUE>		pufless ID		\n";
	cout << "	-l	<VALUE>		pufless host	\n";
	cout << "	-p	<VALUE>		pufless port	\n";
	cout << "	-v	<VALUE>		verifier host	\n";
	cout << "	-f	<VALUE>		verifier port	\n";
	cout << endl;
}

void initScreen(void)
{
	cout << "\n\n\n";
	cout << "	██████╗ ██╗  ██╗███████╗███╗   ███╗ █████╗ ██████╗ \n";
	cout << "	██╔══██╗██║  ██║██╔════╝████╗ ████║██╔══██╗██╔══██╗\n";
	cout << "	██████╔╝███████║█████╗  ██╔████╔██║███████║██████╔╝\n";
	cout << "	██╔═══╝ ██╔══██║██╔══╝  ██║╚██╔╝██║██╔══██║██╔═══╝ \n";
	cout << "	██║     ██║  ██║███████╗██║ ╚═╝ ██║██║  ██║██║     \n";
	cout << "	╚═╝     ╚═╝  ╚═╝╚══════╝╚═╝     ╚═╝╚═╝  ╚═╝╚═╝     \n";
	cout << "\n\n\n";
}