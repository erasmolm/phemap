//===- verifier.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file verifier.cpp
/// \author Mario Barbareschi
/// \brief This file defines the main function for the verifier software
//===----------------------------------------------------------------------===//

#include <iostream>
#include <cstring>
#include "verifier.h"
#include "deviceManager.h"
#include "verifierServer.h"
#include <thread>
#include <fstream>

#include "deviceException.h"
#include "pufChainFromFile.h"

#include "utils.h"

#define VER_DEFAULT_HOST "localhost"
#define VER_DEFAULT_PORT 8765

using namespace std;

void usage(void);
void initScreen(void);

void run_test_1(phemap::Verifier *verifier, int iterations)
{
	/*Loop on verification requests */
	bool loop = true;
	int msg_num = 0;
	chrono::seconds now = chrono::duration_cast<chrono::seconds>(chrono::system_clock::now().time_since_epoch());
	string ts_dir = to_string(now.count());

	while (loop == true)
	{
		string body(BODY_BYTE_SIZE, 0);
		body = "message_" + to_string(msg_num);

		if (msg_num > iterations - 1)
		{
			cout << "Closing device.\n\n";
			loop = false;
		}
		else
		{
			msg_num++;
			/** Find device */
			DevicesMap_t::const_iterator it = verifier->getDevicesMap()->find("F7");

			/** Route request */
			string targetDeviceID = it->first;

			/** Write current timestamp to the log file */
			chrono::nanoseconds now = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());
			string startFile("../test_data/");
			startFile.append(ts_dir);
			startFile.append("_test1_ver_start.txt");
			ofstream logFile(startFile, std::ofstream::out | std::ofstream::app);
			logFile << now.count() << endl;
			logFile.close();

			/** Start verification */
			if (verifier->PHEMAP_startVerification(targetDeviceID, body))
			{
				cout << "\033[1;032m" << targetDeviceID << " authenticated\n\n\033[0m";

				/** Write current timestamp to the log file */
				chrono::nanoseconds now = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());
				string endFile("../test_data/");
				endFile.append(ts_dir);
				endFile.append("_test1_ver_end.txt");
				ofstream logFile(endFile, std::ofstream::out | std::ofstream::app);
				logFile << now.count() << endl;
				logFile.close();
			}
			else
			{
				cout << "\033[1;031m" << targetDeviceID << " NOT authenticated\033[0m\n\n";
			}
			msleep(250);
		}
	}
}

void run_test_1b(phemap::Verifier *verifier, int iterations)
{
	for (int i = 0; i < iterations; i++)
	{
		/** Write current timestamp to the log file */
		chrono::nanoseconds start = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());

		if (true == verifier->PHEMAP_startInitiation("F7"))
		{
			/** Write current timestamp to the log file */
			chrono::nanoseconds end = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());
			ofstream logFile("../test_data/test1_init.txt", std::ofstream::out | std::ofstream::app);
			logFile << end.count()-start.count() << endl;
			logFile.close();
		}

		msleep(50);
	}
}

/**
 * @brief	Verifier server thread implementation.
 * 			This procedure implements the actions of the verifier to be executed into a
 * 			separated thread.
 * @param	verifierServer: The object of type VerifierServer initialized into 
 * 			the main thread.
 * @retval 	None
 */
void functionForVerifierThread(phemap::VerifierServer *verifierServer)
{
	verifierServer->runVerifierServer();
}

/**
 * @brief	Verifier main task. This function creates a Verifier instance and a Verifier server instance. 
 * 			Then it starts a separated thread to make the verifierServer listen to devices verifications. 
 * 			Then, for testing purposes, it creates a new pair for devicesMap (called "Device A")
 * 			and initiate PHEMAP protocol on this device.
 * 			Finally, it asks the user (within a while(1) loop) to insert new values in order to start
 * 			a new verification phase of PHEMAP protocol. 
 * @param	id: device ID.
 * @retval	0
 */
int runVerifier(string verifierHost, int verifierPort)
{
	/** creates a new verifier */
	phemap::Verifier *verifier = new phemap::Verifier(DEFAULT_PUF_LENGTH);

	/** creates a new verifier server and starts its thread */
	phemap::VerifierServer *verifierServer = new phemap::VerifierServer(verifier);
	verifierServer->setVerifierHost(verifierHost)->setVerifierPort(verifierPort);

	thread verifierServerThread(functionForVerifierThread, verifierServer);

	/** Load device file */
	string filename;
	ifstream deviceFile("../resources/deviceList.txt");
	string deviceID;

	if (deviceFile.is_open())
	{
		int devicePort = -1;
		string deviceHost = "localhost";
		while (deviceFile >> deviceID)
		{
			deviceFile >> deviceHost;
			deviceFile >> devicePort;

			filename = "../resources/" + deviceID + "Links.txt";

			pair<string, phemap::DeviceManager *> pair(deviceID, (new phemap::DeviceManager())->setDeviceProxy(new phemap::DeviceProxy(deviceID, deviceHost, devicePort, new phemap::PufChainFromFile(filename))));
			verifier->getDevicesMap()->insert(pair);
		}
	}
	deviceFile.close();

	for (auto k = verifier->getDevicesMap()->begin(); k != verifier->getDevicesMap()->end(); k++)
	{
		cout << "\nDevice ID: " << k->first;
	}

	/** Loop on verification requests */
	bool loop = true;
	while (loop == true)
	{
		string body(BODY_BYTE_SIZE, 0);
		size_t pos = 0;
		vector<string> token;

		//cin.getline((char *)body.c_str(), BODY_BYTE_SIZE);
		getline(std::cin, body);

		/** Remove multiple white spaces */
		while (((pos = body.find("  ")) != string::npos))
		{
			body.erase(pos, 1);
		}

		/** Create vector of tokens */
		while (((pos = body.find(" ")) != string::npos))
		{
			token.push_back(body.substr(0, pos));
			body.erase(0, pos + 1);
		}
		token.push_back(body);

		if (token[0] == "exit")
		{
			cout << "Closing verifier.\n\n";
			loop = false;
		}
		else if (token[0] == "sleep")
		{
			int ms = stoi(token[1]);
			cout << "Sleep for " << std::dec << ms << " milliseconds.\n\n";
			msleep(ms);
		}
		else if (token[0] == "init")
		{
			string targetID = token[1];
			cout << "Starting initialization for " << targetID << "\n\n";
			verifier->PHEMAP_startInitiation(targetID);
		}
		else if (token[0] == "test1")
		{
			run_test_1(verifier, stoi(token[1]));
		}
		else if (token[0] == "test1b")
		{
			run_test_1b(verifier, stoi(token[1]));
		}
		else
		{
			/** Find device */
			DevicesMap_t::const_iterator it = verifier->getDevicesMap()->find(token[0]);

			if (it != verifier->getDevicesMap()->end())
			{
				/** Route request */
				string targetDeviceID = it->first;

				/** Start verification */
				if (verifier->PHEMAP_startVerification(targetDeviceID, body))
				{
					cout << "\033[1;032m" << targetDeviceID << " authenticated\033[0m\n\n";
				}
				else
				{
					cout << "\033[1;031m" << targetDeviceID << " NOT authenticated\033[0m\n\n";
				}
			}
			else
			{
				cout << "Device " << token[0] << " does not exist.\n\n";
			}
		}
	}

	//Finally, stop the server thread
	//verifierServerThread.join();
	return 0;
}

/**
 * @brief	Main function simply calls runVerifier()
 */
int main(int argc, char *argv[])
{
	initScreen();
	int verifierPort = VER_DEFAULT_PORT;
	string verifierHost = VER_DEFAULT_HOST;

	int c = 0;

	while ((c = getopt(argc, argv, "v:f:h")) != -1)
	{
		switch (c)
		{
		case 'v':
			verifierHost = optarg;
			break;
		case 'f':
			verifierPort = atoi(optarg);
			break;
		case 'h':
			//show help
			usage();
			return 0;
		default:
			cout << "\n\nInvalid option: \n\n";
			usage();
			return -1;
		}
	}

	cout << "\n\nRunning Verifier: ";
	cout << "\nVerifier Host: " << verifierHost;
	cout << "\nVerifier Port: " << verifierPort;
	cout << "\n\n";

	return runVerifier(verifierHost, verifierPort);
}

/**
 * @brief	Prints usage instructions.
 */
void usage()
{
	//example: ./verifierMain -v localhost -f 8765
	cout << "\n\n verifierMain [OPTION] [VALUE]	\n";
	cout << "	-v	<VALUE>		verifier host	\n";
	cout << "	-f	<VALUE>		verifier port	\n";
	cout << endl;
}

void initScreen(void)
{
	cout << "\n\n\n";
	cout << "	██████╗ ██╗  ██╗███████╗███╗   ███╗ █████╗ ██████╗ \n";
	cout << "	██╔══██╗██║  ██║██╔════╝████╗ ████║██╔══██╗██╔══██╗\n";
	cout << "	██████╔╝███████║█████╗  ██╔████╔██║███████║██████╔╝\n";
	cout << "	██╔═══╝ ██╔══██║██╔══╝  ██║╚██╔╝██║██╔══██║██╔═══╝ \n";
	cout << "	██║     ██║  ██║███████╗██║ ╚═╝ ██║██║  ██║██║     \n";
	cout << "	╚═╝     ╚═╝  ╚═╝╚══════╝╚═╝     ╚═╝╚═╝  ╚═╝╚═╝     \n";
	cout << "\n\n\n";
}
