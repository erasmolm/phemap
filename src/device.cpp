//===- device.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file device.cpp
/// \author Mario Barbareschi
/// \brief This file defines the main function for the device software
//===----------------------------------------------------------------------===//

#include "utils.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <cstring>
#include <thread>
#include <cstdlib>
#include <unistd.h>
#include "dummyPuf.h"
#include "device.h"
#include "messageSkeleton.h"
#include "deviceServer.h"
#include "deviceManager.h"
#include "pufChainFromCarnet.h"
#include "saltTicketRequest.h"

using namespace std;

#define VER_DEFAULT_HOST "localhost"
#define VER_DEFAULT_PORT 8765
#define DEV_DEFAULT_PORT 9876
#define DEV_DEFAULT_ID "device_1"

/** SOLO PER I TEST*/
#define PUFL_DEFAULT_HOST "192.168.1.103"
#define PUFL_DEFAULT_PORT 8766

void usage(void);
void initScreen(void);

/**
 * Funzione di test 1 per la review.
 * 
 * Dati T tickets, ad ogni iterazione il device estrae
 * un numero random. Se questo raggiunge la soglia r allora
 * il device invia un messaggio al gateway.
 */
void run_testr(phemap::Device *device, int r)
{
	string thisDevice = device->getDeviceID();

	int iterations = 50;
	int attempt = 0;

	cout << "\nTest r per " << std::dec << thisDevice << endl;
	cout << "\nIterazione r = " << r << endl;

	string path = "../log/review_t1_r" + to_string(r) + "_" + thisDevice + ".txt";
	cout << endl
		 << path << endl;

	for (int i = 0; i < iterations; i++)
	{
		msleep(300);

		attempt = rand() % 100 + 1;
		cout << "\nattempt: " << std::dec << attempt << endl;
		if (attempt > r)
			continue;

		/** Write current timestamp to the log file */
		chrono::nanoseconds start = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());

		if (true == device->SALT_PHEMAP_startVerification("r" + to_string(r) + "_msg" + to_string(i + 1)))
		{
			/** Write current timestamp to the log file */
			chrono::nanoseconds end = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());

			/** Write to file */
			ofstream logFile(path, std::ofstream::out | std::ofstream::app);
			logFile << end.count() - start.count() << endl;
			logFile.close();
			cout << "Successfully authenticated\n\n";
		}
		else
		{
			cout << "NOT authenticated\n\n";
			// device->PHEMAP_sendProbe();
		}
	}
}

/**
 * Funzione di test 2 per la review.
 * 
 * Esegue un numero fissato di iterazioni con una lunghezza
 * del carnet T.
 * Ad ogni iterazione il device estrae un numero random.
 * Se questo raggiunge la soglia r allora
 * il device invia un messaggio al gateway.
 * Il device deve anche tener conto dei ticket a disposizione,
 * se dovessero finire è necessario invocare una nuova setup.
 */
void run_testr2(phemap::Device *device, int numberOfTickets, int r)
{
	int iterations = 50;
	int attempt = 0;

	string thisDevice = device->getDeviceID();
	string path = "../log/testr2_r" + to_string(r) + "_" + thisDevice + ".txt";
	cout << endl
		 << path << endl;

	for (int i = 0; i < iterations; i++)
	{

		attempt = rand() % 100 + 1;
		cout << "\nattempt: " << std::dec << attempt << endl;
		if (attempt > r) continue;

		/** attendi la ricezione del carnet*/
		if (device->getSaltCounter() == 0)
		{
			phemap::SaltTicketRequest *req = new phemap::SaltTicketRequest();
			req->setTargetID(thisDevice)->setRequestedTickets(numberOfTickets)->setOriginHost(PUFL_DEFAULT_HOST)->setOriginPort(PUFL_DEFAULT_PORT);

			device->SALT_PHEMAP_ticketRequestFromDevice(req);

			msleep(1000);
		}
		while (device->getSaltCounter() == 0)
		{
			cout << "\nwait";
			msleep(1000);
		}

		/** Write current timestamp to the log file */
		chrono::nanoseconds start = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());

		if (true == device->SALT_PHEMAP_startVerification(thisDevice + " r" + to_string(r) + "_msg" + to_string(i + 1)))
		{
			/** Write current timestamp to the log file */
			chrono::nanoseconds end = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());

			/** Write to file */
			ofstream logFile(path, std::ofstream::out | std::ofstream::app);
			logFile << end.count() - start.count() << endl;
			logFile.close();
			cout << "Successfully authenticated\n\n";
		}
		else
		{
			cout << "NOT authenticated\n\n";
		}

		msleep(5000);
	}
}

void run_test_2(phemap::Device *device, int tickets, int iterations)
{
	cout << "\nRunning TEST2 for " << std::dec << tickets << " tickets and " << iterations << " iterations.\n";
	string targetID = "F7";
	int numberOfTickets = tickets;
	int msg_num = tickets / 2;
	int treq_num = (2 * iterations) / tickets;

	for (int i = 0; i < treq_num; i++)
	{
		/** Write current timestamp to the log file */
		chrono::nanoseconds now = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());
		ofstream logFile("../resources/log.txt", std::ofstream::out | std::ofstream::app);
		logFile << "[BC_SETUP_START] " << now.count() << endl;
		logFile.close();

		/** Send BC ticket request */
		int ticketsAvailable = device->BC_PHEMAP_ticketRequest(phemap::BC_IM_TCK_REQ, targetID, numberOfTickets);

		/** Write current timestamp to the log file */
		now = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());
		logFile.open("../resources/log.txt", std::ofstream::out | std::ofstream::app);
		logFile << "[BC_SETUP_END] " << now.count() << endl;
		logFile.close();

		cout << "\n\n Tickets obtained: " << ticketsAvailable << "\n\n";

		/*Loop on verification requests */
		for (int j = 0; j < msg_num; j++)
		{
			msleep(100);

			string body(BODY_BYTE_SIZE, 0);
			body = "message_" + to_string(j + 1 + i * msg_num);

			int remainingTickets = device->getBCTickets();
			if (remainingTickets > 1)
			{
				/** Write current timestamp to the log file */
				now = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());
				logFile.open("../resources/log.txt", std::ofstream::out | std::ofstream::app);
				logFile << "[BC_VER_START] " << now.count() << endl;
				logFile.close();

				if (device->BC_PHEMAP_startVerification(body))
				{
					/** Write current timestamp to the log file */
					now = chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch());
					logFile.open("../resources/log.txt", std::ofstream::out | std::ofstream::app);
					logFile << "[BC_VER_END] " << now.count() << endl;
					logFile.close();

					cout << "Successfully authenticated\n";
				}
				else
				{
					cout << "Device NOT authenticated\n";
					continue;
				}
			}
			else
			{
				cout << "\n\nBabelchain target NOT initiated: send a BC ticket request first.\n\n";
				//run_test_2(device, tickets, iterations -1 - msg_num);
			}
		}
	}
}

/**
 * @brief	Function passed to the thread which simulates the device server.
 * @param	deviceServer: pointer to the deviceServer object.
 * @retval	None
 */
void functionForDeviceThread(phemap::DeviceServer *deviceServer)
{
	deviceServer->runDeviceServer();
}

/**
 * @brief	Device main task. Instantiates a new device with a given DummyPuf object
 * 			and a given 128-bit puf seed. Starts a thread for the deviceServer.
 * 			Finally, waits for device authentication and then loops on verification requests.
 * @param	id: device ID
 * @param	devicePort: device port.
 * @param	verifierHost: verifier host.
 * @param	verifierPort: verifier port.
 * @retval	0
 */
int runDevice(string deviceID, int devicePort, string verifierHost, int verifierPort)
{
	puf_t seedPad[16] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF};
	puf_t seed[16];

	int i = 0;
	while (*(deviceID.c_str() + i) != '\0')
	{
		seed[i] = *(deviceID.c_str() + i);
		i++;
	}
	while (i < 16)
	{
		seed[i] = seedPad[i];
		i++;
	}

	phemap::Puf *puf = new phemap::DummyPuf(&seed[0]);

	phemap::Device *device = new phemap::Device(deviceID, puf);
	device->getVerifierProxy()->setVerifierHost(verifierHost)->setVerifierPort(verifierPort);

	phemap::DeviceServer *deviceServer = new phemap::DeviceServer(device);
	deviceServer->setDevicePort(devicePort);

	cout << "\nRunning server" << endl;
	thread deviceServerThread(functionForDeviceThread, deviceServer);

	cout << "\nSending probe to verifier...";
	device->PHEMAP_sendProbe();

	/*Wait for device authentication */
	while (phemap::AUTHENTICATED != device->getState())
		continue;

	/*Loop on verification requests */
	bool loop = true;
	while (loop == true)
	{
		string body(BODY_BYTE_SIZE, 0);
		size_t pos = 0;
		vector<string> token;

		//cin.getline((char *)body.c_str(), BODY_BYTE_SIZE);
		getline(std::cin, body);

		/** Remove multiple white spaces */
		while (((pos = body.find("  ")) != string::npos))
		{
			body.erase(pos, 1);
		}

		/** Create vector of tokens */
		while (((pos = body.find(" ")) != string::npos))
		{
			token.push_back(body.substr(0, pos));
			body.erase(0, pos + 1);
		}
		token.push_back(body);

		/** Route request */

		if (token[0] == "exit")
		{
			cout << "Closing device.\n\n";
			loop = false;
		}
		else if (token[0] == "sleep")
		{
			int ms = stoi(token[1]);
			cout << "Sleep for " << std::dec << ms << " milliseconds.\n\n";
			msleep(ms);
		}
		else if (token[0] == "test2")
		{
			run_test_2(device, stoi(token[1]), stoi(token[2]));
		}
		else if (token[0] == "testr")
		{
			run_testr(device, stoi(token[1]));
		}
		else if (token[0] == "testr2")
		{
			run_testr2(device, stoi(token[1]), stoi(token[2]));
			loop = false;
		}
		else if (token[0] == "-bci")
		{
			cout << "implicit bc phemap ticket request";
			string targetID = token[1];
			int numberOfTickets = stoi(token[2]);

			/** Send BC ticket request */
			int ticketsAvailable = device->BC_PHEMAP_ticketRequest(phemap::BC_IM_TCK_REQ, targetID, numberOfTickets);

			cout << "\n\n Tickets obtained: " << ticketsAvailable << "\n\n";
		}
		else if (token[0] == "-bce")
		{
			cout << "explicit bc phemap ticket request";
			string targetID = token[1];
			int numberOfTickets = stoi(token[2]);

			/** Send BC ticket request */
			int ticketsAvailable = device->BC_PHEMAP_ticketRequest(phemap::BC_EX_TCK_REQ, targetID, numberOfTickets);

			cout << "\n\n Tickets obtained: " << ticketsAvailable << "\n\n";
		}
		else if (token[0] == "-bca")
		{
			cout << "bc phemap authentication";
			int remainingTickets = device->getBCTickets();
			if (remainingTickets > 1)
			{
				if (device->BC_PHEMAP_startVerification(body))
				{
					cout << "Successfully authenticated\n\n";
					if (remainingTickets - 2 == 0)
					{
						delete device->getBCTargetManager();
						cout << "\n\nBabelchain tickets finished.\n\n";
					}
				}
				else
				{
					cout << "Device NOT authenticated\n\n";
					cout << "\n I'll delete the babelchain tickets. Try again to contact the verifier.\n\n";
					for (int i = 0; i < device->getBCTickets(); i++)
					{
						device->getNextPufLink(0, false);
					}
					device->setBCTickets(0);
					delete device->getBCTargetManager();
				}
			}
			else
			{
				cout << "\n\nBabelchain target NOT initiated: send a BC ticket request first.\n\n";
			}
		}
		else if (token[0] == "-sr")
		{
			cout << "\nsalted phemap ticket request\n";

			int numberOfTickets = stoi(token[1]);

			phemap::SaltTicketRequest *req = new phemap::SaltTicketRequest();
			req->setTargetID(deviceID)->setRequestedTickets(numberOfTickets)->setOriginHost(PUFL_DEFAULT_HOST)->setOriginPort(PUFL_DEFAULT_PORT);

			device->SALT_PHEMAP_ticketRequestFromDevice(req);
		}
		else
		{

			if (device->authenticationMode == phemap::AMODE_SALTED)
			{
				cout << "\nSalted phemap verification.\n";
				if (true == device->SALT_PHEMAP_startVerification(body))
				{
					cout << "PufLess node successfully authenticated\n\n";
				}
				else
				{
					cout << "PufLess node NOT authenticated\n\n";
					cout << "\n Please kill me. I don't want to live on this planet anymore.\n\n";
					device->PHEMAP_sendProbe();
				}
			}
			else
			{
				cout << "\nClassic phemap verification.\n";
				if (true == device->PHEMAP_startVerification(body))
				{
					cout << "Successfully authenticated\n\n";
				}
				else
				{
					cout << "Verifier NOT authenticated\n\n";
					cout << "\n Please kill me. I don't want to live on this planet anymore.\n\n";
					device->PHEMAP_sendProbe();
				}
			}
		}
	}

	//deviceServerThread.join();
	return 0;
}

/**
 * @brief	Main function simply calls runDevice()
 */
int main(int argc, char *argv[])
{
	initScreen();

	string deviceID = DEV_DEFAULT_ID;
	int devicePort = DEV_DEFAULT_PORT;
	int verifierPort = VER_DEFAULT_PORT;
	string verifierHost = VER_DEFAULT_HOST;
	int c = 0;

	while ((c = getopt(argc, argv, "d:p:v:f:h:t")) != -1)
	{
		switch (c)
		{
		case 'd':
			deviceID = optarg;
			break;
		case 'p':
			devicePort = atoi(optarg);
			break;
		case 'v':
			verifierHost = optarg;
			break;
		case 'f':
			verifierPort = atoi(optarg);
			break;
		case 'h':
			//HELP
			usage();
			return 0;
		default:
			cout << "\n\nInvalid option: \n\n";
			usage();
			return -1;
		}
	}

	cout << "\n\nRunning Device with ID: " << deviceID;
	cout << "\nDevice Port: " << devicePort;
	cout << "\nVerifier Host: " << verifierHost;
	cout << "\nVerifier Port: " << verifierPort;
	cout << "\n\n";

	return runDevice(deviceID, devicePort, verifierHost, verifierPort);
}

/**
 * @brief	Prints usage instructions.
 */
void usage()
{
	//example: ./deviceMain -d deviceB -p 9878 -v localhost -f 8765
	cout << "\n\n deviceMain [OPTION] [VALUE]	\n";
	cout << "	-d	<VALUE>		device ID		\n";
	cout << "	-p	<VALUE>		device port		\n";
	cout << "	-v	<VALUE>		verifier host	\n";
	cout << "	-f	<VALUE>		verifier port	\n";
	cout << endl;
}

void initScreen(void)
{
	cout << "\n\n\n";
	cout << "	██████╗ ██╗  ██╗███████╗███╗   ███╗ █████╗ ██████╗ \n";
	cout << "	██╔══██╗██║  ██║██╔════╝████╗ ████║██╔══██╗██╔══██╗\n";
	cout << "	██████╔╝███████║█████╗  ██╔████╔██║███████║██████╔╝\n";
	cout << "	██╔═══╝ ██╔══██║██╔══╝  ██║╚██╔╝██║██╔══██║██╔═══╝ \n";
	cout << "	██║     ██║  ██║███████╗██║ ╚═╝ ██║██║  ██║██║     \n";
	cout << "	╚═╝     ╚═╝  ╚═╝╚══════╝╚═╝     ╚═╝╚═╝  ╚═╝╚═╝     \n";
	cout << "\n\n\n";
}