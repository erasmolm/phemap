//===- pufChainFromCarnet.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file pufChain.h
/// \author Erasmo La Montagna
/// \brief This file defines the implementation of the PufChainFromCarnet class
//===----------------------------------------------------------------------===//

#include "puf.h"
#include "pufChainFromCarnet.h"
#include "utils.h"
#include <iomanip>
#include <cstring>
#include <iostream>
#include <algorithm>

using namespace std;

/**
 * @brief	Constructor for PufChainFromCarnet.
 * @param 	carnet: pointer to the value to write to the carnet attribute.
 * @param 	carnetLength
 * @param	pufLength
 * @retval 	None
*/
phemap::PufChainFromCarnet::PufChainFromCarnet(puf_t *carnet, int carnetLength, int pufLength) : phemap::PufChain()
{
	this->setCRByteLength(pufLength);
	this->carnet = (puf_t *)malloc(carnetLength * pufLength);
	this->carnetLength = carnetLength;
	memcpy(this->carnet, carnet,carnetLength*pufLength);
}

/**
 * @brief	Increase the rowCounter and put next link in the storage.
 * @param 	nextlink: pointer to the Puf link that will be written.
 * @retval 	true if rowCounter is less than carnetLength, false otherwise.
*/
bool phemap::PufChainFromCarnet::getNextLink(puf_t *nextLink)
{
	if (rowCounter < carnetLength)
	{
		memcpy(nextLink, this->carnet + rowCounter*this->CRByteLength, this->CRByteLength);
		rowCounter++;
		return true;
	}
	else
	{
		return false;
	}
}

/**
 * @brief	Given a Puf link, looks for the successive one at a given distance. It lets the counter unmodified.
 * @param 	nextlink: pointer to the Puf link that will be written.
 * @param	disance: distance of the link from the actual position in the chain file.
 * @retval 	true if distance is less than remaining tickets, false otherwise.
*/
bool phemap::PufChainFromCarnet::peekLink(puf_t *nextLink, int distance)
{
	if (distance < (carnetLength - rowCounter))
	{
		memcpy(nextLink, this->carnet + (rowCounter + distance-1)*CRByteLength, CRByteLength);
		return true;
	}
	else
	{
		return false;
	}
	
}

/**
 * @brief	Destructor for PufChainFromCarnet.
 * @param 	None
 * @retval 	None
*/
phemap::PufChainFromCarnet::~PufChainFromCarnet()
{
	free(carnet);
}