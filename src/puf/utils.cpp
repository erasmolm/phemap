//===- utils.cpp ----------------------------------------------------*- C -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file utils.cpp
/// \author Mario Barbareschi
/// \brief This file implements some utilities
//===----------------------------------------------------------------------===//

#include "utils.h"

/**
 * @brief 	Bitwise XOR between two arbitrarily long arrays.
 * 
 * @param 	o: pointer to the output array.
 * @param 	x: pointer to the first input array.
 * @param 	y: pointer to the second input array.
 * @param	length: length of the arrays.
 * @retval 	None
 */
unsigned char *xorVector(unsigned char *o, const unsigned char *x, const unsigned char *y, int length)
{
	for (int i = 0; i < length; i++)
		o[i] = x[i] ^ y[i];
	return o;
}

/**
 * @brief 	Generate a random nonce based on time of the day.
 * @param 	nonce: pointer to the nounce string.
 * @param	length: length of the nounce string.
 * @retval 	None
 */
void getRandomNonce(unsigned char *nonce, int length)
{
	chrono::milliseconds now = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch());
	srand(now.count());
	for (int i = 0; i < length; i++)
	{
		nonce[i] = rand();
	}
}

/**
 * @brief 	Prints the content of buffer in hex format.
 * @param 	r: pointer to the array of characters.
 * @param	length: size of the array.
 * @retval 	None
 */
void printHexBuffer(const unsigned char *r, int length)
{
	int i;
	for (i = 0; i < length; i++)
		printf("\x1b[31m" "%02X" "\x1b[0m", r[i]);
}

/**
 * @brief	Simulate an active wait of the current executing task.
 * 			This function is used to make the current executing process/thread wait for
 * 			the time in millis expressed into the input variable millisec.
 * @param millisec Time to wait in millis.
 * @retval	1 in any case.
 */
int msleep(unsigned long milisec)
{
	struct timespec req = {0};
	time_t sec = (int)(milisec / 1000);
	milisec = milisec - (sec * 1000);
	req.tv_sec = sec;
	req.tv_nsec = milisec * 1000000L;
	while (nanosleep(&req, &req) == -1)
		continue;
	return 1;
}

/**
 * @brief	Print debug utility.
 * @param 	s: string to print.
 * @retval	None
 */
void printDEBUG(string s)
{
	cout << "\n\n[DEBUG] " << s;
}
