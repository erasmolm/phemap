//===- pufChainFromFile.cpp ----------------------------------------------------*- C++ -*-===//
//
//  Copyright (C) 2017  Mario Barbareschi (mario.barbareschi@unina.it)
//
//  This file is part of PHEMAP.
//
//  PHEMAP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  PHEMAP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with PHEMAP. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
//
//===----------------------------------------------------------------------===//
/// \file pufChainFromFile.cpp
/// \author Mario Barbareschi
/// \brief This file defines the implementation of the PufChainFromFile class
//===----------------------------------------------------------------------===//

#include "pufChainFromFile.h"
#include <assert.h>
#include <stdlib.h> /* strtol */
#include <limits>   /* numeric_limits*/

using namespace std;

 /**
 * @brief	Constructor for PufChainFromFile.
 * @param 	filename[in] name of the file to retrieve the chain
 * @retval 	None
*/
phemap::PufChainFromFile::PufChainFromFile(string filename)
{
	assert(filename.length() > 0 && "Invalid string");
	this->filename = filename;
	pufChainFile = new ifstream(filename);
	if (pufChainFile->is_open())
	{
		getline(*pufChainFile, nextLink);
		assert(nextLink.length() % 2 == 0 && "Invalid string for PUF response");
		setCRByteLength(nextLink.length() / 2);
		pufChainFile->clear();
		pufChainFile->seekg(0, ios::beg);
		rowCounter = 0;
	}
	else
	{
		cerr << "Unable to open file: " << filename << endl;
	}
}

/**
  * @brief	Returns the next link.
  * @param  nextLink: at the end of computation contains the pointer to the link.
  * @retval true if the End-of-File has been reached by the last operation. False otherwise. 
  */
bool phemap::PufChainFromFile::getNextLink(puf_t *nextLink)
{
	(*pufChainFile) >> this->nextLink;
	rowCounter++;
	for (int i = 0; i < getCRByteLength(); i++)
	{
		nextLink[i] = strtol(this->nextLink.substr(2 * i, 2).data(), NULL, 16);
	}
	return pufChainFile->eof();
}

/**
  * @brief	Given a Puf link, looks for the successive one at a given distance. 
  * 		It lets the counter unmodified.
  * @param  nextLink: pointer to the Puf link that will be written.
  * @param	distance: distance of the link from the actual position in the chain file.
  * @retval true if the End-of-File has been reached by the last operation. False otherwise. 
  */
bool phemap::PufChainFromFile::peekLink(puf_t *nextLink, int distance)
{
	string links;
	do
	{
		(*pufChainFile) >> links;
	} while (--distance > 0);
	for (int i = 0; i < getCRByteLength(); i++)
	{
		nextLink[i] = strtol(links.substr(2 * i, 2).data(), NULL, 16);
	}
	bool testEof = pufChainFile->eof();

	pufChainFile->clear();
	pufChainFile->seekg(0, ios::beg);
	for (int i = 0; i < rowCounter; ++i)
		pufChainFile->ignore(numeric_limits<streamsize>::max(), '\n');
	return testEof;
}

/**
  * @brief	Destructor for PufChainFromFile.
  * @param  None
  * @retval None
  */
phemap::PufChainFromFile::~PufChainFromFile()
{
	pufChainFile->close();
	free(pufChainFile);
}
